import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IServicio, Servicio } from '../servicio.model';
import { ServicioService } from '../service/servicio.service';
import { ICliente } from 'app/entities/cliente/cliente.model';
import { ClienteService } from 'app/entities/cliente/service/cliente.service';
import { EstadoServicio } from 'app/entities/enumerations/estado-servicio.model';

@Component({
  selector: 'upgrade-servicio-update',
  templateUrl: './servicio-update.component.html',
})
export class ServicioUpdateComponent implements OnInit {
  isSaving = false;
  estadoServicioValues = Object.keys(EstadoServicio);

  clientesSharedCollection: ICliente[] = [];

  editForm = this.fb.group({
    id: [],
    nombre: [null, [Validators.required]],
    urlPropuesta: [null, [Validators.required]],
    isActivo: [null, [Validators.required]],
    estado: [null, [Validators.required]],
    fechaCreacion: [null, [Validators.required]],
    cliente: [],
  });

  constructor(
    protected servicioService: ServicioService,
    protected clienteService: ClienteService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ servicio }) => {
      if (servicio.id === undefined) {
        const today = dayjs().startOf('day');
        servicio.fechaCreacion = today;
      }

      this.updateForm(servicio);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const servicio = this.createFromForm();
    if (servicio.id !== undefined) {
      this.subscribeToSaveResponse(this.servicioService.update(servicio));
    } else {
      this.subscribeToSaveResponse(this.servicioService.create(servicio));
    }
  }

  trackClienteById(index: number, item: ICliente): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IServicio>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(servicio: IServicio): void {
    this.editForm.patchValue({
      id: servicio.id,
      nombre: servicio.nombre,
      urlPropuesta: servicio.urlPropuesta,
      isActivo: servicio.isActivo,
      estado: servicio.estado,
      fechaCreacion: servicio.fechaCreacion ? servicio.fechaCreacion.format(DATE_TIME_FORMAT) : null,
      cliente: servicio.cliente,
    });

    this.clientesSharedCollection = this.clienteService.addClienteToCollectionIfMissing(this.clientesSharedCollection, servicio.cliente);
  }

  protected loadRelationshipsOptions(): void {
    this.clienteService
      .query()
      .pipe(map((res: HttpResponse<ICliente[]>) => res.body ?? []))
      .pipe(
        map((clientes: ICliente[]) => this.clienteService.addClienteToCollectionIfMissing(clientes, this.editForm.get('cliente')!.value))
      )
      .subscribe((clientes: ICliente[]) => (this.clientesSharedCollection = clientes));
  }

  protected createFromForm(): IServicio {
    return {
      ...new Servicio(),
      id: this.editForm.get(['id'])!.value,
      nombre: this.editForm.get(['nombre'])!.value,
      urlPropuesta: this.editForm.get(['urlPropuesta'])!.value,
      isActivo: this.editForm.get(['isActivo'])!.value,
      estado: this.editForm.get(['estado'])!.value,
      fechaCreacion: this.editForm.get(['fechaCreacion'])!.value
        ? dayjs(this.editForm.get(['fechaCreacion'])!.value, DATE_TIME_FORMAT)
        : undefined,
      cliente: this.editForm.get(['cliente'])!.value,
    };
  }
}
