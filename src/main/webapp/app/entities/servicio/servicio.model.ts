import * as dayjs from 'dayjs';
import { IIniciativa } from 'app/entities/iniciativa/iniciativa.model';
import { ICliente } from 'app/entities/cliente/cliente.model';
import { EstadoServicio } from 'app/entities/enumerations/estado-servicio.model';

export interface IServicio {
  id?: number;
  nombre?: string;
  urlPropuesta?: string;
  isActivo?: boolean;
  estado?: EstadoServicio;
  fechaCreacion?: dayjs.Dayjs;
  iniciativas?: IIniciativa[] | null;
  cliente?: ICliente | null;
}

export class Servicio implements IServicio {
  constructor(
    public id?: number,
    public nombre?: string,
    public urlPropuesta?: string,
    public isActivo?: boolean,
    public estado?: EstadoServicio,
    public fechaCreacion?: dayjs.Dayjs,
    public iniciativas?: IIniciativa[] | null,
    public cliente?: ICliente | null
  ) {
    this.isActivo = this.isActivo ?? false;
  }
}

export function getServicioIdentifier(servicio: IServicio): number | undefined {
  return servicio.id;
}
