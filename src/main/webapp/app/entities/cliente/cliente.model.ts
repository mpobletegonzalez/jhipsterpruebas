import * as dayjs from 'dayjs';
import { IServicio } from 'app/entities/servicio/servicio.model';

export interface ICliente {
  id?: number;
  nombre?: string;
  img?: string | null;
  fechaCreacion?: dayjs.Dayjs;
  isActivo?: boolean;
  servicios?: IServicio[] | null;
}

export class Cliente implements ICliente {
  constructor(
    public id?: number,
    public nombre?: string,
    public img?: string | null,
    public fechaCreacion?: dayjs.Dayjs,
    public isActivo?: boolean,
    public servicios?: IServicio[] | null
  ) {
    this.isActivo = this.isActivo ?? false;
  }
}

export function getClienteIdentifier(cliente: ICliente): number | undefined {
  return cliente.id;
}
