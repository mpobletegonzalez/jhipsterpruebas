import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IPersona, Persona } from '../persona.model';
import { PersonaService } from '../service/persona.service';
import { TipoCargo } from 'app/entities/enumerations/tipo-cargo.model';

@Component({
  selector: 'upgrade-persona-update',
  templateUrl: './persona-update.component.html',
})
export class PersonaUpdateComponent implements OnInit {
  isSaving = false;
  tipoCargoValues = Object.keys(TipoCargo);

  editForm = this.fb.group({
    id: [],
    nombre: [null, [Validators.required]],
    apellido: [null, [Validators.required]],
    img: [],
    email: [null, [Validators.required]],
    login: [],
    userId: [null, []],
    fechaCreacion: [null, [Validators.required]],
    isActivo: [null, [Validators.required]],
    cargo: [],
  });

  constructor(protected personaService: PersonaService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ persona }) => {
      if (persona.id === undefined) {
        const today = dayjs().startOf('day');
        persona.fechaCreacion = today;
      }

      this.updateForm(persona);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const persona = this.createFromForm();
    if (persona.id !== undefined) {
      this.subscribeToSaveResponse(this.personaService.update(persona));
    } else {
      this.subscribeToSaveResponse(this.personaService.create(persona));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IPersona>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(persona: IPersona): void {
    this.editForm.patchValue({
      id: persona.id,
      nombre: persona.nombre,
      apellido: persona.apellido,
      img: persona.img,
      email: persona.email,
      login: persona.login,
      userId: persona.userId,
      fechaCreacion: persona.fechaCreacion ? persona.fechaCreacion.format(DATE_TIME_FORMAT) : null,
      isActivo: persona.isActivo,
      cargo: persona.cargo,
    });
  }

  protected createFromForm(): IPersona {
    return {
      ...new Persona(),
      id: this.editForm.get(['id'])!.value,
      nombre: this.editForm.get(['nombre'])!.value,
      apellido: this.editForm.get(['apellido'])!.value,
      img: this.editForm.get(['img'])!.value,
      email: this.editForm.get(['email'])!.value,
      login: this.editForm.get(['login'])!.value,
      userId: this.editForm.get(['userId'])!.value,
      fechaCreacion: this.editForm.get(['fechaCreacion'])!.value
        ? dayjs(this.editForm.get(['fechaCreacion'])!.value, DATE_TIME_FORMAT)
        : undefined,
      isActivo: this.editForm.get(['isActivo'])!.value,
      cargo: this.editForm.get(['cargo'])!.value,
    };
  }
}
