import * as dayjs from 'dayjs';
import { IAsignacion } from 'app/entities/asignacion/asignacion.model';
import { TipoCargo } from 'app/entities/enumerations/tipo-cargo.model';

export interface IPersona {
  id?: number;
  nombre?: string;
  apellido?: string;
  img?: string | null;
  email?: string;
  login?: string | null;
  userId?: number | null;
  fechaCreacion?: dayjs.Dayjs;
  isActivo?: boolean;
  cargo?: TipoCargo | null;
  asignacions?: IAsignacion[] | null;
}

export class Persona implements IPersona {
  constructor(
    public id?: number,
    public nombre?: string,
    public apellido?: string,
    public img?: string | null,
    public email?: string,
    public login?: string | null,
    public userId?: number | null,
    public fechaCreacion?: dayjs.Dayjs,
    public isActivo?: boolean,
    public cargo?: TipoCargo | null,
    public asignacions?: IAsignacion[] | null
  ) {
    this.isActivo = this.isActivo ?? false;
  }
}

export function getPersonaIdentifier(persona: IPersona): number | undefined {
  return persona.id;
}
