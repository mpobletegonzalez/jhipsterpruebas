import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IHito } from '../hito.model';
import { HitoService } from '../service/hito.service';

@Component({
  templateUrl: './hito-delete-dialog.component.html',
})
export class HitoDeleteDialogComponent {
  hito?: IHito;

  constructor(protected hitoService: HitoService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.hitoService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
