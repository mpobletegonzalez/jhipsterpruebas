import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { HitoComponent } from '../list/hito.component';
import { HitoDetailComponent } from '../detail/hito-detail.component';
import { HitoUpdateComponent } from '../update/hito-update.component';
import { HitoRoutingResolveService } from './hito-routing-resolve.service';

const hitoRoute: Routes = [
  {
    path: '',
    component: HitoComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: HitoDetailComponent,
    resolve: {
      hito: HitoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: HitoUpdateComponent,
    resolve: {
      hito: HitoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: HitoUpdateComponent,
    resolve: {
      hito: HitoRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(hitoRoute)],
  exports: [RouterModule],
})
export class HitoRoutingModule {}
