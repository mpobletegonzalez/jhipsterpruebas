import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IHito, Hito } from '../hito.model';
import { HitoService } from '../service/hito.service';

@Injectable({ providedIn: 'root' })
export class HitoRoutingResolveService implements Resolve<IHito> {
  constructor(protected service: HitoService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IHito> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((hito: HttpResponse<Hito>) => {
          if (hito.body) {
            return of(hito.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Hito());
  }
}
