jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { HitoService } from '../service/hito.service';
import { IHito, Hito } from '../hito.model';
import { IIniciativa } from 'app/entities/iniciativa/iniciativa.model';
import { IniciativaService } from 'app/entities/iniciativa/service/iniciativa.service';

import { HitoUpdateComponent } from './hito-update.component';

describe('Hito Management Update Component', () => {
  let comp: HitoUpdateComponent;
  let fixture: ComponentFixture<HitoUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let hitoService: HitoService;
  let iniciativaService: IniciativaService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [HitoUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(HitoUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(HitoUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    hitoService = TestBed.inject(HitoService);
    iniciativaService = TestBed.inject(IniciativaService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Iniciativa query and add missing value', () => {
      const hito: IHito = { id: 456 };
      const iniciativa: IIniciativa = { id: 23376 };
      hito.iniciativa = iniciativa;

      const iniciativaCollection: IIniciativa[] = [{ id: 92393 }];
      jest.spyOn(iniciativaService, 'query').mockReturnValue(of(new HttpResponse({ body: iniciativaCollection })));
      const additionalIniciativas = [iniciativa];
      const expectedCollection: IIniciativa[] = [...additionalIniciativas, ...iniciativaCollection];
      jest.spyOn(iniciativaService, 'addIniciativaToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ hito });
      comp.ngOnInit();

      expect(iniciativaService.query).toHaveBeenCalled();
      expect(iniciativaService.addIniciativaToCollectionIfMissing).toHaveBeenCalledWith(iniciativaCollection, ...additionalIniciativas);
      expect(comp.iniciativasSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const hito: IHito = { id: 456 };
      const iniciativa: IIniciativa = { id: 70617 };
      hito.iniciativa = iniciativa;

      activatedRoute.data = of({ hito });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(hito));
      expect(comp.iniciativasSharedCollection).toContain(iniciativa);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Hito>>();
      const hito = { id: 123 };
      jest.spyOn(hitoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ hito });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: hito }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(hitoService.update).toHaveBeenCalledWith(hito);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Hito>>();
      const hito = new Hito();
      jest.spyOn(hitoService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ hito });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: hito }));
      saveSubject.complete();

      // THEN
      expect(hitoService.create).toHaveBeenCalledWith(hito);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Hito>>();
      const hito = { id: 123 };
      jest.spyOn(hitoService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ hito });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(hitoService.update).toHaveBeenCalledWith(hito);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackIniciativaById', () => {
      it('Should return tracked Iniciativa primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackIniciativaById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
