import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IHito, Hito } from '../hito.model';
import { HitoService } from '../service/hito.service';
import { IIniciativa } from 'app/entities/iniciativa/iniciativa.model';
import { IniciativaService } from 'app/entities/iniciativa/service/iniciativa.service';
import { TipoHito } from 'app/entities/enumerations/tipo-hito.model';

@Component({
  selector: 'upgrade-hito-update',
  templateUrl: './hito-update.component.html',
})
export class HitoUpdateComponent implements OnInit {
  isSaving = false;
  tipoHitoValues = Object.keys(TipoHito);

  iniciativasSharedCollection: IIniciativa[] = [];

  editForm = this.fb.group({
    id: [],
    nombre: [],
    descripcion: [null, [Validators.required]],
    fechaInicio: [],
    fechaCumplimiento: [],
    tipoHito: [null, [Validators.required]],
    fechaCreacion: [null, [Validators.required]],
    isActivo: [null, [Validators.required]],
    iniciativa: [],
  });

  constructor(
    protected hitoService: HitoService,
    protected iniciativaService: IniciativaService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ hito }) => {
      if (hito.id === undefined) {
        const today = dayjs().startOf('day');
        hito.fechaInicio = today;
        hito.fechaCumplimiento = today;
        hito.fechaCreacion = today;
      }

      this.updateForm(hito);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const hito = this.createFromForm();
    if (hito.id !== undefined) {
      this.subscribeToSaveResponse(this.hitoService.update(hito));
    } else {
      this.subscribeToSaveResponse(this.hitoService.create(hito));
    }
  }

  trackIniciativaById(index: number, item: IIniciativa): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IHito>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(hito: IHito): void {
    this.editForm.patchValue({
      id: hito.id,
      nombre: hito.nombre,
      descripcion: hito.descripcion,
      fechaInicio: hito.fechaInicio ? hito.fechaInicio.format(DATE_TIME_FORMAT) : null,
      fechaCumplimiento: hito.fechaCumplimiento ? hito.fechaCumplimiento.format(DATE_TIME_FORMAT) : null,
      tipoHito: hito.tipoHito,
      fechaCreacion: hito.fechaCreacion ? hito.fechaCreacion.format(DATE_TIME_FORMAT) : null,
      isActivo: hito.isActivo,
      iniciativa: hito.iniciativa,
    });

    this.iniciativasSharedCollection = this.iniciativaService.addIniciativaToCollectionIfMissing(
      this.iniciativasSharedCollection,
      hito.iniciativa
    );
  }

  protected loadRelationshipsOptions(): void {
    this.iniciativaService
      .query()
      .pipe(map((res: HttpResponse<IIniciativa[]>) => res.body ?? []))
      .pipe(
        map((iniciativas: IIniciativa[]) =>
          this.iniciativaService.addIniciativaToCollectionIfMissing(iniciativas, this.editForm.get('iniciativa')!.value)
        )
      )
      .subscribe((iniciativas: IIniciativa[]) => (this.iniciativasSharedCollection = iniciativas));
  }

  protected createFromForm(): IHito {
    return {
      ...new Hito(),
      id: this.editForm.get(['id'])!.value,
      nombre: this.editForm.get(['nombre'])!.value,
      descripcion: this.editForm.get(['descripcion'])!.value,
      fechaInicio: this.editForm.get(['fechaInicio'])!.value
        ? dayjs(this.editForm.get(['fechaInicio'])!.value, DATE_TIME_FORMAT)
        : undefined,
      fechaCumplimiento: this.editForm.get(['fechaCumplimiento'])!.value
        ? dayjs(this.editForm.get(['fechaCumplimiento'])!.value, DATE_TIME_FORMAT)
        : undefined,
      tipoHito: this.editForm.get(['tipoHito'])!.value,
      fechaCreacion: this.editForm.get(['fechaCreacion'])!.value
        ? dayjs(this.editForm.get(['fechaCreacion'])!.value, DATE_TIME_FORMAT)
        : undefined,
      isActivo: this.editForm.get(['isActivo'])!.value,
      iniciativa: this.editForm.get(['iniciativa'])!.value,
    };
  }
}
