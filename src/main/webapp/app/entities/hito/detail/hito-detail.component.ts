import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IHito } from '../hito.model';

@Component({
  selector: 'upgrade-hito-detail',
  templateUrl: './hito-detail.component.html',
})
export class HitoDetailComponent implements OnInit {
  hito: IHito | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ hito }) => {
      this.hito = hito;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
