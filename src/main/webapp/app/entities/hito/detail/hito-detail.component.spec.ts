import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { HitoDetailComponent } from './hito-detail.component';

describe('Hito Management Detail Component', () => {
  let comp: HitoDetailComponent;
  let fixture: ComponentFixture<HitoDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [HitoDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ hito: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(HitoDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(HitoDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load hito on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.hito).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
