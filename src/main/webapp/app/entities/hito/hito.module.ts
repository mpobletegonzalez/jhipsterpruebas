import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { HitoComponent } from './list/hito.component';
import { HitoDetailComponent } from './detail/hito-detail.component';
import { HitoUpdateComponent } from './update/hito-update.component';
import { HitoDeleteDialogComponent } from './delete/hito-delete-dialog.component';
import { HitoRoutingModule } from './route/hito-routing.module';

@NgModule({
  imports: [SharedModule, HitoRoutingModule],
  declarations: [HitoComponent, HitoDetailComponent, HitoUpdateComponent, HitoDeleteDialogComponent],
  entryComponents: [HitoDeleteDialogComponent],
})
export class HitoModule {}
