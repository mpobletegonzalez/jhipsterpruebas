import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IHito, getHitoIdentifier } from '../hito.model';

export type EntityResponseType = HttpResponse<IHito>;
export type EntityArrayResponseType = HttpResponse<IHito[]>;

@Injectable({ providedIn: 'root' })
export class HitoService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/hitos');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(hito: IHito): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(hito);
    return this.http
      .post<IHito>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(hito: IHito): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(hito);
    return this.http
      .put<IHito>(`${this.resourceUrl}/${getHitoIdentifier(hito) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(hito: IHito): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(hito);
    return this.http
      .patch<IHito>(`${this.resourceUrl}/${getHitoIdentifier(hito) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IHito>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IHito[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addHitoToCollectionIfMissing(hitoCollection: IHito[], ...hitosToCheck: (IHito | null | undefined)[]): IHito[] {
    const hitos: IHito[] = hitosToCheck.filter(isPresent);
    if (hitos.length > 0) {
      const hitoCollectionIdentifiers = hitoCollection.map(hitoItem => getHitoIdentifier(hitoItem)!);
      const hitosToAdd = hitos.filter(hitoItem => {
        const hitoIdentifier = getHitoIdentifier(hitoItem);
        if (hitoIdentifier == null || hitoCollectionIdentifiers.includes(hitoIdentifier)) {
          return false;
        }
        hitoCollectionIdentifiers.push(hitoIdentifier);
        return true;
      });
      return [...hitosToAdd, ...hitoCollection];
    }
    return hitoCollection;
  }

  protected convertDateFromClient(hito: IHito): IHito {
    return Object.assign({}, hito, {
      fechaInicio: hito.fechaInicio?.isValid() ? hito.fechaInicio.toJSON() : undefined,
      fechaCumplimiento: hito.fechaCumplimiento?.isValid() ? hito.fechaCumplimiento.toJSON() : undefined,
      fechaCreacion: hito.fechaCreacion?.isValid() ? hito.fechaCreacion.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.fechaInicio = res.body.fechaInicio ? dayjs(res.body.fechaInicio) : undefined;
      res.body.fechaCumplimiento = res.body.fechaCumplimiento ? dayjs(res.body.fechaCumplimiento) : undefined;
      res.body.fechaCreacion = res.body.fechaCreacion ? dayjs(res.body.fechaCreacion) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((hito: IHito) => {
        hito.fechaInicio = hito.fechaInicio ? dayjs(hito.fechaInicio) : undefined;
        hito.fechaCumplimiento = hito.fechaCumplimiento ? dayjs(hito.fechaCumplimiento) : undefined;
        hito.fechaCreacion = hito.fechaCreacion ? dayjs(hito.fechaCreacion) : undefined;
      });
    }
    return res;
  }
}
