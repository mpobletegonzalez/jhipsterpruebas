import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { TipoHito } from 'app/entities/enumerations/tipo-hito.model';
import { IHito, Hito } from '../hito.model';

import { HitoService } from './hito.service';

describe('Hito Service', () => {
  let service: HitoService;
  let httpMock: HttpTestingController;
  let elemDefault: IHito;
  let expectedResult: IHito | IHito[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(HitoService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      nombre: 'AAAAAAA',
      descripcion: 'AAAAAAA',
      fechaInicio: currentDate,
      fechaCumplimiento: currentDate,
      tipoHito: TipoHito.RLS,
      fechaCreacion: currentDate,
      isActivo: false,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          fechaInicio: currentDate.format(DATE_TIME_FORMAT),
          fechaCumplimiento: currentDate.format(DATE_TIME_FORMAT),
          fechaCreacion: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Hito', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          fechaInicio: currentDate.format(DATE_TIME_FORMAT),
          fechaCumplimiento: currentDate.format(DATE_TIME_FORMAT),
          fechaCreacion: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          fechaInicio: currentDate,
          fechaCumplimiento: currentDate,
          fechaCreacion: currentDate,
        },
        returnedFromService
      );

      service.create(new Hito()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Hito', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nombre: 'BBBBBB',
          descripcion: 'BBBBBB',
          fechaInicio: currentDate.format(DATE_TIME_FORMAT),
          fechaCumplimiento: currentDate.format(DATE_TIME_FORMAT),
          tipoHito: 'BBBBBB',
          fechaCreacion: currentDate.format(DATE_TIME_FORMAT),
          isActivo: true,
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          fechaInicio: currentDate,
          fechaCumplimiento: currentDate,
          fechaCreacion: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Hito', () => {
      const patchObject = Object.assign(
        {
          descripcion: 'BBBBBB',
          tipoHito: 'BBBBBB',
          fechaCreacion: currentDate.format(DATE_TIME_FORMAT),
          isActivo: true,
        },
        new Hito()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          fechaInicio: currentDate,
          fechaCumplimiento: currentDate,
          fechaCreacion: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Hito', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nombre: 'BBBBBB',
          descripcion: 'BBBBBB',
          fechaInicio: currentDate.format(DATE_TIME_FORMAT),
          fechaCumplimiento: currentDate.format(DATE_TIME_FORMAT),
          tipoHito: 'BBBBBB',
          fechaCreacion: currentDate.format(DATE_TIME_FORMAT),
          isActivo: true,
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          fechaInicio: currentDate,
          fechaCumplimiento: currentDate,
          fechaCreacion: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Hito', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addHitoToCollectionIfMissing', () => {
      it('should add a Hito to an empty array', () => {
        const hito: IHito = { id: 123 };
        expectedResult = service.addHitoToCollectionIfMissing([], hito);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(hito);
      });

      it('should not add a Hito to an array that contains it', () => {
        const hito: IHito = { id: 123 };
        const hitoCollection: IHito[] = [
          {
            ...hito,
          },
          { id: 456 },
        ];
        expectedResult = service.addHitoToCollectionIfMissing(hitoCollection, hito);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Hito to an array that doesn't contain it", () => {
        const hito: IHito = { id: 123 };
        const hitoCollection: IHito[] = [{ id: 456 }];
        expectedResult = service.addHitoToCollectionIfMissing(hitoCollection, hito);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(hito);
      });

      it('should add only unique Hito to an array', () => {
        const hitoArray: IHito[] = [{ id: 123 }, { id: 456 }, { id: 79621 }];
        const hitoCollection: IHito[] = [{ id: 123 }];
        expectedResult = service.addHitoToCollectionIfMissing(hitoCollection, ...hitoArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const hito: IHito = { id: 123 };
        const hito2: IHito = { id: 456 };
        expectedResult = service.addHitoToCollectionIfMissing([], hito, hito2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(hito);
        expect(expectedResult).toContain(hito2);
      });

      it('should accept null and undefined values', () => {
        const hito: IHito = { id: 123 };
        expectedResult = service.addHitoToCollectionIfMissing([], null, hito, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(hito);
      });

      it('should return initial array if no Hito is added', () => {
        const hitoCollection: IHito[] = [{ id: 123 }];
        expectedResult = service.addHitoToCollectionIfMissing(hitoCollection, undefined, null);
        expect(expectedResult).toEqual(hitoCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
