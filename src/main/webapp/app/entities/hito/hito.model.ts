import * as dayjs from 'dayjs';
import { IIniciativa } from 'app/entities/iniciativa/iniciativa.model';
import { TipoHito } from 'app/entities/enumerations/tipo-hito.model';

export interface IHito {
  id?: number;
  nombre?: string | null;
  descripcion?: string;
  fechaInicio?: dayjs.Dayjs | null;
  fechaCumplimiento?: dayjs.Dayjs | null;
  tipoHito?: TipoHito;
  fechaCreacion?: dayjs.Dayjs;
  isActivo?: boolean;
  iniciativa?: IIniciativa | null;
}

export class Hito implements IHito {
  constructor(
    public id?: number,
    public nombre?: string | null,
    public descripcion?: string,
    public fechaInicio?: dayjs.Dayjs | null,
    public fechaCumplimiento?: dayjs.Dayjs | null,
    public tipoHito?: TipoHito,
    public fechaCreacion?: dayjs.Dayjs,
    public isActivo?: boolean,
    public iniciativa?: IIniciativa | null
  ) {
    this.isActivo = this.isActivo ?? false;
  }
}

export function getHitoIdentifier(hito: IHito): number | undefined {
  return hito.id;
}
