import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'cliente',
        data: { pageTitle: 'upgradeAppApp.cliente.home.title' },
        loadChildren: () => import('./cliente/cliente.module').then(m => m.ClienteModule),
      },
      {
        path: 'servicio',
        data: { pageTitle: 'upgradeAppApp.servicio.home.title' },
        loadChildren: () => import('./servicio/servicio.module').then(m => m.ServicioModule),
      },
      {
        path: 'iniciativa',
        data: { pageTitle: 'upgradeAppApp.iniciativa.home.title' },
        loadChildren: () => import('./iniciativa/iniciativa.module').then(m => m.IniciativaModule),
      },
      {
        path: 'hito',
        data: { pageTitle: 'upgradeAppApp.hito.home.title' },
        loadChildren: () => import('./hito/hito.module').then(m => m.HitoModule),
      },
      {
        path: 'persona',
        data: { pageTitle: 'upgradeAppApp.persona.home.title' },
        loadChildren: () => import('./persona/persona.module').then(m => m.PersonaModule),
      },
      {
        path: 'estados-historias',
        data: { pageTitle: 'upgradeAppApp.estadosHistorias.home.title' },
        loadChildren: () => import('./estados-historias/estados-historias.module').then(m => m.EstadosHistoriasModule),
      },
      {
        path: 'funcionalidad',
        data: { pageTitle: 'upgradeAppApp.funcionalidad.home.title' },
        loadChildren: () => import('./funcionalidad/funcionalidad.module').then(m => m.FuncionalidadModule),
      },
      {
        path: 'registro-historia',
        data: { pageTitle: 'upgradeAppApp.registroHistoria.home.title' },
        loadChildren: () => import('./registro-historia/registro-historia.module').then(m => m.RegistroHistoriaModule),
      },
      {
        path: 'asignacion',
        data: { pageTitle: 'upgradeAppApp.asignacion.home.title' },
        loadChildren: () => import('./asignacion/asignacion.module').then(m => m.AsignacionModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
