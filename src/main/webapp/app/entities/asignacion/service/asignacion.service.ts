import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IAsignacion, getAsignacionIdentifier } from '../asignacion.model';

export type EntityResponseType = HttpResponse<IAsignacion>;
export type EntityArrayResponseType = HttpResponse<IAsignacion[]>;

@Injectable({ providedIn: 'root' })
export class AsignacionService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/asignacions');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(asignacion: IAsignacion): Observable<EntityResponseType> {
    return this.http.post<IAsignacion>(this.resourceUrl, asignacion, { observe: 'response' });
  }

  update(asignacion: IAsignacion): Observable<EntityResponseType> {
    return this.http.put<IAsignacion>(`${this.resourceUrl}/${getAsignacionIdentifier(asignacion) as number}`, asignacion, {
      observe: 'response',
    });
  }

  partialUpdate(asignacion: IAsignacion): Observable<EntityResponseType> {
    return this.http.patch<IAsignacion>(`${this.resourceUrl}/${getAsignacionIdentifier(asignacion) as number}`, asignacion, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IAsignacion>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IAsignacion[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addAsignacionToCollectionIfMissing(
    asignacionCollection: IAsignacion[],
    ...asignacionsToCheck: (IAsignacion | null | undefined)[]
  ): IAsignacion[] {
    const asignacions: IAsignacion[] = asignacionsToCheck.filter(isPresent);
    if (asignacions.length > 0) {
      const asignacionCollectionIdentifiers = asignacionCollection.map(asignacionItem => getAsignacionIdentifier(asignacionItem)!);
      const asignacionsToAdd = asignacions.filter(asignacionItem => {
        const asignacionIdentifier = getAsignacionIdentifier(asignacionItem);
        if (asignacionIdentifier == null || asignacionCollectionIdentifiers.includes(asignacionIdentifier)) {
          return false;
        }
        asignacionCollectionIdentifiers.push(asignacionIdentifier);
        return true;
      });
      return [...asignacionsToAdd, ...asignacionCollection];
    }
    return asignacionCollection;
  }
}
