import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IAsignacion, Asignacion } from '../asignacion.model';
import { AsignacionService } from '../service/asignacion.service';
import { IPersona } from 'app/entities/persona/persona.model';
import { PersonaService } from 'app/entities/persona/service/persona.service';
import { IIniciativa } from 'app/entities/iniciativa/iniciativa.model';
import { IniciativaService } from 'app/entities/iniciativa/service/iniciativa.service';

@Component({
  selector: 'upgrade-asignacion-update',
  templateUrl: './asignacion-update.component.html',
})
export class AsignacionUpdateComponent implements OnInit {
  isSaving = false;

  personasSharedCollection: IPersona[] = [];
  iniciativasSharedCollection: IIniciativa[] = [];

  editForm = this.fb.group({
    id: [],
    porcentaje: [],
    cliente: [],
    iniciativa: [],
  });

  constructor(
    protected asignacionService: AsignacionService,
    protected personaService: PersonaService,
    protected iniciativaService: IniciativaService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ asignacion }) => {
      this.updateForm(asignacion);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const asignacion = this.createFromForm();
    if (asignacion.id !== undefined) {
      this.subscribeToSaveResponse(this.asignacionService.update(asignacion));
    } else {
      this.subscribeToSaveResponse(this.asignacionService.create(asignacion));
    }
  }

  trackPersonaById(index: number, item: IPersona): number {
    return item.id!;
  }

  trackIniciativaById(index: number, item: IIniciativa): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IAsignacion>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(asignacion: IAsignacion): void {
    this.editForm.patchValue({
      id: asignacion.id,
      porcentaje: asignacion.porcentaje,
      cliente: asignacion.cliente,
      iniciativa: asignacion.iniciativa,
    });

    this.personasSharedCollection = this.personaService.addPersonaToCollectionIfMissing(this.personasSharedCollection, asignacion.cliente);
    this.iniciativasSharedCollection = this.iniciativaService.addIniciativaToCollectionIfMissing(
      this.iniciativasSharedCollection,
      asignacion.iniciativa
    );
  }

  protected loadRelationshipsOptions(): void {
    this.personaService
      .query()
      .pipe(map((res: HttpResponse<IPersona[]>) => res.body ?? []))
      .pipe(
        map((personas: IPersona[]) => this.personaService.addPersonaToCollectionIfMissing(personas, this.editForm.get('cliente')!.value))
      )
      .subscribe((personas: IPersona[]) => (this.personasSharedCollection = personas));

    this.iniciativaService
      .query()
      .pipe(map((res: HttpResponse<IIniciativa[]>) => res.body ?? []))
      .pipe(
        map((iniciativas: IIniciativa[]) =>
          this.iniciativaService.addIniciativaToCollectionIfMissing(iniciativas, this.editForm.get('iniciativa')!.value)
        )
      )
      .subscribe((iniciativas: IIniciativa[]) => (this.iniciativasSharedCollection = iniciativas));
  }

  protected createFromForm(): IAsignacion {
    return {
      ...new Asignacion(),
      id: this.editForm.get(['id'])!.value,
      porcentaje: this.editForm.get(['porcentaje'])!.value,
      cliente: this.editForm.get(['cliente'])!.value,
      iniciativa: this.editForm.get(['iniciativa'])!.value,
    };
  }
}
