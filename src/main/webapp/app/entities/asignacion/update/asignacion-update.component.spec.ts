jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { AsignacionService } from '../service/asignacion.service';
import { IAsignacion, Asignacion } from '../asignacion.model';
import { IPersona } from 'app/entities/persona/persona.model';
import { PersonaService } from 'app/entities/persona/service/persona.service';
import { IIniciativa } from 'app/entities/iniciativa/iniciativa.model';
import { IniciativaService } from 'app/entities/iniciativa/service/iniciativa.service';

import { AsignacionUpdateComponent } from './asignacion-update.component';

describe('Asignacion Management Update Component', () => {
  let comp: AsignacionUpdateComponent;
  let fixture: ComponentFixture<AsignacionUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let asignacionService: AsignacionService;
  let personaService: PersonaService;
  let iniciativaService: IniciativaService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [AsignacionUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(AsignacionUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(AsignacionUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    asignacionService = TestBed.inject(AsignacionService);
    personaService = TestBed.inject(PersonaService);
    iniciativaService = TestBed.inject(IniciativaService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Persona query and add missing value', () => {
      const asignacion: IAsignacion = { id: 456 };
      const cliente: IPersona = { id: 36833 };
      asignacion.cliente = cliente;

      const personaCollection: IPersona[] = [{ id: 49030 }];
      jest.spyOn(personaService, 'query').mockReturnValue(of(new HttpResponse({ body: personaCollection })));
      const additionalPersonas = [cliente];
      const expectedCollection: IPersona[] = [...additionalPersonas, ...personaCollection];
      jest.spyOn(personaService, 'addPersonaToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ asignacion });
      comp.ngOnInit();

      expect(personaService.query).toHaveBeenCalled();
      expect(personaService.addPersonaToCollectionIfMissing).toHaveBeenCalledWith(personaCollection, ...additionalPersonas);
      expect(comp.personasSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Iniciativa query and add missing value', () => {
      const asignacion: IAsignacion = { id: 456 };
      const iniciativa: IIniciativa = { id: 42381 };
      asignacion.iniciativa = iniciativa;

      const iniciativaCollection: IIniciativa[] = [{ id: 55468 }];
      jest.spyOn(iniciativaService, 'query').mockReturnValue(of(new HttpResponse({ body: iniciativaCollection })));
      const additionalIniciativas = [iniciativa];
      const expectedCollection: IIniciativa[] = [...additionalIniciativas, ...iniciativaCollection];
      jest.spyOn(iniciativaService, 'addIniciativaToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ asignacion });
      comp.ngOnInit();

      expect(iniciativaService.query).toHaveBeenCalled();
      expect(iniciativaService.addIniciativaToCollectionIfMissing).toHaveBeenCalledWith(iniciativaCollection, ...additionalIniciativas);
      expect(comp.iniciativasSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const asignacion: IAsignacion = { id: 456 };
      const cliente: IPersona = { id: 47175 };
      asignacion.cliente = cliente;
      const iniciativa: IIniciativa = { id: 74467 };
      asignacion.iniciativa = iniciativa;

      activatedRoute.data = of({ asignacion });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(asignacion));
      expect(comp.personasSharedCollection).toContain(cliente);
      expect(comp.iniciativasSharedCollection).toContain(iniciativa);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Asignacion>>();
      const asignacion = { id: 123 };
      jest.spyOn(asignacionService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ asignacion });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: asignacion }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(asignacionService.update).toHaveBeenCalledWith(asignacion);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Asignacion>>();
      const asignacion = new Asignacion();
      jest.spyOn(asignacionService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ asignacion });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: asignacion }));
      saveSubject.complete();

      // THEN
      expect(asignacionService.create).toHaveBeenCalledWith(asignacion);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Asignacion>>();
      const asignacion = { id: 123 };
      jest.spyOn(asignacionService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ asignacion });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(asignacionService.update).toHaveBeenCalledWith(asignacion);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackPersonaById', () => {
      it('Should return tracked Persona primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackPersonaById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackIniciativaById', () => {
      it('Should return tracked Iniciativa primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackIniciativaById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
