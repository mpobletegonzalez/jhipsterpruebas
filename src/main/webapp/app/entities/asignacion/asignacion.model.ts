import { IPersona } from 'app/entities/persona/persona.model';
import { IIniciativa } from 'app/entities/iniciativa/iniciativa.model';

export interface IAsignacion {
  id?: number;
  porcentaje?: number | null;
  cliente?: IPersona | null;
  iniciativa?: IIniciativa | null;
}

export class Asignacion implements IAsignacion {
  constructor(
    public id?: number,
    public porcentaje?: number | null,
    public cliente?: IPersona | null,
    public iniciativa?: IIniciativa | null
  ) {}
}

export function getAsignacionIdentifier(asignacion: IAsignacion): number | undefined {
  return asignacion.id;
}
