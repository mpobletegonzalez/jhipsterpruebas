import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IFuncionalidad, Funcionalidad } from '../funcionalidad.model';
import { FuncionalidadService } from '../service/funcionalidad.service';
import { TipoEstadoFuncionalidad } from 'app/entities/enumerations/tipo-estado-funcionalidad.model';

@Component({
  selector: 'upgrade-funcionalidad-update',
  templateUrl: './funcionalidad-update.component.html',
})
export class FuncionalidadUpdateComponent implements OnInit {
  isSaving = false;
  tipoEstadoFuncionalidadValues = Object.keys(TipoEstadoFuncionalidad);

  editForm = this.fb.group({
    id: [],
    nombre: [],
    estado: [],
    prioridad: [],
  });

  constructor(protected funcionalidadService: FuncionalidadService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ funcionalidad }) => {
      this.updateForm(funcionalidad);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const funcionalidad = this.createFromForm();
    if (funcionalidad.id !== undefined) {
      this.subscribeToSaveResponse(this.funcionalidadService.update(funcionalidad));
    } else {
      this.subscribeToSaveResponse(this.funcionalidadService.create(funcionalidad));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IFuncionalidad>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(funcionalidad: IFuncionalidad): void {
    this.editForm.patchValue({
      id: funcionalidad.id,
      nombre: funcionalidad.nombre,
      estado: funcionalidad.estado,
      prioridad: funcionalidad.prioridad,
    });
  }

  protected createFromForm(): IFuncionalidad {
    return {
      ...new Funcionalidad(),
      id: this.editForm.get(['id'])!.value,
      nombre: this.editForm.get(['nombre'])!.value,
      estado: this.editForm.get(['estado'])!.value,
      prioridad: this.editForm.get(['prioridad'])!.value,
    };
  }
}
