jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { FuncionalidadService } from '../service/funcionalidad.service';
import { IFuncionalidad, Funcionalidad } from '../funcionalidad.model';

import { FuncionalidadUpdateComponent } from './funcionalidad-update.component';

describe('Funcionalidad Management Update Component', () => {
  let comp: FuncionalidadUpdateComponent;
  let fixture: ComponentFixture<FuncionalidadUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let funcionalidadService: FuncionalidadService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [FuncionalidadUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(FuncionalidadUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(FuncionalidadUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    funcionalidadService = TestBed.inject(FuncionalidadService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const funcionalidad: IFuncionalidad = { id: 456 };

      activatedRoute.data = of({ funcionalidad });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(funcionalidad));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Funcionalidad>>();
      const funcionalidad = { id: 123 };
      jest.spyOn(funcionalidadService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ funcionalidad });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: funcionalidad }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(funcionalidadService.update).toHaveBeenCalledWith(funcionalidad);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Funcionalidad>>();
      const funcionalidad = new Funcionalidad();
      jest.spyOn(funcionalidadService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ funcionalidad });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: funcionalidad }));
      saveSubject.complete();

      // THEN
      expect(funcionalidadService.create).toHaveBeenCalledWith(funcionalidad);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Funcionalidad>>();
      const funcionalidad = { id: 123 };
      jest.spyOn(funcionalidadService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ funcionalidad });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(funcionalidadService.update).toHaveBeenCalledWith(funcionalidad);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
