import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { TipoEstadoFuncionalidad } from 'app/entities/enumerations/tipo-estado-funcionalidad.model';
import { IFuncionalidad, Funcionalidad } from '../funcionalidad.model';

import { FuncionalidadService } from './funcionalidad.service';

describe('Funcionalidad Service', () => {
  let service: FuncionalidadService;
  let httpMock: HttpTestingController;
  let elemDefault: IFuncionalidad;
  let expectedResult: IFuncionalidad | IFuncionalidad[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(FuncionalidadService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      nombre: 'AAAAAAA',
      estado: TipoEstadoFuncionalidad.ORIGINAL,
      prioridad: 0,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Funcionalidad', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new Funcionalidad()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Funcionalidad', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nombre: 'BBBBBB',
          estado: 'BBBBBB',
          prioridad: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Funcionalidad', () => {
      const patchObject = Object.assign(
        {
          nombre: 'BBBBBB',
          estado: 'BBBBBB',
          prioridad: 1,
        },
        new Funcionalidad()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Funcionalidad', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nombre: 'BBBBBB',
          estado: 'BBBBBB',
          prioridad: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Funcionalidad', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addFuncionalidadToCollectionIfMissing', () => {
      it('should add a Funcionalidad to an empty array', () => {
        const funcionalidad: IFuncionalidad = { id: 123 };
        expectedResult = service.addFuncionalidadToCollectionIfMissing([], funcionalidad);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(funcionalidad);
      });

      it('should not add a Funcionalidad to an array that contains it', () => {
        const funcionalidad: IFuncionalidad = { id: 123 };
        const funcionalidadCollection: IFuncionalidad[] = [
          {
            ...funcionalidad,
          },
          { id: 456 },
        ];
        expectedResult = service.addFuncionalidadToCollectionIfMissing(funcionalidadCollection, funcionalidad);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Funcionalidad to an array that doesn't contain it", () => {
        const funcionalidad: IFuncionalidad = { id: 123 };
        const funcionalidadCollection: IFuncionalidad[] = [{ id: 456 }];
        expectedResult = service.addFuncionalidadToCollectionIfMissing(funcionalidadCollection, funcionalidad);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(funcionalidad);
      });

      it('should add only unique Funcionalidad to an array', () => {
        const funcionalidadArray: IFuncionalidad[] = [{ id: 123 }, { id: 456 }, { id: 69932 }];
        const funcionalidadCollection: IFuncionalidad[] = [{ id: 123 }];
        expectedResult = service.addFuncionalidadToCollectionIfMissing(funcionalidadCollection, ...funcionalidadArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const funcionalidad: IFuncionalidad = { id: 123 };
        const funcionalidad2: IFuncionalidad = { id: 456 };
        expectedResult = service.addFuncionalidadToCollectionIfMissing([], funcionalidad, funcionalidad2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(funcionalidad);
        expect(expectedResult).toContain(funcionalidad2);
      });

      it('should accept null and undefined values', () => {
        const funcionalidad: IFuncionalidad = { id: 123 };
        expectedResult = service.addFuncionalidadToCollectionIfMissing([], null, funcionalidad, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(funcionalidad);
      });

      it('should return initial array if no Funcionalidad is added', () => {
        const funcionalidadCollection: IFuncionalidad[] = [{ id: 123 }];
        expectedResult = service.addFuncionalidadToCollectionIfMissing(funcionalidadCollection, undefined, null);
        expect(expectedResult).toEqual(funcionalidadCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
