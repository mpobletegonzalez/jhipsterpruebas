import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IFuncionalidad, getFuncionalidadIdentifier } from '../funcionalidad.model';

export type EntityResponseType = HttpResponse<IFuncionalidad>;
export type EntityArrayResponseType = HttpResponse<IFuncionalidad[]>;

@Injectable({ providedIn: 'root' })
export class FuncionalidadService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/funcionalidads');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(funcionalidad: IFuncionalidad): Observable<EntityResponseType> {
    return this.http.post<IFuncionalidad>(this.resourceUrl, funcionalidad, { observe: 'response' });
  }

  update(funcionalidad: IFuncionalidad): Observable<EntityResponseType> {
    return this.http.put<IFuncionalidad>(`${this.resourceUrl}/${getFuncionalidadIdentifier(funcionalidad) as number}`, funcionalidad, {
      observe: 'response',
    });
  }

  partialUpdate(funcionalidad: IFuncionalidad): Observable<EntityResponseType> {
    return this.http.patch<IFuncionalidad>(`${this.resourceUrl}/${getFuncionalidadIdentifier(funcionalidad) as number}`, funcionalidad, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IFuncionalidad>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IFuncionalidad[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addFuncionalidadToCollectionIfMissing(
    funcionalidadCollection: IFuncionalidad[],
    ...funcionalidadsToCheck: (IFuncionalidad | null | undefined)[]
  ): IFuncionalidad[] {
    const funcionalidads: IFuncionalidad[] = funcionalidadsToCheck.filter(isPresent);
    if (funcionalidads.length > 0) {
      const funcionalidadCollectionIdentifiers = funcionalidadCollection.map(
        funcionalidadItem => getFuncionalidadIdentifier(funcionalidadItem)!
      );
      const funcionalidadsToAdd = funcionalidads.filter(funcionalidadItem => {
        const funcionalidadIdentifier = getFuncionalidadIdentifier(funcionalidadItem);
        if (funcionalidadIdentifier == null || funcionalidadCollectionIdentifiers.includes(funcionalidadIdentifier)) {
          return false;
        }
        funcionalidadCollectionIdentifiers.push(funcionalidadIdentifier);
        return true;
      });
      return [...funcionalidadsToAdd, ...funcionalidadCollection];
    }
    return funcionalidadCollection;
  }
}
