jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IFuncionalidad, Funcionalidad } from '../funcionalidad.model';
import { FuncionalidadService } from '../service/funcionalidad.service';

import { FuncionalidadRoutingResolveService } from './funcionalidad-routing-resolve.service';

describe('Funcionalidad routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: FuncionalidadRoutingResolveService;
  let service: FuncionalidadService;
  let resultFuncionalidad: IFuncionalidad | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(FuncionalidadRoutingResolveService);
    service = TestBed.inject(FuncionalidadService);
    resultFuncionalidad = undefined;
  });

  describe('resolve', () => {
    it('should return IFuncionalidad returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultFuncionalidad = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultFuncionalidad).toEqual({ id: 123 });
    });

    it('should return new IFuncionalidad if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultFuncionalidad = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultFuncionalidad).toEqual(new Funcionalidad());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as Funcionalidad })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultFuncionalidad = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultFuncionalidad).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
