import { IRegistroHistoria } from 'app/entities/registro-historia/registro-historia.model';
import { TipoEstadoFuncionalidad } from 'app/entities/enumerations/tipo-estado-funcionalidad.model';

export interface IFuncionalidad {
  id?: number;
  nombre?: string | null;
  estado?: TipoEstadoFuncionalidad | null;
  prioridad?: number | null;
  registroHistorias?: IRegistroHistoria[] | null;
}

export class Funcionalidad implements IFuncionalidad {
  constructor(
    public id?: number,
    public nombre?: string | null,
    public estado?: TipoEstadoFuncionalidad | null,
    public prioridad?: number | null,
    public registroHistorias?: IRegistroHistoria[] | null
  ) {}
}

export function getFuncionalidadIdentifier(funcionalidad: IFuncionalidad): number | undefined {
  return funcionalidad.id;
}
