export enum TipoCargo {
  ISW = 'ISW',

  SM = 'SM',

  ARQ = 'ARQ',

  UIUX = 'UIUX',
}
