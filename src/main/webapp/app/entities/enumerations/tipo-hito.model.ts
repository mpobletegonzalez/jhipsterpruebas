export enum TipoHito {
  RLS = 'RLS',

  PRD = 'PRD',

  QA = 'QA',

  DEMO = 'DEMO',
}
