export enum TipoEstadoFuncionalidad {
  ORIGINAL = 'ORIGINAL',

  VIGENTE = 'VIGENTE',

  CANCELADA = 'CANCELADA',

  CONGELADA = 'CONGELADA',
}
