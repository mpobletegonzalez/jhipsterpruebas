export enum EstadoServicio {
  ACTIVO = 'ACTIVO',

  INACTIVO = 'INACTIVO',

  CONGELADO = 'CONGELADO',

  TERMINADO = 'TERMINADO',
}
