import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { RegistroHistoriaComponent } from './list/registro-historia.component';
import { RegistroHistoriaDetailComponent } from './detail/registro-historia-detail.component';
import { RegistroHistoriaUpdateComponent } from './update/registro-historia-update.component';
import { RegistroHistoriaDeleteDialogComponent } from './delete/registro-historia-delete-dialog.component';
import { RegistroHistoriaRoutingModule } from './route/registro-historia-routing.module';

@NgModule({
  imports: [SharedModule, RegistroHistoriaRoutingModule],
  declarations: [
    RegistroHistoriaComponent,
    RegistroHistoriaDetailComponent,
    RegistroHistoriaUpdateComponent,
    RegistroHistoriaDeleteDialogComponent,
  ],
  entryComponents: [RegistroHistoriaDeleteDialogComponent],
})
export class RegistroHistoriaModule {}
