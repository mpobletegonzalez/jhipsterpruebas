import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IRegistroHistoria, RegistroHistoria } from '../registro-historia.model';
import { RegistroHistoriaService } from '../service/registro-historia.service';
import { IFuncionalidad } from 'app/entities/funcionalidad/funcionalidad.model';
import { FuncionalidadService } from 'app/entities/funcionalidad/service/funcionalidad.service';

@Component({
  selector: 'upgrade-registro-historia-update',
  templateUrl: './registro-historia-update.component.html',
})
export class RegistroHistoriaUpdateComponent implements OnInit {
  isSaving = false;

  funcionalidadsSharedCollection: IFuncionalidad[] = [];

  editForm = this.fb.group({
    id: [],
    nombre: [],
    codigo: [],
    funcionalidad: [],
  });

  constructor(
    protected registroHistoriaService: RegistroHistoriaService,
    protected funcionalidadService: FuncionalidadService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ registroHistoria }) => {
      this.updateForm(registroHistoria);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const registroHistoria = this.createFromForm();
    if (registroHistoria.id !== undefined) {
      this.subscribeToSaveResponse(this.registroHistoriaService.update(registroHistoria));
    } else {
      this.subscribeToSaveResponse(this.registroHistoriaService.create(registroHistoria));
    }
  }

  trackFuncionalidadById(index: number, item: IFuncionalidad): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRegistroHistoria>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(registroHistoria: IRegistroHistoria): void {
    this.editForm.patchValue({
      id: registroHistoria.id,
      nombre: registroHistoria.nombre,
      codigo: registroHistoria.codigo,
      funcionalidad: registroHistoria.funcionalidad,
    });

    this.funcionalidadsSharedCollection = this.funcionalidadService.addFuncionalidadToCollectionIfMissing(
      this.funcionalidadsSharedCollection,
      registroHistoria.funcionalidad
    );
  }

  protected loadRelationshipsOptions(): void {
    this.funcionalidadService
      .query()
      .pipe(map((res: HttpResponse<IFuncionalidad[]>) => res.body ?? []))
      .pipe(
        map((funcionalidads: IFuncionalidad[]) =>
          this.funcionalidadService.addFuncionalidadToCollectionIfMissing(funcionalidads, this.editForm.get('funcionalidad')!.value)
        )
      )
      .subscribe((funcionalidads: IFuncionalidad[]) => (this.funcionalidadsSharedCollection = funcionalidads));
  }

  protected createFromForm(): IRegistroHistoria {
    return {
      ...new RegistroHistoria(),
      id: this.editForm.get(['id'])!.value,
      nombre: this.editForm.get(['nombre'])!.value,
      codigo: this.editForm.get(['codigo'])!.value,
      funcionalidad: this.editForm.get(['funcionalidad'])!.value,
    };
  }
}
