jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { RegistroHistoriaService } from '../service/registro-historia.service';
import { IRegistroHistoria, RegistroHistoria } from '../registro-historia.model';
import { IFuncionalidad } from 'app/entities/funcionalidad/funcionalidad.model';
import { FuncionalidadService } from 'app/entities/funcionalidad/service/funcionalidad.service';

import { RegistroHistoriaUpdateComponent } from './registro-historia-update.component';

describe('RegistroHistoria Management Update Component', () => {
  let comp: RegistroHistoriaUpdateComponent;
  let fixture: ComponentFixture<RegistroHistoriaUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let registroHistoriaService: RegistroHistoriaService;
  let funcionalidadService: FuncionalidadService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [RegistroHistoriaUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(RegistroHistoriaUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(RegistroHistoriaUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    registroHistoriaService = TestBed.inject(RegistroHistoriaService);
    funcionalidadService = TestBed.inject(FuncionalidadService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Funcionalidad query and add missing value', () => {
      const registroHistoria: IRegistroHistoria = { id: 456 };
      const funcionalidad: IFuncionalidad = { id: 7045 };
      registroHistoria.funcionalidad = funcionalidad;

      const funcionalidadCollection: IFuncionalidad[] = [{ id: 50434 }];
      jest.spyOn(funcionalidadService, 'query').mockReturnValue(of(new HttpResponse({ body: funcionalidadCollection })));
      const additionalFuncionalidads = [funcionalidad];
      const expectedCollection: IFuncionalidad[] = [...additionalFuncionalidads, ...funcionalidadCollection];
      jest.spyOn(funcionalidadService, 'addFuncionalidadToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ registroHistoria });
      comp.ngOnInit();

      expect(funcionalidadService.query).toHaveBeenCalled();
      expect(funcionalidadService.addFuncionalidadToCollectionIfMissing).toHaveBeenCalledWith(
        funcionalidadCollection,
        ...additionalFuncionalidads
      );
      expect(comp.funcionalidadsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const registroHistoria: IRegistroHistoria = { id: 456 };
      const funcionalidad: IFuncionalidad = { id: 46588 };
      registroHistoria.funcionalidad = funcionalidad;

      activatedRoute.data = of({ registroHistoria });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(registroHistoria));
      expect(comp.funcionalidadsSharedCollection).toContain(funcionalidad);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<RegistroHistoria>>();
      const registroHistoria = { id: 123 };
      jest.spyOn(registroHistoriaService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ registroHistoria });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: registroHistoria }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(registroHistoriaService.update).toHaveBeenCalledWith(registroHistoria);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<RegistroHistoria>>();
      const registroHistoria = new RegistroHistoria();
      jest.spyOn(registroHistoriaService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ registroHistoria });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: registroHistoria }));
      saveSubject.complete();

      // THEN
      expect(registroHistoriaService.create).toHaveBeenCalledWith(registroHistoria);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<RegistroHistoria>>();
      const registroHistoria = { id: 123 };
      jest.spyOn(registroHistoriaService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ registroHistoria });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(registroHistoriaService.update).toHaveBeenCalledWith(registroHistoria);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackFuncionalidadById', () => {
      it('Should return tracked Funcionalidad primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackFuncionalidadById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
