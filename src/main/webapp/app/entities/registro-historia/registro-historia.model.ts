import { IFuncionalidad } from 'app/entities/funcionalidad/funcionalidad.model';

export interface IRegistroHistoria {
  id?: number;
  nombre?: string | null;
  codigo?: number | null;
  funcionalidad?: IFuncionalidad | null;
}

export class RegistroHistoria implements IRegistroHistoria {
  constructor(
    public id?: number,
    public nombre?: string | null,
    public codigo?: number | null,
    public funcionalidad?: IFuncionalidad | null
  ) {}
}

export function getRegistroHistoriaIdentifier(registroHistoria: IRegistroHistoria): number | undefined {
  return registroHistoria.id;
}
