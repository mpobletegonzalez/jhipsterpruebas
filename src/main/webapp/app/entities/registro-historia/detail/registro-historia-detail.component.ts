import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IRegistroHistoria } from '../registro-historia.model';

@Component({
  selector: 'upgrade-registro-historia-detail',
  templateUrl: './registro-historia-detail.component.html',
})
export class RegistroHistoriaDetailComponent implements OnInit {
  registroHistoria: IRegistroHistoria | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ registroHistoria }) => {
      this.registroHistoria = registroHistoria;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
