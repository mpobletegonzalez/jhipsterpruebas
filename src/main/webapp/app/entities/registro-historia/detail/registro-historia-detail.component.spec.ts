import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { RegistroHistoriaDetailComponent } from './registro-historia-detail.component';

describe('RegistroHistoria Management Detail Component', () => {
  let comp: RegistroHistoriaDetailComponent;
  let fixture: ComponentFixture<RegistroHistoriaDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [RegistroHistoriaDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ registroHistoria: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(RegistroHistoriaDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(RegistroHistoriaDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load registroHistoria on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.registroHistoria).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
