import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IRegistroHistoria } from '../registro-historia.model';
import { RegistroHistoriaService } from '../service/registro-historia.service';

@Component({
  templateUrl: './registro-historia-delete-dialog.component.html',
})
export class RegistroHistoriaDeleteDialogComponent {
  registroHistoria?: IRegistroHistoria;

  constructor(protected registroHistoriaService: RegistroHistoriaService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.registroHistoriaService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
