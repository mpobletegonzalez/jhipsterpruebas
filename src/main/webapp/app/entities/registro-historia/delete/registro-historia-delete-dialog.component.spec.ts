jest.mock('@ng-bootstrap/ng-bootstrap');

import { ComponentFixture, TestBed, inject, fakeAsync, tick } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { RegistroHistoriaService } from '../service/registro-historia.service';

import { RegistroHistoriaDeleteDialogComponent } from './registro-historia-delete-dialog.component';

describe('RegistroHistoria Management Delete Component', () => {
  let comp: RegistroHistoriaDeleteDialogComponent;
  let fixture: ComponentFixture<RegistroHistoriaDeleteDialogComponent>;
  let service: RegistroHistoriaService;
  let mockActiveModal: NgbActiveModal;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [RegistroHistoriaDeleteDialogComponent],
      providers: [NgbActiveModal],
    })
      .overrideTemplate(RegistroHistoriaDeleteDialogComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(RegistroHistoriaDeleteDialogComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(RegistroHistoriaService);
    mockActiveModal = TestBed.inject(NgbActiveModal);
  });

  describe('confirmDelete', () => {
    it('Should call delete service on confirmDelete', inject(
      [],
      fakeAsync(() => {
        // GIVEN
        jest.spyOn(service, 'delete').mockReturnValue(of(new HttpResponse({})));

        // WHEN
        comp.confirmDelete(123);
        tick();

        // THEN
        expect(service.delete).toHaveBeenCalledWith(123);
        expect(mockActiveModal.close).toHaveBeenCalledWith('deleted');
      })
    ));

    it('Should not call delete service on clear', () => {
      // GIVEN
      jest.spyOn(service, 'delete');

      // WHEN
      comp.cancel();

      // THEN
      expect(service.delete).not.toHaveBeenCalled();
      expect(mockActiveModal.close).not.toHaveBeenCalled();
      expect(mockActiveModal.dismiss).toHaveBeenCalled();
    });
  });
});
