import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IRegistroHistoria, getRegistroHistoriaIdentifier } from '../registro-historia.model';

export type EntityResponseType = HttpResponse<IRegistroHistoria>;
export type EntityArrayResponseType = HttpResponse<IRegistroHistoria[]>;

@Injectable({ providedIn: 'root' })
export class RegistroHistoriaService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/registro-historias');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(registroHistoria: IRegistroHistoria): Observable<EntityResponseType> {
    return this.http.post<IRegistroHistoria>(this.resourceUrl, registroHistoria, { observe: 'response' });
  }

  update(registroHistoria: IRegistroHistoria): Observable<EntityResponseType> {
    return this.http.put<IRegistroHistoria>(
      `${this.resourceUrl}/${getRegistroHistoriaIdentifier(registroHistoria) as number}`,
      registroHistoria,
      { observe: 'response' }
    );
  }

  partialUpdate(registroHistoria: IRegistroHistoria): Observable<EntityResponseType> {
    return this.http.patch<IRegistroHistoria>(
      `${this.resourceUrl}/${getRegistroHistoriaIdentifier(registroHistoria) as number}`,
      registroHistoria,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IRegistroHistoria>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IRegistroHistoria[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addRegistroHistoriaToCollectionIfMissing(
    registroHistoriaCollection: IRegistroHistoria[],
    ...registroHistoriasToCheck: (IRegistroHistoria | null | undefined)[]
  ): IRegistroHistoria[] {
    const registroHistorias: IRegistroHistoria[] = registroHistoriasToCheck.filter(isPresent);
    if (registroHistorias.length > 0) {
      const registroHistoriaCollectionIdentifiers = registroHistoriaCollection.map(
        registroHistoriaItem => getRegistroHistoriaIdentifier(registroHistoriaItem)!
      );
      const registroHistoriasToAdd = registroHistorias.filter(registroHistoriaItem => {
        const registroHistoriaIdentifier = getRegistroHistoriaIdentifier(registroHistoriaItem);
        if (registroHistoriaIdentifier == null || registroHistoriaCollectionIdentifiers.includes(registroHistoriaIdentifier)) {
          return false;
        }
        registroHistoriaCollectionIdentifiers.push(registroHistoriaIdentifier);
        return true;
      });
      return [...registroHistoriasToAdd, ...registroHistoriaCollection];
    }
    return registroHistoriaCollection;
  }
}
