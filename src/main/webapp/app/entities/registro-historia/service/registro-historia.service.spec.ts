import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IRegistroHistoria, RegistroHistoria } from '../registro-historia.model';

import { RegistroHistoriaService } from './registro-historia.service';

describe('RegistroHistoria Service', () => {
  let service: RegistroHistoriaService;
  let httpMock: HttpTestingController;
  let elemDefault: IRegistroHistoria;
  let expectedResult: IRegistroHistoria | IRegistroHistoria[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(RegistroHistoriaService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      nombre: 'AAAAAAA',
      codigo: 0,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a RegistroHistoria', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new RegistroHistoria()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a RegistroHistoria', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nombre: 'BBBBBB',
          codigo: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a RegistroHistoria', () => {
      const patchObject = Object.assign(
        {
          nombre: 'BBBBBB',
        },
        new RegistroHistoria()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of RegistroHistoria', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nombre: 'BBBBBB',
          codigo: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a RegistroHistoria', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addRegistroHistoriaToCollectionIfMissing', () => {
      it('should add a RegistroHistoria to an empty array', () => {
        const registroHistoria: IRegistroHistoria = { id: 123 };
        expectedResult = service.addRegistroHistoriaToCollectionIfMissing([], registroHistoria);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(registroHistoria);
      });

      it('should not add a RegistroHistoria to an array that contains it', () => {
        const registroHistoria: IRegistroHistoria = { id: 123 };
        const registroHistoriaCollection: IRegistroHistoria[] = [
          {
            ...registroHistoria,
          },
          { id: 456 },
        ];
        expectedResult = service.addRegistroHistoriaToCollectionIfMissing(registroHistoriaCollection, registroHistoria);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a RegistroHistoria to an array that doesn't contain it", () => {
        const registroHistoria: IRegistroHistoria = { id: 123 };
        const registroHistoriaCollection: IRegistroHistoria[] = [{ id: 456 }];
        expectedResult = service.addRegistroHistoriaToCollectionIfMissing(registroHistoriaCollection, registroHistoria);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(registroHistoria);
      });

      it('should add only unique RegistroHistoria to an array', () => {
        const registroHistoriaArray: IRegistroHistoria[] = [{ id: 123 }, { id: 456 }, { id: 86460 }];
        const registroHistoriaCollection: IRegistroHistoria[] = [{ id: 123 }];
        expectedResult = service.addRegistroHistoriaToCollectionIfMissing(registroHistoriaCollection, ...registroHistoriaArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const registroHistoria: IRegistroHistoria = { id: 123 };
        const registroHistoria2: IRegistroHistoria = { id: 456 };
        expectedResult = service.addRegistroHistoriaToCollectionIfMissing([], registroHistoria, registroHistoria2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(registroHistoria);
        expect(expectedResult).toContain(registroHistoria2);
      });

      it('should accept null and undefined values', () => {
        const registroHistoria: IRegistroHistoria = { id: 123 };
        expectedResult = service.addRegistroHistoriaToCollectionIfMissing([], null, registroHistoria, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(registroHistoria);
      });

      it('should return initial array if no RegistroHistoria is added', () => {
        const registroHistoriaCollection: IRegistroHistoria[] = [{ id: 123 }];
        expectedResult = service.addRegistroHistoriaToCollectionIfMissing(registroHistoriaCollection, undefined, null);
        expect(expectedResult).toEqual(registroHistoriaCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
