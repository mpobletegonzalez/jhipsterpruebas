jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IRegistroHistoria, RegistroHistoria } from '../registro-historia.model';
import { RegistroHistoriaService } from '../service/registro-historia.service';

import { RegistroHistoriaRoutingResolveService } from './registro-historia-routing-resolve.service';

describe('RegistroHistoria routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: RegistroHistoriaRoutingResolveService;
  let service: RegistroHistoriaService;
  let resultRegistroHistoria: IRegistroHistoria | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(RegistroHistoriaRoutingResolveService);
    service = TestBed.inject(RegistroHistoriaService);
    resultRegistroHistoria = undefined;
  });

  describe('resolve', () => {
    it('should return IRegistroHistoria returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultRegistroHistoria = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultRegistroHistoria).toEqual({ id: 123 });
    });

    it('should return new IRegistroHistoria if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultRegistroHistoria = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultRegistroHistoria).toEqual(new RegistroHistoria());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as RegistroHistoria })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultRegistroHistoria = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultRegistroHistoria).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
