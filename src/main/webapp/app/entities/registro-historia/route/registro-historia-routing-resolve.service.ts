import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IRegistroHistoria, RegistroHistoria } from '../registro-historia.model';
import { RegistroHistoriaService } from '../service/registro-historia.service';

@Injectable({ providedIn: 'root' })
export class RegistroHistoriaRoutingResolveService implements Resolve<IRegistroHistoria> {
  constructor(protected service: RegistroHistoriaService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IRegistroHistoria> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((registroHistoria: HttpResponse<RegistroHistoria>) => {
          if (registroHistoria.body) {
            return of(registroHistoria.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new RegistroHistoria());
  }
}
