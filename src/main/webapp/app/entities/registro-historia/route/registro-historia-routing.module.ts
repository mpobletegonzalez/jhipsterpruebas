import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { RegistroHistoriaComponent } from '../list/registro-historia.component';
import { RegistroHistoriaDetailComponent } from '../detail/registro-historia-detail.component';
import { RegistroHistoriaUpdateComponent } from '../update/registro-historia-update.component';
import { RegistroHistoriaRoutingResolveService } from './registro-historia-routing-resolve.service';

const registroHistoriaRoute: Routes = [
  {
    path: '',
    component: RegistroHistoriaComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: RegistroHistoriaDetailComponent,
    resolve: {
      registroHistoria: RegistroHistoriaRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: RegistroHistoriaUpdateComponent,
    resolve: {
      registroHistoria: RegistroHistoriaRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: RegistroHistoriaUpdateComponent,
    resolve: {
      registroHistoria: RegistroHistoriaRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(registroHistoriaRoute)],
  exports: [RouterModule],
})
export class RegistroHistoriaRoutingModule {}
