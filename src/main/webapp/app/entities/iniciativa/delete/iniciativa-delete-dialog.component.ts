import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IIniciativa } from '../iniciativa.model';
import { IniciativaService } from '../service/iniciativa.service';

@Component({
  templateUrl: './iniciativa-delete-dialog.component.html',
})
export class IniciativaDeleteDialogComponent {
  iniciativa?: IIniciativa;

  constructor(protected iniciativaService: IniciativaService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.iniciativaService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
