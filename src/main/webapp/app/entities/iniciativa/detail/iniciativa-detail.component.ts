import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IIniciativa } from '../iniciativa.model';

@Component({
  selector: 'upgrade-iniciativa-detail',
  templateUrl: './iniciativa-detail.component.html',
})
export class IniciativaDetailComponent implements OnInit {
  iniciativa: IIniciativa | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ iniciativa }) => {
      this.iniciativa = iniciativa;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
