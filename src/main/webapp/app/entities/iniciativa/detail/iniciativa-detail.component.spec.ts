import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { IniciativaDetailComponent } from './iniciativa-detail.component';

describe('Iniciativa Management Detail Component', () => {
  let comp: IniciativaDetailComponent;
  let fixture: ComponentFixture<IniciativaDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [IniciativaDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ iniciativa: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(IniciativaDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(IniciativaDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load iniciativa on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.iniciativa).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
