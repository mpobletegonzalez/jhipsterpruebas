import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import * as dayjs from 'dayjs';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IIniciativa, Iniciativa } from '../iniciativa.model';
import { IniciativaService } from '../service/iniciativa.service';
import { IServicio } from 'app/entities/servicio/servicio.model';
import { ServicioService } from 'app/entities/servicio/service/servicio.service';

@Component({
  selector: 'upgrade-iniciativa-update',
  templateUrl: './iniciativa-update.component.html',
})
export class IniciativaUpdateComponent implements OnInit {
  isSaving = false;

  serviciosSharedCollection: IServicio[] = [];

  editForm = this.fb.group({
    id: [],
    nombre: [null, [Validators.required]],
    fechaInicio: [null, [Validators.required]],
    fechaFin: [null, [Validators.required]],
    fechaCreacion: [null, [Validators.required]],
    isActivo: [null, [Validators.required]],
    promesa: [null, [Validators.required]],
    tituloProductoDigital: [null, [Validators.required]],
    descripcionProductoDigital: [null, [Validators.required]],
    servicio: [],
  });

  constructor(
    protected iniciativaService: IniciativaService,
    protected servicioService: ServicioService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ iniciativa }) => {
      if (iniciativa.id === undefined) {
        const today = dayjs().startOf('day');
        iniciativa.fechaInicio = today;
        iniciativa.fechaFin = today;
        iniciativa.fechaCreacion = today;
      }

      this.updateForm(iniciativa);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const iniciativa = this.createFromForm();
    if (iniciativa.id !== undefined) {
      this.subscribeToSaveResponse(this.iniciativaService.update(iniciativa));
    } else {
      this.subscribeToSaveResponse(this.iniciativaService.create(iniciativa));
    }
  }

  trackServicioById(index: number, item: IServicio): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IIniciativa>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(iniciativa: IIniciativa): void {
    this.editForm.patchValue({
      id: iniciativa.id,
      nombre: iniciativa.nombre,
      fechaInicio: iniciativa.fechaInicio ? iniciativa.fechaInicio.format(DATE_TIME_FORMAT) : null,
      fechaFin: iniciativa.fechaFin ? iniciativa.fechaFin.format(DATE_TIME_FORMAT) : null,
      fechaCreacion: iniciativa.fechaCreacion ? iniciativa.fechaCreacion.format(DATE_TIME_FORMAT) : null,
      isActivo: iniciativa.isActivo,
      promesa: iniciativa.promesa,
      tituloProductoDigital: iniciativa.tituloProductoDigital,
      descripcionProductoDigital: iniciativa.descripcionProductoDigital,
      servicio: iniciativa.servicio,
    });

    this.serviciosSharedCollection = this.servicioService.addServicioToCollectionIfMissing(
      this.serviciosSharedCollection,
      iniciativa.servicio
    );
  }

  protected loadRelationshipsOptions(): void {
    this.servicioService
      .query()
      .pipe(map((res: HttpResponse<IServicio[]>) => res.body ?? []))
      .pipe(
        map((servicios: IServicio[]) =>
          this.servicioService.addServicioToCollectionIfMissing(servicios, this.editForm.get('servicio')!.value)
        )
      )
      .subscribe((servicios: IServicio[]) => (this.serviciosSharedCollection = servicios));
  }

  protected createFromForm(): IIniciativa {
    return {
      ...new Iniciativa(),
      id: this.editForm.get(['id'])!.value,
      nombre: this.editForm.get(['nombre'])!.value,
      fechaInicio: this.editForm.get(['fechaInicio'])!.value
        ? dayjs(this.editForm.get(['fechaInicio'])!.value, DATE_TIME_FORMAT)
        : undefined,
      fechaFin: this.editForm.get(['fechaFin'])!.value ? dayjs(this.editForm.get(['fechaFin'])!.value, DATE_TIME_FORMAT) : undefined,
      fechaCreacion: this.editForm.get(['fechaCreacion'])!.value
        ? dayjs(this.editForm.get(['fechaCreacion'])!.value, DATE_TIME_FORMAT)
        : undefined,
      isActivo: this.editForm.get(['isActivo'])!.value,
      promesa: this.editForm.get(['promesa'])!.value,
      tituloProductoDigital: this.editForm.get(['tituloProductoDigital'])!.value,
      descripcionProductoDigital: this.editForm.get(['descripcionProductoDigital'])!.value,
      servicio: this.editForm.get(['servicio'])!.value,
    };
  }
}
