jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { IniciativaService } from '../service/iniciativa.service';
import { IIniciativa, Iniciativa } from '../iniciativa.model';
import { IServicio } from 'app/entities/servicio/servicio.model';
import { ServicioService } from 'app/entities/servicio/service/servicio.service';

import { IniciativaUpdateComponent } from './iniciativa-update.component';

describe('Iniciativa Management Update Component', () => {
  let comp: IniciativaUpdateComponent;
  let fixture: ComponentFixture<IniciativaUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let iniciativaService: IniciativaService;
  let servicioService: ServicioService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [IniciativaUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(IniciativaUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(IniciativaUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    iniciativaService = TestBed.inject(IniciativaService);
    servicioService = TestBed.inject(ServicioService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Servicio query and add missing value', () => {
      const iniciativa: IIniciativa = { id: 456 };
      const servicio: IServicio = { id: 3426 };
      iniciativa.servicio = servicio;

      const servicioCollection: IServicio[] = [{ id: 54748 }];
      jest.spyOn(servicioService, 'query').mockReturnValue(of(new HttpResponse({ body: servicioCollection })));
      const additionalServicios = [servicio];
      const expectedCollection: IServicio[] = [...additionalServicios, ...servicioCollection];
      jest.spyOn(servicioService, 'addServicioToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ iniciativa });
      comp.ngOnInit();

      expect(servicioService.query).toHaveBeenCalled();
      expect(servicioService.addServicioToCollectionIfMissing).toHaveBeenCalledWith(servicioCollection, ...additionalServicios);
      expect(comp.serviciosSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const iniciativa: IIniciativa = { id: 456 };
      const servicio: IServicio = { id: 68700 };
      iniciativa.servicio = servicio;

      activatedRoute.data = of({ iniciativa });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(iniciativa));
      expect(comp.serviciosSharedCollection).toContain(servicio);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Iniciativa>>();
      const iniciativa = { id: 123 };
      jest.spyOn(iniciativaService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ iniciativa });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: iniciativa }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(iniciativaService.update).toHaveBeenCalledWith(iniciativa);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Iniciativa>>();
      const iniciativa = new Iniciativa();
      jest.spyOn(iniciativaService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ iniciativa });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: iniciativa }));
      saveSubject.complete();

      // THEN
      expect(iniciativaService.create).toHaveBeenCalledWith(iniciativa);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Iniciativa>>();
      const iniciativa = { id: 123 };
      jest.spyOn(iniciativaService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ iniciativa });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(iniciativaService.update).toHaveBeenCalledWith(iniciativa);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackServicioById', () => {
      it('Should return tracked Servicio primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackServicioById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
