import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { IniciativaComponent } from './list/iniciativa.component';
import { IniciativaDetailComponent } from './detail/iniciativa-detail.component';
import { IniciativaUpdateComponent } from './update/iniciativa-update.component';
import { IniciativaDeleteDialogComponent } from './delete/iniciativa-delete-dialog.component';
import { IniciativaRoutingModule } from './route/iniciativa-routing.module';

@NgModule({
  imports: [SharedModule, IniciativaRoutingModule],
  declarations: [IniciativaComponent, IniciativaDetailComponent, IniciativaUpdateComponent, IniciativaDeleteDialogComponent],
  entryComponents: [IniciativaDeleteDialogComponent],
})
export class IniciativaModule {}
