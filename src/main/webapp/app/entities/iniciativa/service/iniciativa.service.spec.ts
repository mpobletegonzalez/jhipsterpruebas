import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import * as dayjs from 'dayjs';

import { DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IIniciativa, Iniciativa } from '../iniciativa.model';

import { IniciativaService } from './iniciativa.service';

describe('Iniciativa Service', () => {
  let service: IniciativaService;
  let httpMock: HttpTestingController;
  let elemDefault: IIniciativa;
  let expectedResult: IIniciativa | IIniciativa[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(IniciativaService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      nombre: 'AAAAAAA',
      fechaInicio: currentDate,
      fechaFin: currentDate,
      fechaCreacion: currentDate,
      isActivo: false,
      promesa: 'AAAAAAA',
      tituloProductoDigital: 'AAAAAAA',
      descripcionProductoDigital: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          fechaInicio: currentDate.format(DATE_TIME_FORMAT),
          fechaFin: currentDate.format(DATE_TIME_FORMAT),
          fechaCreacion: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Iniciativa', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          fechaInicio: currentDate.format(DATE_TIME_FORMAT),
          fechaFin: currentDate.format(DATE_TIME_FORMAT),
          fechaCreacion: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          fechaInicio: currentDate,
          fechaFin: currentDate,
          fechaCreacion: currentDate,
        },
        returnedFromService
      );

      service.create(new Iniciativa()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Iniciativa', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nombre: 'BBBBBB',
          fechaInicio: currentDate.format(DATE_TIME_FORMAT),
          fechaFin: currentDate.format(DATE_TIME_FORMAT),
          fechaCreacion: currentDate.format(DATE_TIME_FORMAT),
          isActivo: true,
          promesa: 'BBBBBB',
          tituloProductoDigital: 'BBBBBB',
          descripcionProductoDigital: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          fechaInicio: currentDate,
          fechaFin: currentDate,
          fechaCreacion: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Iniciativa', () => {
      const patchObject = Object.assign(
        {
          fechaInicio: currentDate.format(DATE_TIME_FORMAT),
          fechaFin: currentDate.format(DATE_TIME_FORMAT),
          descripcionProductoDigital: 'BBBBBB',
        },
        new Iniciativa()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          fechaInicio: currentDate,
          fechaFin: currentDate,
          fechaCreacion: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Iniciativa', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          nombre: 'BBBBBB',
          fechaInicio: currentDate.format(DATE_TIME_FORMAT),
          fechaFin: currentDate.format(DATE_TIME_FORMAT),
          fechaCreacion: currentDate.format(DATE_TIME_FORMAT),
          isActivo: true,
          promesa: 'BBBBBB',
          tituloProductoDigital: 'BBBBBB',
          descripcionProductoDigital: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          fechaInicio: currentDate,
          fechaFin: currentDate,
          fechaCreacion: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Iniciativa', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addIniciativaToCollectionIfMissing', () => {
      it('should add a Iniciativa to an empty array', () => {
        const iniciativa: IIniciativa = { id: 123 };
        expectedResult = service.addIniciativaToCollectionIfMissing([], iniciativa);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(iniciativa);
      });

      it('should not add a Iniciativa to an array that contains it', () => {
        const iniciativa: IIniciativa = { id: 123 };
        const iniciativaCollection: IIniciativa[] = [
          {
            ...iniciativa,
          },
          { id: 456 },
        ];
        expectedResult = service.addIniciativaToCollectionIfMissing(iniciativaCollection, iniciativa);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Iniciativa to an array that doesn't contain it", () => {
        const iniciativa: IIniciativa = { id: 123 };
        const iniciativaCollection: IIniciativa[] = [{ id: 456 }];
        expectedResult = service.addIniciativaToCollectionIfMissing(iniciativaCollection, iniciativa);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(iniciativa);
      });

      it('should add only unique Iniciativa to an array', () => {
        const iniciativaArray: IIniciativa[] = [{ id: 123 }, { id: 456 }, { id: 55716 }];
        const iniciativaCollection: IIniciativa[] = [{ id: 123 }];
        expectedResult = service.addIniciativaToCollectionIfMissing(iniciativaCollection, ...iniciativaArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const iniciativa: IIniciativa = { id: 123 };
        const iniciativa2: IIniciativa = { id: 456 };
        expectedResult = service.addIniciativaToCollectionIfMissing([], iniciativa, iniciativa2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(iniciativa);
        expect(expectedResult).toContain(iniciativa2);
      });

      it('should accept null and undefined values', () => {
        const iniciativa: IIniciativa = { id: 123 };
        expectedResult = service.addIniciativaToCollectionIfMissing([], null, iniciativa, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(iniciativa);
      });

      it('should return initial array if no Iniciativa is added', () => {
        const iniciativaCollection: IIniciativa[] = [{ id: 123 }];
        expectedResult = service.addIniciativaToCollectionIfMissing(iniciativaCollection, undefined, null);
        expect(expectedResult).toEqual(iniciativaCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
