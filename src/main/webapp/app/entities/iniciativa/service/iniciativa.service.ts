import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import * as dayjs from 'dayjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IIniciativa, getIniciativaIdentifier } from '../iniciativa.model';

export type EntityResponseType = HttpResponse<IIniciativa>;
export type EntityArrayResponseType = HttpResponse<IIniciativa[]>;

@Injectable({ providedIn: 'root' })
export class IniciativaService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/iniciativas');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(iniciativa: IIniciativa): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(iniciativa);
    return this.http
      .post<IIniciativa>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(iniciativa: IIniciativa): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(iniciativa);
    return this.http
      .put<IIniciativa>(`${this.resourceUrl}/${getIniciativaIdentifier(iniciativa) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(iniciativa: IIniciativa): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(iniciativa);
    return this.http
      .patch<IIniciativa>(`${this.resourceUrl}/${getIniciativaIdentifier(iniciativa) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IIniciativa>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IIniciativa[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addIniciativaToCollectionIfMissing(
    iniciativaCollection: IIniciativa[],
    ...iniciativasToCheck: (IIniciativa | null | undefined)[]
  ): IIniciativa[] {
    const iniciativas: IIniciativa[] = iniciativasToCheck.filter(isPresent);
    if (iniciativas.length > 0) {
      const iniciativaCollectionIdentifiers = iniciativaCollection.map(iniciativaItem => getIniciativaIdentifier(iniciativaItem)!);
      const iniciativasToAdd = iniciativas.filter(iniciativaItem => {
        const iniciativaIdentifier = getIniciativaIdentifier(iniciativaItem);
        if (iniciativaIdentifier == null || iniciativaCollectionIdentifiers.includes(iniciativaIdentifier)) {
          return false;
        }
        iniciativaCollectionIdentifiers.push(iniciativaIdentifier);
        return true;
      });
      return [...iniciativasToAdd, ...iniciativaCollection];
    }
    return iniciativaCollection;
  }

  protected convertDateFromClient(iniciativa: IIniciativa): IIniciativa {
    return Object.assign({}, iniciativa, {
      fechaInicio: iniciativa.fechaInicio?.isValid() ? iniciativa.fechaInicio.toJSON() : undefined,
      fechaFin: iniciativa.fechaFin?.isValid() ? iniciativa.fechaFin.toJSON() : undefined,
      fechaCreacion: iniciativa.fechaCreacion?.isValid() ? iniciativa.fechaCreacion.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.fechaInicio = res.body.fechaInicio ? dayjs(res.body.fechaInicio) : undefined;
      res.body.fechaFin = res.body.fechaFin ? dayjs(res.body.fechaFin) : undefined;
      res.body.fechaCreacion = res.body.fechaCreacion ? dayjs(res.body.fechaCreacion) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((iniciativa: IIniciativa) => {
        iniciativa.fechaInicio = iniciativa.fechaInicio ? dayjs(iniciativa.fechaInicio) : undefined;
        iniciativa.fechaFin = iniciativa.fechaFin ? dayjs(iniciativa.fechaFin) : undefined;
        iniciativa.fechaCreacion = iniciativa.fechaCreacion ? dayjs(iniciativa.fechaCreacion) : undefined;
      });
    }
    return res;
  }
}
