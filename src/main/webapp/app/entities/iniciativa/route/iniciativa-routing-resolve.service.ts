import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IIniciativa, Iniciativa } from '../iniciativa.model';
import { IniciativaService } from '../service/iniciativa.service';

@Injectable({ providedIn: 'root' })
export class IniciativaRoutingResolveService implements Resolve<IIniciativa> {
  constructor(protected service: IniciativaService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IIniciativa> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((iniciativa: HttpResponse<Iniciativa>) => {
          if (iniciativa.body) {
            return of(iniciativa.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Iniciativa());
  }
}
