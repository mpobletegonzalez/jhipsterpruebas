import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { IniciativaComponent } from '../list/iniciativa.component';
import { IniciativaDetailComponent } from '../detail/iniciativa-detail.component';
import { IniciativaUpdateComponent } from '../update/iniciativa-update.component';
import { IniciativaRoutingResolveService } from './iniciativa-routing-resolve.service';

const iniciativaRoute: Routes = [
  {
    path: '',
    component: IniciativaComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: IniciativaDetailComponent,
    resolve: {
      iniciativa: IniciativaRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: IniciativaUpdateComponent,
    resolve: {
      iniciativa: IniciativaRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: IniciativaUpdateComponent,
    resolve: {
      iniciativa: IniciativaRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(iniciativaRoute)],
  exports: [RouterModule],
})
export class IniciativaRoutingModule {}
