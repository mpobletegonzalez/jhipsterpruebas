import * as dayjs from 'dayjs';
import { IHito } from 'app/entities/hito/hito.model';
import { IEstadosHistorias } from 'app/entities/estados-historias/estados-historias.model';
import { IAsignacion } from 'app/entities/asignacion/asignacion.model';
import { IServicio } from 'app/entities/servicio/servicio.model';

export interface IIniciativa {
  id?: number;
  nombre?: string;
  fechaInicio?: dayjs.Dayjs;
  fechaFin?: dayjs.Dayjs;
  fechaCreacion?: dayjs.Dayjs;
  isActivo?: boolean;
  promesa?: string;
  tituloProductoDigital?: string;
  descripcionProductoDigital?: string;
  hitos?: IHito[] | null;
  estadosHistorias?: IEstadosHistorias[] | null;
  asignacions?: IAsignacion[] | null;
  servicio?: IServicio | null;
}

export class Iniciativa implements IIniciativa {
  constructor(
    public id?: number,
    public nombre?: string,
    public fechaInicio?: dayjs.Dayjs,
    public fechaFin?: dayjs.Dayjs,
    public fechaCreacion?: dayjs.Dayjs,
    public isActivo?: boolean,
    public promesa?: string,
    public tituloProductoDigital?: string,
    public descripcionProductoDigital?: string,
    public hitos?: IHito[] | null,
    public estadosHistorias?: IEstadosHistorias[] | null,
    public asignacions?: IAsignacion[] | null,
    public servicio?: IServicio | null
  ) {
    this.isActivo = this.isActivo ?? false;
  }
}

export function getIniciativaIdentifier(iniciativa: IIniciativa): number | undefined {
  return iniciativa.id;
}
