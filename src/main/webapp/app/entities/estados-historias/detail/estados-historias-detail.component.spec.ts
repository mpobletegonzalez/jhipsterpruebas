import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { EstadosHistoriasDetailComponent } from './estados-historias-detail.component';

describe('EstadosHistorias Management Detail Component', () => {
  let comp: EstadosHistoriasDetailComponent;
  let fixture: ComponentFixture<EstadosHistoriasDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [EstadosHistoriasDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ estadosHistorias: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(EstadosHistoriasDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(EstadosHistoriasDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load estadosHistorias on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.estadosHistorias).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
