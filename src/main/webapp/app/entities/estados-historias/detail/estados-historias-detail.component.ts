import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IEstadosHistorias } from '../estados-historias.model';

@Component({
  selector: 'upgrade-estados-historias-detail',
  templateUrl: './estados-historias-detail.component.html',
})
export class EstadosHistoriasDetailComponent implements OnInit {
  estadosHistorias: IEstadosHistorias | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ estadosHistorias }) => {
      this.estadosHistorias = estadosHistorias;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
