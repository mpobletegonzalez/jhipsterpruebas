import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IEstadosHistorias, EstadosHistorias } from '../estados-historias.model';
import { EstadosHistoriasService } from '../service/estados-historias.service';

@Injectable({ providedIn: 'root' })
export class EstadosHistoriasRoutingResolveService implements Resolve<IEstadosHistorias> {
  constructor(protected service: EstadosHistoriasService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IEstadosHistorias> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((estadosHistorias: HttpResponse<EstadosHistorias>) => {
          if (estadosHistorias.body) {
            return of(estadosHistorias.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new EstadosHistorias());
  }
}
