jest.mock('@angular/router');

import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, Router } from '@angular/router';
import { of } from 'rxjs';

import { IEstadosHistorias, EstadosHistorias } from '../estados-historias.model';
import { EstadosHistoriasService } from '../service/estados-historias.service';

import { EstadosHistoriasRoutingResolveService } from './estados-historias-routing-resolve.service';

describe('EstadosHistorias routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: EstadosHistoriasRoutingResolveService;
  let service: EstadosHistoriasService;
  let resultEstadosHistorias: IEstadosHistorias | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [Router, ActivatedRouteSnapshot],
    });
    mockRouter = TestBed.inject(Router);
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRouteSnapshot);
    routingResolveService = TestBed.inject(EstadosHistoriasRoutingResolveService);
    service = TestBed.inject(EstadosHistoriasService);
    resultEstadosHistorias = undefined;
  });

  describe('resolve', () => {
    it('should return IEstadosHistorias returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultEstadosHistorias = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultEstadosHistorias).toEqual({ id: 123 });
    });

    it('should return new IEstadosHistorias if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultEstadosHistorias = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultEstadosHistorias).toEqual(new EstadosHistorias());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as EstadosHistorias })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultEstadosHistorias = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultEstadosHistorias).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
