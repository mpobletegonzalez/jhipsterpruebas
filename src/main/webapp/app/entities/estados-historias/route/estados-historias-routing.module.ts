import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { EstadosHistoriasComponent } from '../list/estados-historias.component';
import { EstadosHistoriasDetailComponent } from '../detail/estados-historias-detail.component';
import { EstadosHistoriasUpdateComponent } from '../update/estados-historias-update.component';
import { EstadosHistoriasRoutingResolveService } from './estados-historias-routing-resolve.service';

const estadosHistoriasRoute: Routes = [
  {
    path: '',
    component: EstadosHistoriasComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: EstadosHistoriasDetailComponent,
    resolve: {
      estadosHistorias: EstadosHistoriasRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: EstadosHistoriasUpdateComponent,
    resolve: {
      estadosHistorias: EstadosHistoriasRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: EstadosHistoriasUpdateComponent,
    resolve: {
      estadosHistorias: EstadosHistoriasRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(estadosHistoriasRoute)],
  exports: [RouterModule],
})
export class EstadosHistoriasRoutingModule {}
