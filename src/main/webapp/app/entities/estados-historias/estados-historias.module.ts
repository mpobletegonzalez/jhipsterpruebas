import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { EstadosHistoriasComponent } from './list/estados-historias.component';
import { EstadosHistoriasDetailComponent } from './detail/estados-historias-detail.component';
import { EstadosHistoriasUpdateComponent } from './update/estados-historias-update.component';
import { EstadosHistoriasDeleteDialogComponent } from './delete/estados-historias-delete-dialog.component';
import { EstadosHistoriasRoutingModule } from './route/estados-historias-routing.module';

@NgModule({
  imports: [SharedModule, EstadosHistoriasRoutingModule],
  declarations: [
    EstadosHistoriasComponent,
    EstadosHistoriasDetailComponent,
    EstadosHistoriasUpdateComponent,
    EstadosHistoriasDeleteDialogComponent,
  ],
  entryComponents: [EstadosHistoriasDeleteDialogComponent],
})
export class EstadosHistoriasModule {}
