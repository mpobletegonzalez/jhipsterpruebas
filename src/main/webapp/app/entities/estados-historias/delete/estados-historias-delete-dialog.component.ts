import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IEstadosHistorias } from '../estados-historias.model';
import { EstadosHistoriasService } from '../service/estados-historias.service';

@Component({
  templateUrl: './estados-historias-delete-dialog.component.html',
})
export class EstadosHistoriasDeleteDialogComponent {
  estadosHistorias?: IEstadosHistorias;

  constructor(protected estadosHistoriasService: EstadosHistoriasService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.estadosHistoriasService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
