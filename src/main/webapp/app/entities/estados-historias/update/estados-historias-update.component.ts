import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IEstadosHistorias, EstadosHistorias } from '../estados-historias.model';
import { EstadosHistoriasService } from '../service/estados-historias.service';
import { IIniciativa } from 'app/entities/iniciativa/iniciativa.model';
import { IniciativaService } from 'app/entities/iniciativa/service/iniciativa.service';

@Component({
  selector: 'upgrade-estados-historias-update',
  templateUrl: './estados-historias-update.component.html',
})
export class EstadosHistoriasUpdateComponent implements OnInit {
  isSaving = false;

  iniciativasSharedCollection: IIniciativa[] = [];

  editForm = this.fb.group({
    id: [],
    totalEstimacion: [],
    totalBacklog: [],
    totalEnRefinamiento: [],
    totalListoParaDesarrollo: [],
    totalEnDesarrollo: [],
    totalListoCertificacion: [],
    totalEnCertificacion: [],
    totalPruebasUAT: [],
    totalListoProduccion: [],
    totalEnProduccion: [],
    totalAprobadosPO: [],
    iniciativa: [],
  });

  constructor(
    protected estadosHistoriasService: EstadosHistoriasService,
    protected iniciativaService: IniciativaService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ estadosHistorias }) => {
      this.updateForm(estadosHistorias);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const estadosHistorias = this.createFromForm();
    if (estadosHistorias.id !== undefined) {
      this.subscribeToSaveResponse(this.estadosHistoriasService.update(estadosHistorias));
    } else {
      this.subscribeToSaveResponse(this.estadosHistoriasService.create(estadosHistorias));
    }
  }

  trackIniciativaById(index: number, item: IIniciativa): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IEstadosHistorias>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe(
      () => this.onSaveSuccess(),
      () => this.onSaveError()
    );
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(estadosHistorias: IEstadosHistorias): void {
    this.editForm.patchValue({
      id: estadosHistorias.id,
      totalEstimacion: estadosHistorias.totalEstimacion,
      totalBacklog: estadosHistorias.totalBacklog,
      totalEnRefinamiento: estadosHistorias.totalEnRefinamiento,
      totalListoParaDesarrollo: estadosHistorias.totalListoParaDesarrollo,
      totalEnDesarrollo: estadosHistorias.totalEnDesarrollo,
      totalListoCertificacion: estadosHistorias.totalListoCertificacion,
      totalEnCertificacion: estadosHistorias.totalEnCertificacion,
      totalPruebasUAT: estadosHistorias.totalPruebasUAT,
      totalListoProduccion: estadosHistorias.totalListoProduccion,
      totalEnProduccion: estadosHistorias.totalEnProduccion,
      totalAprobadosPO: estadosHistorias.totalAprobadosPO,
      iniciativa: estadosHistorias.iniciativa,
    });

    this.iniciativasSharedCollection = this.iniciativaService.addIniciativaToCollectionIfMissing(
      this.iniciativasSharedCollection,
      estadosHistorias.iniciativa
    );
  }

  protected loadRelationshipsOptions(): void {
    this.iniciativaService
      .query()
      .pipe(map((res: HttpResponse<IIniciativa[]>) => res.body ?? []))
      .pipe(
        map((iniciativas: IIniciativa[]) =>
          this.iniciativaService.addIniciativaToCollectionIfMissing(iniciativas, this.editForm.get('iniciativa')!.value)
        )
      )
      .subscribe((iniciativas: IIniciativa[]) => (this.iniciativasSharedCollection = iniciativas));
  }

  protected createFromForm(): IEstadosHistorias {
    return {
      ...new EstadosHistorias(),
      id: this.editForm.get(['id'])!.value,
      totalEstimacion: this.editForm.get(['totalEstimacion'])!.value,
      totalBacklog: this.editForm.get(['totalBacklog'])!.value,
      totalEnRefinamiento: this.editForm.get(['totalEnRefinamiento'])!.value,
      totalListoParaDesarrollo: this.editForm.get(['totalListoParaDesarrollo'])!.value,
      totalEnDesarrollo: this.editForm.get(['totalEnDesarrollo'])!.value,
      totalListoCertificacion: this.editForm.get(['totalListoCertificacion'])!.value,
      totalEnCertificacion: this.editForm.get(['totalEnCertificacion'])!.value,
      totalPruebasUAT: this.editForm.get(['totalPruebasUAT'])!.value,
      totalListoProduccion: this.editForm.get(['totalListoProduccion'])!.value,
      totalEnProduccion: this.editForm.get(['totalEnProduccion'])!.value,
      totalAprobadosPO: this.editForm.get(['totalAprobadosPO'])!.value,
      iniciativa: this.editForm.get(['iniciativa'])!.value,
    };
  }
}
