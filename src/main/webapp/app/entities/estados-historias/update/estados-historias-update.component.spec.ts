jest.mock('@angular/router');

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { of, Subject } from 'rxjs';

import { EstadosHistoriasService } from '../service/estados-historias.service';
import { IEstadosHistorias, EstadosHistorias } from '../estados-historias.model';
import { IIniciativa } from 'app/entities/iniciativa/iniciativa.model';
import { IniciativaService } from 'app/entities/iniciativa/service/iniciativa.service';

import { EstadosHistoriasUpdateComponent } from './estados-historias-update.component';

describe('EstadosHistorias Management Update Component', () => {
  let comp: EstadosHistoriasUpdateComponent;
  let fixture: ComponentFixture<EstadosHistoriasUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let estadosHistoriasService: EstadosHistoriasService;
  let iniciativaService: IniciativaService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [EstadosHistoriasUpdateComponent],
      providers: [FormBuilder, ActivatedRoute],
    })
      .overrideTemplate(EstadosHistoriasUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(EstadosHistoriasUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    estadosHistoriasService = TestBed.inject(EstadosHistoriasService);
    iniciativaService = TestBed.inject(IniciativaService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Iniciativa query and add missing value', () => {
      const estadosHistorias: IEstadosHistorias = { id: 456 };
      const iniciativa: IIniciativa = { id: 89967 };
      estadosHistorias.iniciativa = iniciativa;

      const iniciativaCollection: IIniciativa[] = [{ id: 96022 }];
      jest.spyOn(iniciativaService, 'query').mockReturnValue(of(new HttpResponse({ body: iniciativaCollection })));
      const additionalIniciativas = [iniciativa];
      const expectedCollection: IIniciativa[] = [...additionalIniciativas, ...iniciativaCollection];
      jest.spyOn(iniciativaService, 'addIniciativaToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ estadosHistorias });
      comp.ngOnInit();

      expect(iniciativaService.query).toHaveBeenCalled();
      expect(iniciativaService.addIniciativaToCollectionIfMissing).toHaveBeenCalledWith(iniciativaCollection, ...additionalIniciativas);
      expect(comp.iniciativasSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const estadosHistorias: IEstadosHistorias = { id: 456 };
      const iniciativa: IIniciativa = { id: 27446 };
      estadosHistorias.iniciativa = iniciativa;

      activatedRoute.data = of({ estadosHistorias });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(estadosHistorias));
      expect(comp.iniciativasSharedCollection).toContain(iniciativa);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<EstadosHistorias>>();
      const estadosHistorias = { id: 123 };
      jest.spyOn(estadosHistoriasService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ estadosHistorias });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: estadosHistorias }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(estadosHistoriasService.update).toHaveBeenCalledWith(estadosHistorias);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<EstadosHistorias>>();
      const estadosHistorias = new EstadosHistorias();
      jest.spyOn(estadosHistoriasService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ estadosHistorias });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: estadosHistorias }));
      saveSubject.complete();

      // THEN
      expect(estadosHistoriasService.create).toHaveBeenCalledWith(estadosHistorias);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<EstadosHistorias>>();
      const estadosHistorias = { id: 123 };
      jest.spyOn(estadosHistoriasService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ estadosHistorias });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(estadosHistoriasService.update).toHaveBeenCalledWith(estadosHistorias);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackIniciativaById', () => {
      it('Should return tracked Iniciativa primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackIniciativaById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
