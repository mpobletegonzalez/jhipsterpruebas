import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IEstadosHistorias, getEstadosHistoriasIdentifier } from '../estados-historias.model';

export type EntityResponseType = HttpResponse<IEstadosHistorias>;
export type EntityArrayResponseType = HttpResponse<IEstadosHistorias[]>;

@Injectable({ providedIn: 'root' })
export class EstadosHistoriasService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/estados-historias');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(estadosHistorias: IEstadosHistorias): Observable<EntityResponseType> {
    return this.http.post<IEstadosHistorias>(this.resourceUrl, estadosHistorias, { observe: 'response' });
  }

  update(estadosHistorias: IEstadosHistorias): Observable<EntityResponseType> {
    return this.http.put<IEstadosHistorias>(
      `${this.resourceUrl}/${getEstadosHistoriasIdentifier(estadosHistorias) as number}`,
      estadosHistorias,
      { observe: 'response' }
    );
  }

  partialUpdate(estadosHistorias: IEstadosHistorias): Observable<EntityResponseType> {
    return this.http.patch<IEstadosHistorias>(
      `${this.resourceUrl}/${getEstadosHistoriasIdentifier(estadosHistorias) as number}`,
      estadosHistorias,
      { observe: 'response' }
    );
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IEstadosHistorias>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IEstadosHistorias[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addEstadosHistoriasToCollectionIfMissing(
    estadosHistoriasCollection: IEstadosHistorias[],
    ...estadosHistoriasToCheck: (IEstadosHistorias | null | undefined)[]
  ): IEstadosHistorias[] {
    const estadosHistorias: IEstadosHistorias[] = estadosHistoriasToCheck.filter(isPresent);
    if (estadosHistorias.length > 0) {
      const estadosHistoriasCollectionIdentifiers = estadosHistoriasCollection.map(
        estadosHistoriasItem => getEstadosHistoriasIdentifier(estadosHistoriasItem)!
      );
      const estadosHistoriasToAdd = estadosHistorias.filter(estadosHistoriasItem => {
        const estadosHistoriasIdentifier = getEstadosHistoriasIdentifier(estadosHistoriasItem);
        if (estadosHistoriasIdentifier == null || estadosHistoriasCollectionIdentifiers.includes(estadosHistoriasIdentifier)) {
          return false;
        }
        estadosHistoriasCollectionIdentifiers.push(estadosHistoriasIdentifier);
        return true;
      });
      return [...estadosHistoriasToAdd, ...estadosHistoriasCollection];
    }
    return estadosHistoriasCollection;
  }
}
