import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IEstadosHistorias, EstadosHistorias } from '../estados-historias.model';

import { EstadosHistoriasService } from './estados-historias.service';

describe('EstadosHistorias Service', () => {
  let service: EstadosHistoriasService;
  let httpMock: HttpTestingController;
  let elemDefault: IEstadosHistorias;
  let expectedResult: IEstadosHistorias | IEstadosHistorias[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(EstadosHistoriasService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      totalEstimacion: 0,
      totalBacklog: 0,
      totalEnRefinamiento: 0,
      totalListoParaDesarrollo: 0,
      totalEnDesarrollo: 0,
      totalListoCertificacion: 0,
      totalEnCertificacion: 0,
      totalPruebasUAT: 0,
      totalListoProduccion: 0,
      totalEnProduccion: 0,
      totalAprobadosPO: 0,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a EstadosHistorias', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new EstadosHistorias()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a EstadosHistorias', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          totalEstimacion: 1,
          totalBacklog: 1,
          totalEnRefinamiento: 1,
          totalListoParaDesarrollo: 1,
          totalEnDesarrollo: 1,
          totalListoCertificacion: 1,
          totalEnCertificacion: 1,
          totalPruebasUAT: 1,
          totalListoProduccion: 1,
          totalEnProduccion: 1,
          totalAprobadosPO: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a EstadosHistorias', () => {
      const patchObject = Object.assign(
        {
          totalEstimacion: 1,
          totalEnRefinamiento: 1,
          totalListoParaDesarrollo: 1,
          totalEnDesarrollo: 1,
          totalListoCertificacion: 1,
          totalEnCertificacion: 1,
          totalPruebasUAT: 1,
          totalListoProduccion: 1,
          totalEnProduccion: 1,
        },
        new EstadosHistorias()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of EstadosHistorias', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          totalEstimacion: 1,
          totalBacklog: 1,
          totalEnRefinamiento: 1,
          totalListoParaDesarrollo: 1,
          totalEnDesarrollo: 1,
          totalListoCertificacion: 1,
          totalEnCertificacion: 1,
          totalPruebasUAT: 1,
          totalListoProduccion: 1,
          totalEnProduccion: 1,
          totalAprobadosPO: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a EstadosHistorias', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addEstadosHistoriasToCollectionIfMissing', () => {
      it('should add a EstadosHistorias to an empty array', () => {
        const estadosHistorias: IEstadosHistorias = { id: 123 };
        expectedResult = service.addEstadosHistoriasToCollectionIfMissing([], estadosHistorias);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(estadosHistorias);
      });

      it('should not add a EstadosHistorias to an array that contains it', () => {
        const estadosHistorias: IEstadosHistorias = { id: 123 };
        const estadosHistoriasCollection: IEstadosHistorias[] = [
          {
            ...estadosHistorias,
          },
          { id: 456 },
        ];
        expectedResult = service.addEstadosHistoriasToCollectionIfMissing(estadosHistoriasCollection, estadosHistorias);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a EstadosHistorias to an array that doesn't contain it", () => {
        const estadosHistorias: IEstadosHistorias = { id: 123 };
        const estadosHistoriasCollection: IEstadosHistorias[] = [{ id: 456 }];
        expectedResult = service.addEstadosHistoriasToCollectionIfMissing(estadosHistoriasCollection, estadosHistorias);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(estadosHistorias);
      });

      it('should add only unique EstadosHistorias to an array', () => {
        const estadosHistoriasArray: IEstadosHistorias[] = [{ id: 123 }, { id: 456 }, { id: 17514 }];
        const estadosHistoriasCollection: IEstadosHistorias[] = [{ id: 123 }];
        expectedResult = service.addEstadosHistoriasToCollectionIfMissing(estadosHistoriasCollection, ...estadosHistoriasArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const estadosHistorias: IEstadosHistorias = { id: 123 };
        const estadosHistorias2: IEstadosHistorias = { id: 456 };
        expectedResult = service.addEstadosHistoriasToCollectionIfMissing([], estadosHistorias, estadosHistorias2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(estadosHistorias);
        expect(expectedResult).toContain(estadosHistorias2);
      });

      it('should accept null and undefined values', () => {
        const estadosHistorias: IEstadosHistorias = { id: 123 };
        expectedResult = service.addEstadosHistoriasToCollectionIfMissing([], null, estadosHistorias, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(estadosHistorias);
      });

      it('should return initial array if no EstadosHistorias is added', () => {
        const estadosHistoriasCollection: IEstadosHistorias[] = [{ id: 123 }];
        expectedResult = service.addEstadosHistoriasToCollectionIfMissing(estadosHistoriasCollection, undefined, null);
        expect(expectedResult).toEqual(estadosHistoriasCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
