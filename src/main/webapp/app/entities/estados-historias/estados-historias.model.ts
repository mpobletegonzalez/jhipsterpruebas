import { IIniciativa } from 'app/entities/iniciativa/iniciativa.model';

export interface IEstadosHistorias {
  id?: number;
  totalEstimacion?: number | null;
  totalBacklog?: number | null;
  totalEnRefinamiento?: number | null;
  totalListoParaDesarrollo?: number | null;
  totalEnDesarrollo?: number | null;
  totalListoCertificacion?: number | null;
  totalEnCertificacion?: number | null;
  totalPruebasUAT?: number | null;
  totalListoProduccion?: number | null;
  totalEnProduccion?: number | null;
  totalAprobadosPO?: number | null;
  iniciativa?: IIniciativa | null;
}

export class EstadosHistorias implements IEstadosHistorias {
  constructor(
    public id?: number,
    public totalEstimacion?: number | null,
    public totalBacklog?: number | null,
    public totalEnRefinamiento?: number | null,
    public totalListoParaDesarrollo?: number | null,
    public totalEnDesarrollo?: number | null,
    public totalListoCertificacion?: number | null,
    public totalEnCertificacion?: number | null,
    public totalPruebasUAT?: number | null,
    public totalListoProduccion?: number | null,
    public totalEnProduccion?: number | null,
    public totalAprobadosPO?: number | null,
    public iniciativa?: IIniciativa | null
  ) {}
}

export function getEstadosHistoriasIdentifier(estadosHistorias: IEstadosHistorias): number | undefined {
  return estadosHistorias.id;
}
