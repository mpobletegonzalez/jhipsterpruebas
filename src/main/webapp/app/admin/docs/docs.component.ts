import { Component } from '@angular/core';

@Component({
  selector: 'upgrade-docs',
  templateUrl: './docs.component.html',
  styleUrls: ['./docs.component.scss'],
})
export class DocsComponent {}
