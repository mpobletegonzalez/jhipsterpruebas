package cl.tinet.upgrade.web.rest;

import cl.tinet.upgrade.repository.AsignacionRepository;
import cl.tinet.upgrade.service.AsignacionService;
import cl.tinet.upgrade.service.dto.AsignacionDTO;
import cl.tinet.upgrade.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link cl.tinet.upgrade.domain.Asignacion}.
 */
@RestController
@RequestMapping("/api")
public class AsignacionResource {

    private final Logger log = LoggerFactory.getLogger(AsignacionResource.class);

    private static final String ENTITY_NAME = "asignacion";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final AsignacionService asignacionService;

    private final AsignacionRepository asignacionRepository;

    public AsignacionResource(AsignacionService asignacionService, AsignacionRepository asignacionRepository) {
        this.asignacionService = asignacionService;
        this.asignacionRepository = asignacionRepository;
    }

    /**
     * {@code POST  /asignacions} : Create a new asignacion.
     *
     * @param asignacionDTO the asignacionDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new asignacionDTO, or with status {@code 400 (Bad Request)} if the asignacion has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/asignacions")
    public ResponseEntity<AsignacionDTO> createAsignacion(@RequestBody AsignacionDTO asignacionDTO) throws URISyntaxException {
        log.debug("REST request to save Asignacion : {}", asignacionDTO);
        if (asignacionDTO.getId() != null) {
            throw new BadRequestAlertException("A new asignacion cannot already have an ID", ENTITY_NAME, "idexists");
        }
        AsignacionDTO result = asignacionService.save(asignacionDTO);
        return ResponseEntity
            .created(new URI("/api/asignacions/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /asignacions/:id} : Updates an existing asignacion.
     *
     * @param id the id of the asignacionDTO to save.
     * @param asignacionDTO the asignacionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated asignacionDTO,
     * or with status {@code 400 (Bad Request)} if the asignacionDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the asignacionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/asignacions/{id}")
    public ResponseEntity<AsignacionDTO> updateAsignacion(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody AsignacionDTO asignacionDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Asignacion : {}, {}", id, asignacionDTO);
        if (asignacionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, asignacionDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!asignacionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        AsignacionDTO result = asignacionService.save(asignacionDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, asignacionDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /asignacions/:id} : Partial updates given fields of an existing asignacion, field will ignore if it is null
     *
     * @param id the id of the asignacionDTO to save.
     * @param asignacionDTO the asignacionDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated asignacionDTO,
     * or with status {@code 400 (Bad Request)} if the asignacionDTO is not valid,
     * or with status {@code 404 (Not Found)} if the asignacionDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the asignacionDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/asignacions/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<AsignacionDTO> partialUpdateAsignacion(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody AsignacionDTO asignacionDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Asignacion partially : {}, {}", id, asignacionDTO);
        if (asignacionDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, asignacionDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!asignacionRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<AsignacionDTO> result = asignacionService.partialUpdate(asignacionDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, asignacionDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /asignacions} : get all the asignacions.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of asignacions in body.
     */
    @GetMapping("/asignacions")
    public ResponseEntity<List<AsignacionDTO>> getAllAsignacions(Pageable pageable) {
        log.debug("REST request to get a page of Asignacions");
        Page<AsignacionDTO> page = asignacionService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /asignacions/:id} : get the "id" asignacion.
     *
     * @param id the id of the asignacionDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the asignacionDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/asignacions/{id}")
    public ResponseEntity<AsignacionDTO> getAsignacion(@PathVariable Long id) {
        log.debug("REST request to get Asignacion : {}", id);
        Optional<AsignacionDTO> asignacionDTO = asignacionService.findOne(id);
        return ResponseUtil.wrapOrNotFound(asignacionDTO);
    }

    /**
     * {@code DELETE  /asignacions/:id} : delete the "id" asignacion.
     *
     * @param id the id of the asignacionDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/asignacions/{id}")
    public ResponseEntity<Void> deleteAsignacion(@PathVariable Long id) {
        log.debug("REST request to delete Asignacion : {}", id);
        asignacionService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
