package cl.tinet.upgrade.web.rest;

import cl.tinet.upgrade.repository.EstadosHistoriasRepository;
import cl.tinet.upgrade.service.EstadosHistoriasService;
import cl.tinet.upgrade.service.dto.EstadosHistoriasDTO;
import cl.tinet.upgrade.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link cl.tinet.upgrade.domain.EstadosHistorias}.
 */
@RestController
@RequestMapping("/api")
public class EstadosHistoriasResource {

    private final Logger log = LoggerFactory.getLogger(EstadosHistoriasResource.class);

    private static final String ENTITY_NAME = "estadosHistorias";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final EstadosHistoriasService estadosHistoriasService;

    private final EstadosHistoriasRepository estadosHistoriasRepository;

    public EstadosHistoriasResource(
        EstadosHistoriasService estadosHistoriasService,
        EstadosHistoriasRepository estadosHistoriasRepository
    ) {
        this.estadosHistoriasService = estadosHistoriasService;
        this.estadosHistoriasRepository = estadosHistoriasRepository;
    }

    /**
     * {@code POST  /estados-historias} : Create a new estadosHistorias.
     *
     * @param estadosHistoriasDTO the estadosHistoriasDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new estadosHistoriasDTO, or with status {@code 400 (Bad Request)} if the estadosHistorias has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/estados-historias")
    public ResponseEntity<EstadosHistoriasDTO> createEstadosHistorias(@RequestBody EstadosHistoriasDTO estadosHistoriasDTO)
        throws URISyntaxException {
        log.debug("REST request to save EstadosHistorias : {}", estadosHistoriasDTO);
        if (estadosHistoriasDTO.getId() != null) {
            throw new BadRequestAlertException("A new estadosHistorias cannot already have an ID", ENTITY_NAME, "idexists");
        }
        EstadosHistoriasDTO result = estadosHistoriasService.save(estadosHistoriasDTO);
        return ResponseEntity
            .created(new URI("/api/estados-historias/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /estados-historias/:id} : Updates an existing estadosHistorias.
     *
     * @param id the id of the estadosHistoriasDTO to save.
     * @param estadosHistoriasDTO the estadosHistoriasDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated estadosHistoriasDTO,
     * or with status {@code 400 (Bad Request)} if the estadosHistoriasDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the estadosHistoriasDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/estados-historias/{id}")
    public ResponseEntity<EstadosHistoriasDTO> updateEstadosHistorias(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody EstadosHistoriasDTO estadosHistoriasDTO
    ) throws URISyntaxException {
        log.debug("REST request to update EstadosHistorias : {}, {}", id, estadosHistoriasDTO);
        if (estadosHistoriasDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, estadosHistoriasDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!estadosHistoriasRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        EstadosHistoriasDTO result = estadosHistoriasService.save(estadosHistoriasDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, estadosHistoriasDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /estados-historias/:id} : Partial updates given fields of an existing estadosHistorias, field will ignore if it is null
     *
     * @param id the id of the estadosHistoriasDTO to save.
     * @param estadosHistoriasDTO the estadosHistoriasDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated estadosHistoriasDTO,
     * or with status {@code 400 (Bad Request)} if the estadosHistoriasDTO is not valid,
     * or with status {@code 404 (Not Found)} if the estadosHistoriasDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the estadosHistoriasDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/estados-historias/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<EstadosHistoriasDTO> partialUpdateEstadosHistorias(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody EstadosHistoriasDTO estadosHistoriasDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update EstadosHistorias partially : {}, {}", id, estadosHistoriasDTO);
        if (estadosHistoriasDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, estadosHistoriasDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!estadosHistoriasRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<EstadosHistoriasDTO> result = estadosHistoriasService.partialUpdate(estadosHistoriasDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, estadosHistoriasDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /estados-historias} : get all the estadosHistorias.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of estadosHistorias in body.
     */
    @GetMapping("/estados-historias")
    public ResponseEntity<List<EstadosHistoriasDTO>> getAllEstadosHistorias(Pageable pageable) {
        log.debug("REST request to get a page of EstadosHistorias");
        Page<EstadosHistoriasDTO> page = estadosHistoriasService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /estados-historias/:id} : get the "id" estadosHistorias.
     *
     * @param id the id of the estadosHistoriasDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the estadosHistoriasDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/estados-historias/{id}")
    public ResponseEntity<EstadosHistoriasDTO> getEstadosHistorias(@PathVariable Long id) {
        log.debug("REST request to get EstadosHistorias : {}", id);
        Optional<EstadosHistoriasDTO> estadosHistoriasDTO = estadosHistoriasService.findOne(id);
        return ResponseUtil.wrapOrNotFound(estadosHistoriasDTO);
    }

    /**
     * {@code DELETE  /estados-historias/:id} : delete the "id" estadosHistorias.
     *
     * @param id the id of the estadosHistoriasDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/estados-historias/{id}")
    public ResponseEntity<Void> deleteEstadosHistorias(@PathVariable Long id) {
        log.debug("REST request to delete EstadosHistorias : {}", id);
        estadosHistoriasService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
