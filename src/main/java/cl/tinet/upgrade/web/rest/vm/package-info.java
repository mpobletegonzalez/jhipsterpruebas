/**
 * View Models used by Spring MVC REST controllers.
 */
package cl.tinet.upgrade.web.rest.vm;
