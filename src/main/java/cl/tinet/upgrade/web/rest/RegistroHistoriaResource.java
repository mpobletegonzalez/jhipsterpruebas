package cl.tinet.upgrade.web.rest;

import cl.tinet.upgrade.repository.RegistroHistoriaRepository;
import cl.tinet.upgrade.service.RegistroHistoriaService;
import cl.tinet.upgrade.service.dto.RegistroHistoriaDTO;
import cl.tinet.upgrade.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link cl.tinet.upgrade.domain.RegistroHistoria}.
 */
@RestController
@RequestMapping("/api")
public class RegistroHistoriaResource {

    private final Logger log = LoggerFactory.getLogger(RegistroHistoriaResource.class);

    private static final String ENTITY_NAME = "registroHistoria";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final RegistroHistoriaService registroHistoriaService;

    private final RegistroHistoriaRepository registroHistoriaRepository;

    public RegistroHistoriaResource(
        RegistroHistoriaService registroHistoriaService,
        RegistroHistoriaRepository registroHistoriaRepository
    ) {
        this.registroHistoriaService = registroHistoriaService;
        this.registroHistoriaRepository = registroHistoriaRepository;
    }

    /**
     * {@code POST  /registro-historias} : Create a new registroHistoria.
     *
     * @param registroHistoriaDTO the registroHistoriaDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new registroHistoriaDTO, or with status {@code 400 (Bad Request)} if the registroHistoria has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/registro-historias")
    public ResponseEntity<RegistroHistoriaDTO> createRegistroHistoria(@RequestBody RegistroHistoriaDTO registroHistoriaDTO)
        throws URISyntaxException {
        log.debug("REST request to save RegistroHistoria : {}", registroHistoriaDTO);
        if (registroHistoriaDTO.getId() != null) {
            throw new BadRequestAlertException("A new registroHistoria cannot already have an ID", ENTITY_NAME, "idexists");
        }
        RegistroHistoriaDTO result = registroHistoriaService.save(registroHistoriaDTO);
        return ResponseEntity
            .created(new URI("/api/registro-historias/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /registro-historias/:id} : Updates an existing registroHistoria.
     *
     * @param id the id of the registroHistoriaDTO to save.
     * @param registroHistoriaDTO the registroHistoriaDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated registroHistoriaDTO,
     * or with status {@code 400 (Bad Request)} if the registroHistoriaDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the registroHistoriaDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/registro-historias/{id}")
    public ResponseEntity<RegistroHistoriaDTO> updateRegistroHistoria(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody RegistroHistoriaDTO registroHistoriaDTO
    ) throws URISyntaxException {
        log.debug("REST request to update RegistroHistoria : {}, {}", id, registroHistoriaDTO);
        if (registroHistoriaDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, registroHistoriaDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!registroHistoriaRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        RegistroHistoriaDTO result = registroHistoriaService.save(registroHistoriaDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, registroHistoriaDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /registro-historias/:id} : Partial updates given fields of an existing registroHistoria, field will ignore if it is null
     *
     * @param id the id of the registroHistoriaDTO to save.
     * @param registroHistoriaDTO the registroHistoriaDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated registroHistoriaDTO,
     * or with status {@code 400 (Bad Request)} if the registroHistoriaDTO is not valid,
     * or with status {@code 404 (Not Found)} if the registroHistoriaDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the registroHistoriaDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/registro-historias/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<RegistroHistoriaDTO> partialUpdateRegistroHistoria(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody RegistroHistoriaDTO registroHistoriaDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update RegistroHistoria partially : {}, {}", id, registroHistoriaDTO);
        if (registroHistoriaDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, registroHistoriaDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!registroHistoriaRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<RegistroHistoriaDTO> result = registroHistoriaService.partialUpdate(registroHistoriaDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, registroHistoriaDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /registro-historias} : get all the registroHistorias.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of registroHistorias in body.
     */
    @GetMapping("/registro-historias")
    public ResponseEntity<List<RegistroHistoriaDTO>> getAllRegistroHistorias(Pageable pageable) {
        log.debug("REST request to get a page of RegistroHistorias");
        Page<RegistroHistoriaDTO> page = registroHistoriaService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /registro-historias/:id} : get the "id" registroHistoria.
     *
     * @param id the id of the registroHistoriaDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the registroHistoriaDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/registro-historias/{id}")
    public ResponseEntity<RegistroHistoriaDTO> getRegistroHistoria(@PathVariable Long id) {
        log.debug("REST request to get RegistroHistoria : {}", id);
        Optional<RegistroHistoriaDTO> registroHistoriaDTO = registroHistoriaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(registroHistoriaDTO);
    }

    /**
     * {@code DELETE  /registro-historias/:id} : delete the "id" registroHistoria.
     *
     * @param id the id of the registroHistoriaDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/registro-historias/{id}")
    public ResponseEntity<Void> deleteRegistroHistoria(@PathVariable Long id) {
        log.debug("REST request to delete RegistroHistoria : {}", id);
        registroHistoriaService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
