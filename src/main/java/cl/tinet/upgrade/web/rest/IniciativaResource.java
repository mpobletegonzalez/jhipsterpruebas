package cl.tinet.upgrade.web.rest;

import cl.tinet.upgrade.repository.IniciativaRepository;
import cl.tinet.upgrade.service.IniciativaService;
import cl.tinet.upgrade.service.dto.IniciativaDTO;
import cl.tinet.upgrade.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link cl.tinet.upgrade.domain.Iniciativa}.
 */
@RestController
@RequestMapping("/api")
public class IniciativaResource {

    private final Logger log = LoggerFactory.getLogger(IniciativaResource.class);

    private static final String ENTITY_NAME = "iniciativa";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final IniciativaService iniciativaService;

    private final IniciativaRepository iniciativaRepository;

    public IniciativaResource(IniciativaService iniciativaService, IniciativaRepository iniciativaRepository) {
        this.iniciativaService = iniciativaService;
        this.iniciativaRepository = iniciativaRepository;
    }

    /**
     * {@code POST  /iniciativas} : Create a new iniciativa.
     *
     * @param iniciativaDTO the iniciativaDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new iniciativaDTO, or with status {@code 400 (Bad Request)} if the iniciativa has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/iniciativas")
    public ResponseEntity<IniciativaDTO> createIniciativa(@Valid @RequestBody IniciativaDTO iniciativaDTO) throws URISyntaxException {
        log.debug("REST request to save Iniciativa : {}", iniciativaDTO);
        if (iniciativaDTO.getId() != null) {
            throw new BadRequestAlertException("A new iniciativa cannot already have an ID", ENTITY_NAME, "idexists");
        }
        IniciativaDTO result = iniciativaService.save(iniciativaDTO);
        return ResponseEntity
            .created(new URI("/api/iniciativas/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /iniciativas/:id} : Updates an existing iniciativa.
     *
     * @param id the id of the iniciativaDTO to save.
     * @param iniciativaDTO the iniciativaDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated iniciativaDTO,
     * or with status {@code 400 (Bad Request)} if the iniciativaDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the iniciativaDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/iniciativas/{id}")
    public ResponseEntity<IniciativaDTO> updateIniciativa(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody IniciativaDTO iniciativaDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Iniciativa : {}, {}", id, iniciativaDTO);
        if (iniciativaDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, iniciativaDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!iniciativaRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        IniciativaDTO result = iniciativaService.save(iniciativaDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, iniciativaDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /iniciativas/:id} : Partial updates given fields of an existing iniciativa, field will ignore if it is null
     *
     * @param id the id of the iniciativaDTO to save.
     * @param iniciativaDTO the iniciativaDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated iniciativaDTO,
     * or with status {@code 400 (Bad Request)} if the iniciativaDTO is not valid,
     * or with status {@code 404 (Not Found)} if the iniciativaDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the iniciativaDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/iniciativas/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<IniciativaDTO> partialUpdateIniciativa(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody IniciativaDTO iniciativaDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Iniciativa partially : {}, {}", id, iniciativaDTO);
        if (iniciativaDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, iniciativaDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!iniciativaRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<IniciativaDTO> result = iniciativaService.partialUpdate(iniciativaDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, iniciativaDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /iniciativas} : get all the iniciativas.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of iniciativas in body.
     */
    @GetMapping("/iniciativas")
    public ResponseEntity<List<IniciativaDTO>> getAllIniciativas(Pageable pageable) {
        log.debug("REST request to get a page of Iniciativas");
        Page<IniciativaDTO> page = iniciativaService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /iniciativas/:id} : get the "id" iniciativa.
     *
     * @param id the id of the iniciativaDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the iniciativaDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/iniciativas/{id}")
    public ResponseEntity<IniciativaDTO> getIniciativa(@PathVariable Long id) {
        log.debug("REST request to get Iniciativa : {}", id);
        Optional<IniciativaDTO> iniciativaDTO = iniciativaService.findOne(id);
        return ResponseUtil.wrapOrNotFound(iniciativaDTO);
    }

    /**
     * {@code DELETE  /iniciativas/:id} : delete the "id" iniciativa.
     *
     * @param id the id of the iniciativaDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/iniciativas/{id}")
    public ResponseEntity<Void> deleteIniciativa(@PathVariable Long id) {
        log.debug("REST request to delete Iniciativa : {}", id);
        iniciativaService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
