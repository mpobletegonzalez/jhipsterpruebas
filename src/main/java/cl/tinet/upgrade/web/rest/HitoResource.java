package cl.tinet.upgrade.web.rest;

import cl.tinet.upgrade.repository.HitoRepository;
import cl.tinet.upgrade.service.HitoService;
import cl.tinet.upgrade.service.dto.HitoDTO;
import cl.tinet.upgrade.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link cl.tinet.upgrade.domain.Hito}.
 */
@RestController
@RequestMapping("/api")
public class HitoResource {

    private final Logger log = LoggerFactory.getLogger(HitoResource.class);

    private static final String ENTITY_NAME = "hito";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final HitoService hitoService;

    private final HitoRepository hitoRepository;

    public HitoResource(HitoService hitoService, HitoRepository hitoRepository) {
        this.hitoService = hitoService;
        this.hitoRepository = hitoRepository;
    }

    /**
     * {@code POST  /hitos} : Create a new hito.
     *
     * @param hitoDTO the hitoDTO to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new hitoDTO, or with status {@code 400 (Bad Request)} if the hito has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/hitos")
    public ResponseEntity<HitoDTO> createHito(@Valid @RequestBody HitoDTO hitoDTO) throws URISyntaxException {
        log.debug("REST request to save Hito : {}", hitoDTO);
        if (hitoDTO.getId() != null) {
            throw new BadRequestAlertException("A new hito cannot already have an ID", ENTITY_NAME, "idexists");
        }
        HitoDTO result = hitoService.save(hitoDTO);
        return ResponseEntity
            .created(new URI("/api/hitos/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /hitos/:id} : Updates an existing hito.
     *
     * @param id the id of the hitoDTO to save.
     * @param hitoDTO the hitoDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated hitoDTO,
     * or with status {@code 400 (Bad Request)} if the hitoDTO is not valid,
     * or with status {@code 500 (Internal Server Error)} if the hitoDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/hitos/{id}")
    public ResponseEntity<HitoDTO> updateHito(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody HitoDTO hitoDTO
    ) throws URISyntaxException {
        log.debug("REST request to update Hito : {}, {}", id, hitoDTO);
        if (hitoDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, hitoDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!hitoRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        HitoDTO result = hitoService.save(hitoDTO);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, hitoDTO.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /hitos/:id} : Partial updates given fields of an existing hito, field will ignore if it is null
     *
     * @param id the id of the hitoDTO to save.
     * @param hitoDTO the hitoDTO to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated hitoDTO,
     * or with status {@code 400 (Bad Request)} if the hitoDTO is not valid,
     * or with status {@code 404 (Not Found)} if the hitoDTO is not found,
     * or with status {@code 500 (Internal Server Error)} if the hitoDTO couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/hitos/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<HitoDTO> partialUpdateHito(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody HitoDTO hitoDTO
    ) throws URISyntaxException {
        log.debug("REST request to partial update Hito partially : {}, {}", id, hitoDTO);
        if (hitoDTO.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, hitoDTO.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!hitoRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<HitoDTO> result = hitoService.partialUpdate(hitoDTO);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, hitoDTO.getId().toString())
        );
    }

    /**
     * {@code GET  /hitos} : get all the hitos.
     *
     * @param pageable the pagination information.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of hitos in body.
     */
    @GetMapping("/hitos")
    public ResponseEntity<List<HitoDTO>> getAllHitos(Pageable pageable) {
        log.debug("REST request to get a page of Hitos");
        Page<HitoDTO> page = hitoService.findAll(pageable);
        HttpHeaders headers = PaginationUtil.generatePaginationHttpHeaders(ServletUriComponentsBuilder.fromCurrentRequest(), page);
        return ResponseEntity.ok().headers(headers).body(page.getContent());
    }

    /**
     * {@code GET  /hitos/:id} : get the "id" hito.
     *
     * @param id the id of the hitoDTO to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the hitoDTO, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/hitos/{id}")
    public ResponseEntity<HitoDTO> getHito(@PathVariable Long id) {
        log.debug("REST request to get Hito : {}", id);
        Optional<HitoDTO> hitoDTO = hitoService.findOne(id);
        return ResponseUtil.wrapOrNotFound(hitoDTO);
    }

    /**
     * {@code DELETE  /hitos/:id} : delete the "id" hito.
     *
     * @param id the id of the hitoDTO to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/hitos/{id}")
    public ResponseEntity<Void> deleteHito(@PathVariable Long id) {
        log.debug("REST request to delete Hito : {}", id);
        hitoService.delete(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
