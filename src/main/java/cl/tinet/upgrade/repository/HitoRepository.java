package cl.tinet.upgrade.repository;

import cl.tinet.upgrade.domain.Hito;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Hito entity.
 */
@SuppressWarnings("unused")
@Repository
public interface HitoRepository extends JpaRepository<Hito, Long> {}
