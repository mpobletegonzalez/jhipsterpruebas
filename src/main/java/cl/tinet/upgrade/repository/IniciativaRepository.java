package cl.tinet.upgrade.repository;

import cl.tinet.upgrade.domain.Iniciativa;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Iniciativa entity.
 */
@SuppressWarnings("unused")
@Repository
public interface IniciativaRepository extends JpaRepository<Iniciativa, Long> {}
