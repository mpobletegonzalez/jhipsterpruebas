package cl.tinet.upgrade.repository;

import cl.tinet.upgrade.domain.Funcionalidad;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the Funcionalidad entity.
 */
@SuppressWarnings("unused")
@Repository
public interface FuncionalidadRepository extends JpaRepository<Funcionalidad, Long> {}
