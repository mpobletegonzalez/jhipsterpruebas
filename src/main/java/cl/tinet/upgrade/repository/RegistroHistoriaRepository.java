package cl.tinet.upgrade.repository;

import cl.tinet.upgrade.domain.RegistroHistoria;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the RegistroHistoria entity.
 */
@SuppressWarnings("unused")
@Repository
public interface RegistroHistoriaRepository extends JpaRepository<RegistroHistoria, Long> {}
