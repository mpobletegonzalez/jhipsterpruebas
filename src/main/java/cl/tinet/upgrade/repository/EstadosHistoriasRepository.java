package cl.tinet.upgrade.repository;

import cl.tinet.upgrade.domain.EstadosHistorias;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the EstadosHistorias entity.
 */
@SuppressWarnings("unused")
@Repository
public interface EstadosHistoriasRepository extends JpaRepository<EstadosHistorias, Long> {}
