package cl.tinet.upgrade.service.dto;

import cl.tinet.upgrade.domain.enumeration.TipoEstadoFuncionalidad;
import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link cl.tinet.upgrade.domain.Funcionalidad} entity.
 */
public class FuncionalidadDTO implements Serializable {

    private Long id;

    private String nombre;

    private TipoEstadoFuncionalidad estado;

    private Integer prioridad;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public TipoEstadoFuncionalidad getEstado() {
        return estado;
    }

    public void setEstado(TipoEstadoFuncionalidad estado) {
        this.estado = estado;
    }

    public Integer getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(Integer prioridad) {
        this.prioridad = prioridad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof FuncionalidadDTO)) {
            return false;
        }

        FuncionalidadDTO funcionalidadDTO = (FuncionalidadDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, funcionalidadDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "FuncionalidadDTO{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", estado='" + getEstado() + "'" +
            ", prioridad=" + getPrioridad() +
            "}";
    }
}
