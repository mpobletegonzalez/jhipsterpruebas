package cl.tinet.upgrade.service.dto;

import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link cl.tinet.upgrade.domain.Iniciativa} entity.
 */
public class IniciativaDTO implements Serializable {

    private Long id;

    @NotNull
    private String nombre;

    @NotNull
    private ZonedDateTime fechaInicio;

    @NotNull
    private ZonedDateTime fechaFin;

    @NotNull
    private ZonedDateTime fechaCreacion;

    @NotNull
    private Boolean isActivo;

    @NotNull
    private String promesa;

    @NotNull
    private String tituloProductoDigital;

    @NotNull
    private String descripcionProductoDigital;

    private ServicioDTO servicio;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ZonedDateTime getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(ZonedDateTime fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public ZonedDateTime getFechaFin() {
        return fechaFin;
    }

    public void setFechaFin(ZonedDateTime fechaFin) {
        this.fechaFin = fechaFin;
    }

    public ZonedDateTime getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(ZonedDateTime fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Boolean getIsActivo() {
        return isActivo;
    }

    public void setIsActivo(Boolean isActivo) {
        this.isActivo = isActivo;
    }

    public String getPromesa() {
        return promesa;
    }

    public void setPromesa(String promesa) {
        this.promesa = promesa;
    }

    public String getTituloProductoDigital() {
        return tituloProductoDigital;
    }

    public void setTituloProductoDigital(String tituloProductoDigital) {
        this.tituloProductoDigital = tituloProductoDigital;
    }

    public String getDescripcionProductoDigital() {
        return descripcionProductoDigital;
    }

    public void setDescripcionProductoDigital(String descripcionProductoDigital) {
        this.descripcionProductoDigital = descripcionProductoDigital;
    }

    public ServicioDTO getServicio() {
        return servicio;
    }

    public void setServicio(ServicioDTO servicio) {
        this.servicio = servicio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof IniciativaDTO)) {
            return false;
        }

        IniciativaDTO iniciativaDTO = (IniciativaDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, iniciativaDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "IniciativaDTO{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", fechaInicio='" + getFechaInicio() + "'" +
            ", fechaFin='" + getFechaFin() + "'" +
            ", fechaCreacion='" + getFechaCreacion() + "'" +
            ", isActivo='" + getIsActivo() + "'" +
            ", promesa='" + getPromesa() + "'" +
            ", tituloProductoDigital='" + getTituloProductoDigital() + "'" +
            ", descripcionProductoDigital='" + getDescripcionProductoDigital() + "'" +
            ", servicio=" + getServicio() +
            "}";
    }
}
