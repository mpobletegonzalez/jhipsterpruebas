package cl.tinet.upgrade.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link cl.tinet.upgrade.domain.Asignacion} entity.
 */
public class AsignacionDTO implements Serializable {

    private Long id;

    private Double porcentaje;

    private PersonaDTO cliente;

    private IniciativaDTO iniciativa;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getPorcentaje() {
        return porcentaje;
    }

    public void setPorcentaje(Double porcentaje) {
        this.porcentaje = porcentaje;
    }

    public PersonaDTO getCliente() {
        return cliente;
    }

    public void setCliente(PersonaDTO cliente) {
        this.cliente = cliente;
    }

    public IniciativaDTO getIniciativa() {
        return iniciativa;
    }

    public void setIniciativa(IniciativaDTO iniciativa) {
        this.iniciativa = iniciativa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof AsignacionDTO)) {
            return false;
        }

        AsignacionDTO asignacionDTO = (AsignacionDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, asignacionDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "AsignacionDTO{" +
            "id=" + getId() +
            ", porcentaje=" + getPorcentaje() +
            ", cliente=" + getCliente() +
            ", iniciativa=" + getIniciativa() +
            "}";
    }
}
