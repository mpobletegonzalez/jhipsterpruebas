package cl.tinet.upgrade.service.dto;

import cl.tinet.upgrade.domain.enumeration.TipoHito;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.Objects;
import javax.validation.constraints.*;

/**
 * A DTO for the {@link cl.tinet.upgrade.domain.Hito} entity.
 */
public class HitoDTO implements Serializable {

    private Long id;

    private String nombre;

    @NotNull
    private String descripcion;

    private ZonedDateTime fechaInicio;

    private ZonedDateTime fechaCumplimiento;

    @NotNull
    private TipoHito tipoHito;

    @NotNull
    private ZonedDateTime fechaCreacion;

    @NotNull
    private Boolean isActivo;

    private IniciativaDTO iniciativa;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ZonedDateTime getFechaInicio() {
        return fechaInicio;
    }

    public void setFechaInicio(ZonedDateTime fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public ZonedDateTime getFechaCumplimiento() {
        return fechaCumplimiento;
    }

    public void setFechaCumplimiento(ZonedDateTime fechaCumplimiento) {
        this.fechaCumplimiento = fechaCumplimiento;
    }

    public TipoHito getTipoHito() {
        return tipoHito;
    }

    public void setTipoHito(TipoHito tipoHito) {
        this.tipoHito = tipoHito;
    }

    public ZonedDateTime getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(ZonedDateTime fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Boolean getIsActivo() {
        return isActivo;
    }

    public void setIsActivo(Boolean isActivo) {
        this.isActivo = isActivo;
    }

    public IniciativaDTO getIniciativa() {
        return iniciativa;
    }

    public void setIniciativa(IniciativaDTO iniciativa) {
        this.iniciativa = iniciativa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof HitoDTO)) {
            return false;
        }

        HitoDTO hitoDTO = (HitoDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, hitoDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "HitoDTO{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", descripcion='" + getDescripcion() + "'" +
            ", fechaInicio='" + getFechaInicio() + "'" +
            ", fechaCumplimiento='" + getFechaCumplimiento() + "'" +
            ", tipoHito='" + getTipoHito() + "'" +
            ", fechaCreacion='" + getFechaCreacion() + "'" +
            ", isActivo='" + getIsActivo() + "'" +
            ", iniciativa=" + getIniciativa() +
            "}";
    }
}
