package cl.tinet.upgrade.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link cl.tinet.upgrade.domain.RegistroHistoria} entity.
 */
public class RegistroHistoriaDTO implements Serializable {

    private Long id;

    private String nombre;

    private Integer codigo;

    private FuncionalidadDTO funcionalidad;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Integer getCodigo() {
        return codigo;
    }

    public void setCodigo(Integer codigo) {
        this.codigo = codigo;
    }

    public FuncionalidadDTO getFuncionalidad() {
        return funcionalidad;
    }

    public void setFuncionalidad(FuncionalidadDTO funcionalidad) {
        this.funcionalidad = funcionalidad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof RegistroHistoriaDTO)) {
            return false;
        }

        RegistroHistoriaDTO registroHistoriaDTO = (RegistroHistoriaDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, registroHistoriaDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "RegistroHistoriaDTO{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", codigo=" + getCodigo() +
            ", funcionalidad=" + getFuncionalidad() +
            "}";
    }
}
