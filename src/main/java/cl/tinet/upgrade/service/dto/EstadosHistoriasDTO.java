package cl.tinet.upgrade.service.dto;

import java.io.Serializable;
import java.util.Objects;

/**
 * A DTO for the {@link cl.tinet.upgrade.domain.EstadosHistorias} entity.
 */
public class EstadosHistoriasDTO implements Serializable {

    private Long id;

    private Integer totalEstimacion;

    private Integer totalBacklog;

    private Integer totalEnRefinamiento;

    private Integer totalListoParaDesarrollo;

    private Integer totalEnDesarrollo;

    private Integer totalListoCertificacion;

    private Integer totalEnCertificacion;

    private Integer totalPruebasUAT;

    private Integer totalListoProduccion;

    private Integer totalEnProduccion;

    private Integer totalAprobadosPO;

    private IniciativaDTO iniciativa;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTotalEstimacion() {
        return totalEstimacion;
    }

    public void setTotalEstimacion(Integer totalEstimacion) {
        this.totalEstimacion = totalEstimacion;
    }

    public Integer getTotalBacklog() {
        return totalBacklog;
    }

    public void setTotalBacklog(Integer totalBacklog) {
        this.totalBacklog = totalBacklog;
    }

    public Integer getTotalEnRefinamiento() {
        return totalEnRefinamiento;
    }

    public void setTotalEnRefinamiento(Integer totalEnRefinamiento) {
        this.totalEnRefinamiento = totalEnRefinamiento;
    }

    public Integer getTotalListoParaDesarrollo() {
        return totalListoParaDesarrollo;
    }

    public void setTotalListoParaDesarrollo(Integer totalListoParaDesarrollo) {
        this.totalListoParaDesarrollo = totalListoParaDesarrollo;
    }

    public Integer getTotalEnDesarrollo() {
        return totalEnDesarrollo;
    }

    public void setTotalEnDesarrollo(Integer totalEnDesarrollo) {
        this.totalEnDesarrollo = totalEnDesarrollo;
    }

    public Integer getTotalListoCertificacion() {
        return totalListoCertificacion;
    }

    public void setTotalListoCertificacion(Integer totalListoCertificacion) {
        this.totalListoCertificacion = totalListoCertificacion;
    }

    public Integer getTotalEnCertificacion() {
        return totalEnCertificacion;
    }

    public void setTotalEnCertificacion(Integer totalEnCertificacion) {
        this.totalEnCertificacion = totalEnCertificacion;
    }

    public Integer getTotalPruebasUAT() {
        return totalPruebasUAT;
    }

    public void setTotalPruebasUAT(Integer totalPruebasUAT) {
        this.totalPruebasUAT = totalPruebasUAT;
    }

    public Integer getTotalListoProduccion() {
        return totalListoProduccion;
    }

    public void setTotalListoProduccion(Integer totalListoProduccion) {
        this.totalListoProduccion = totalListoProduccion;
    }

    public Integer getTotalEnProduccion() {
        return totalEnProduccion;
    }

    public void setTotalEnProduccion(Integer totalEnProduccion) {
        this.totalEnProduccion = totalEnProduccion;
    }

    public Integer getTotalAprobadosPO() {
        return totalAprobadosPO;
    }

    public void setTotalAprobadosPO(Integer totalAprobadosPO) {
        this.totalAprobadosPO = totalAprobadosPO;
    }

    public IniciativaDTO getIniciativa() {
        return iniciativa;
    }

    public void setIniciativa(IniciativaDTO iniciativa) {
        this.iniciativa = iniciativa;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EstadosHistoriasDTO)) {
            return false;
        }

        EstadosHistoriasDTO estadosHistoriasDTO = (EstadosHistoriasDTO) o;
        if (this.id == null) {
            return false;
        }
        return Objects.equals(this.id, estadosHistoriasDTO.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EstadosHistoriasDTO{" +
            "id=" + getId() +
            ", totalEstimacion=" + getTotalEstimacion() +
            ", totalBacklog=" + getTotalBacklog() +
            ", totalEnRefinamiento=" + getTotalEnRefinamiento() +
            ", totalListoParaDesarrollo=" + getTotalListoParaDesarrollo() +
            ", totalEnDesarrollo=" + getTotalEnDesarrollo() +
            ", totalListoCertificacion=" + getTotalListoCertificacion() +
            ", totalEnCertificacion=" + getTotalEnCertificacion() +
            ", totalPruebasUAT=" + getTotalPruebasUAT() +
            ", totalListoProduccion=" + getTotalListoProduccion() +
            ", totalEnProduccion=" + getTotalEnProduccion() +
            ", totalAprobadosPO=" + getTotalAprobadosPO() +
            ", iniciativa=" + getIniciativa() +
            "}";
    }
}
