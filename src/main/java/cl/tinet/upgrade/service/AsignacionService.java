package cl.tinet.upgrade.service;

import cl.tinet.upgrade.service.dto.AsignacionDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link cl.tinet.upgrade.domain.Asignacion}.
 */
public interface AsignacionService {
    /**
     * Save a asignacion.
     *
     * @param asignacionDTO the entity to save.
     * @return the persisted entity.
     */
    AsignacionDTO save(AsignacionDTO asignacionDTO);

    /**
     * Partially updates a asignacion.
     *
     * @param asignacionDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<AsignacionDTO> partialUpdate(AsignacionDTO asignacionDTO);

    /**
     * Get all the asignacions.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<AsignacionDTO> findAll(Pageable pageable);

    /**
     * Get the "id" asignacion.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<AsignacionDTO> findOne(Long id);

    /**
     * Delete the "id" asignacion.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
