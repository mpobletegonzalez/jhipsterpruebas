package cl.tinet.upgrade.service.impl;

import cl.tinet.upgrade.domain.Asignacion;
import cl.tinet.upgrade.repository.AsignacionRepository;
import cl.tinet.upgrade.service.AsignacionService;
import cl.tinet.upgrade.service.dto.AsignacionDTO;
import cl.tinet.upgrade.service.mapper.AsignacionMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Asignacion}.
 */
@Service
@Transactional
public class AsignacionServiceImpl implements AsignacionService {

    private final Logger log = LoggerFactory.getLogger(AsignacionServiceImpl.class);

    private final AsignacionRepository asignacionRepository;

    private final AsignacionMapper asignacionMapper;

    public AsignacionServiceImpl(AsignacionRepository asignacionRepository, AsignacionMapper asignacionMapper) {
        this.asignacionRepository = asignacionRepository;
        this.asignacionMapper = asignacionMapper;
    }

    @Override
    public AsignacionDTO save(AsignacionDTO asignacionDTO) {
        log.debug("Request to save Asignacion : {}", asignacionDTO);
        Asignacion asignacion = asignacionMapper.toEntity(asignacionDTO);
        asignacion = asignacionRepository.save(asignacion);
        return asignacionMapper.toDto(asignacion);
    }

    @Override
    public Optional<AsignacionDTO> partialUpdate(AsignacionDTO asignacionDTO) {
        log.debug("Request to partially update Asignacion : {}", asignacionDTO);

        return asignacionRepository
            .findById(asignacionDTO.getId())
            .map(existingAsignacion -> {
                asignacionMapper.partialUpdate(existingAsignacion, asignacionDTO);

                return existingAsignacion;
            })
            .map(asignacionRepository::save)
            .map(asignacionMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<AsignacionDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Asignacions");
        return asignacionRepository.findAll(pageable).map(asignacionMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<AsignacionDTO> findOne(Long id) {
        log.debug("Request to get Asignacion : {}", id);
        return asignacionRepository.findById(id).map(asignacionMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Asignacion : {}", id);
        asignacionRepository.deleteById(id);
    }
}
