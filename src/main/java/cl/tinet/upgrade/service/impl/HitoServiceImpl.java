package cl.tinet.upgrade.service.impl;

import cl.tinet.upgrade.domain.Hito;
import cl.tinet.upgrade.repository.HitoRepository;
import cl.tinet.upgrade.service.HitoService;
import cl.tinet.upgrade.service.dto.HitoDTO;
import cl.tinet.upgrade.service.mapper.HitoMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Hito}.
 */
@Service
@Transactional
public class HitoServiceImpl implements HitoService {

    private final Logger log = LoggerFactory.getLogger(HitoServiceImpl.class);

    private final HitoRepository hitoRepository;

    private final HitoMapper hitoMapper;

    public HitoServiceImpl(HitoRepository hitoRepository, HitoMapper hitoMapper) {
        this.hitoRepository = hitoRepository;
        this.hitoMapper = hitoMapper;
    }

    @Override
    public HitoDTO save(HitoDTO hitoDTO) {
        log.debug("Request to save Hito : {}", hitoDTO);
        Hito hito = hitoMapper.toEntity(hitoDTO);
        hito = hitoRepository.save(hito);
        return hitoMapper.toDto(hito);
    }

    @Override
    public Optional<HitoDTO> partialUpdate(HitoDTO hitoDTO) {
        log.debug("Request to partially update Hito : {}", hitoDTO);

        return hitoRepository
            .findById(hitoDTO.getId())
            .map(existingHito -> {
                hitoMapper.partialUpdate(existingHito, hitoDTO);

                return existingHito;
            })
            .map(hitoRepository::save)
            .map(hitoMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<HitoDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Hitos");
        return hitoRepository.findAll(pageable).map(hitoMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<HitoDTO> findOne(Long id) {
        log.debug("Request to get Hito : {}", id);
        return hitoRepository.findById(id).map(hitoMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Hito : {}", id);
        hitoRepository.deleteById(id);
    }
}
