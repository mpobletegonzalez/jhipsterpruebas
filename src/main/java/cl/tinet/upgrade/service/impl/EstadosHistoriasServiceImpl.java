package cl.tinet.upgrade.service.impl;

import cl.tinet.upgrade.domain.EstadosHistorias;
import cl.tinet.upgrade.repository.EstadosHistoriasRepository;
import cl.tinet.upgrade.service.EstadosHistoriasService;
import cl.tinet.upgrade.service.dto.EstadosHistoriasDTO;
import cl.tinet.upgrade.service.mapper.EstadosHistoriasMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link EstadosHistorias}.
 */
@Service
@Transactional
public class EstadosHistoriasServiceImpl implements EstadosHistoriasService {

    private final Logger log = LoggerFactory.getLogger(EstadosHistoriasServiceImpl.class);

    private final EstadosHistoriasRepository estadosHistoriasRepository;

    private final EstadosHistoriasMapper estadosHistoriasMapper;

    public EstadosHistoriasServiceImpl(
        EstadosHistoriasRepository estadosHistoriasRepository,
        EstadosHistoriasMapper estadosHistoriasMapper
    ) {
        this.estadosHistoriasRepository = estadosHistoriasRepository;
        this.estadosHistoriasMapper = estadosHistoriasMapper;
    }

    @Override
    public EstadosHistoriasDTO save(EstadosHistoriasDTO estadosHistoriasDTO) {
        log.debug("Request to save EstadosHistorias : {}", estadosHistoriasDTO);
        EstadosHistorias estadosHistorias = estadosHistoriasMapper.toEntity(estadosHistoriasDTO);
        estadosHistorias = estadosHistoriasRepository.save(estadosHistorias);
        return estadosHistoriasMapper.toDto(estadosHistorias);
    }

    @Override
    public Optional<EstadosHistoriasDTO> partialUpdate(EstadosHistoriasDTO estadosHistoriasDTO) {
        log.debug("Request to partially update EstadosHistorias : {}", estadosHistoriasDTO);

        return estadosHistoriasRepository
            .findById(estadosHistoriasDTO.getId())
            .map(existingEstadosHistorias -> {
                estadosHistoriasMapper.partialUpdate(existingEstadosHistorias, estadosHistoriasDTO);

                return existingEstadosHistorias;
            })
            .map(estadosHistoriasRepository::save)
            .map(estadosHistoriasMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<EstadosHistoriasDTO> findAll(Pageable pageable) {
        log.debug("Request to get all EstadosHistorias");
        return estadosHistoriasRepository.findAll(pageable).map(estadosHistoriasMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<EstadosHistoriasDTO> findOne(Long id) {
        log.debug("Request to get EstadosHistorias : {}", id);
        return estadosHistoriasRepository.findById(id).map(estadosHistoriasMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete EstadosHistorias : {}", id);
        estadosHistoriasRepository.deleteById(id);
    }
}
