package cl.tinet.upgrade.service.impl;

import cl.tinet.upgrade.domain.RegistroHistoria;
import cl.tinet.upgrade.repository.RegistroHistoriaRepository;
import cl.tinet.upgrade.service.RegistroHistoriaService;
import cl.tinet.upgrade.service.dto.RegistroHistoriaDTO;
import cl.tinet.upgrade.service.mapper.RegistroHistoriaMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link RegistroHistoria}.
 */
@Service
@Transactional
public class RegistroHistoriaServiceImpl implements RegistroHistoriaService {

    private final Logger log = LoggerFactory.getLogger(RegistroHistoriaServiceImpl.class);

    private final RegistroHistoriaRepository registroHistoriaRepository;

    private final RegistroHistoriaMapper registroHistoriaMapper;

    public RegistroHistoriaServiceImpl(
        RegistroHistoriaRepository registroHistoriaRepository,
        RegistroHistoriaMapper registroHistoriaMapper
    ) {
        this.registroHistoriaRepository = registroHistoriaRepository;
        this.registroHistoriaMapper = registroHistoriaMapper;
    }

    @Override
    public RegistroHistoriaDTO save(RegistroHistoriaDTO registroHistoriaDTO) {
        log.debug("Request to save RegistroHistoria : {}", registroHistoriaDTO);
        RegistroHistoria registroHistoria = registroHistoriaMapper.toEntity(registroHistoriaDTO);
        registroHistoria = registroHistoriaRepository.save(registroHistoria);
        return registroHistoriaMapper.toDto(registroHistoria);
    }

    @Override
    public Optional<RegistroHistoriaDTO> partialUpdate(RegistroHistoriaDTO registroHistoriaDTO) {
        log.debug("Request to partially update RegistroHistoria : {}", registroHistoriaDTO);

        return registroHistoriaRepository
            .findById(registroHistoriaDTO.getId())
            .map(existingRegistroHistoria -> {
                registroHistoriaMapper.partialUpdate(existingRegistroHistoria, registroHistoriaDTO);

                return existingRegistroHistoria;
            })
            .map(registroHistoriaRepository::save)
            .map(registroHistoriaMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<RegistroHistoriaDTO> findAll(Pageable pageable) {
        log.debug("Request to get all RegistroHistorias");
        return registroHistoriaRepository.findAll(pageable).map(registroHistoriaMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<RegistroHistoriaDTO> findOne(Long id) {
        log.debug("Request to get RegistroHistoria : {}", id);
        return registroHistoriaRepository.findById(id).map(registroHistoriaMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete RegistroHistoria : {}", id);
        registroHistoriaRepository.deleteById(id);
    }
}
