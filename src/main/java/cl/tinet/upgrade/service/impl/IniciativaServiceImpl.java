package cl.tinet.upgrade.service.impl;

import cl.tinet.upgrade.domain.Iniciativa;
import cl.tinet.upgrade.repository.IniciativaRepository;
import cl.tinet.upgrade.service.IniciativaService;
import cl.tinet.upgrade.service.dto.IniciativaDTO;
import cl.tinet.upgrade.service.mapper.IniciativaMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Iniciativa}.
 */
@Service
@Transactional
public class IniciativaServiceImpl implements IniciativaService {

    private final Logger log = LoggerFactory.getLogger(IniciativaServiceImpl.class);

    private final IniciativaRepository iniciativaRepository;

    private final IniciativaMapper iniciativaMapper;

    public IniciativaServiceImpl(IniciativaRepository iniciativaRepository, IniciativaMapper iniciativaMapper) {
        this.iniciativaRepository = iniciativaRepository;
        this.iniciativaMapper = iniciativaMapper;
    }

    @Override
    public IniciativaDTO save(IniciativaDTO iniciativaDTO) {
        log.debug("Request to save Iniciativa : {}", iniciativaDTO);
        Iniciativa iniciativa = iniciativaMapper.toEntity(iniciativaDTO);
        iniciativa = iniciativaRepository.save(iniciativa);
        return iniciativaMapper.toDto(iniciativa);
    }

    @Override
    public Optional<IniciativaDTO> partialUpdate(IniciativaDTO iniciativaDTO) {
        log.debug("Request to partially update Iniciativa : {}", iniciativaDTO);

        return iniciativaRepository
            .findById(iniciativaDTO.getId())
            .map(existingIniciativa -> {
                iniciativaMapper.partialUpdate(existingIniciativa, iniciativaDTO);

                return existingIniciativa;
            })
            .map(iniciativaRepository::save)
            .map(iniciativaMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<IniciativaDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Iniciativas");
        return iniciativaRepository.findAll(pageable).map(iniciativaMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<IniciativaDTO> findOne(Long id) {
        log.debug("Request to get Iniciativa : {}", id);
        return iniciativaRepository.findById(id).map(iniciativaMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Iniciativa : {}", id);
        iniciativaRepository.deleteById(id);
    }
}
