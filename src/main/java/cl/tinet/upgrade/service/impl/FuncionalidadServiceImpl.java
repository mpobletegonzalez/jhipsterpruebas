package cl.tinet.upgrade.service.impl;

import cl.tinet.upgrade.domain.Funcionalidad;
import cl.tinet.upgrade.repository.FuncionalidadRepository;
import cl.tinet.upgrade.service.FuncionalidadService;
import cl.tinet.upgrade.service.dto.FuncionalidadDTO;
import cl.tinet.upgrade.service.mapper.FuncionalidadMapper;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Service Implementation for managing {@link Funcionalidad}.
 */
@Service
@Transactional
public class FuncionalidadServiceImpl implements FuncionalidadService {

    private final Logger log = LoggerFactory.getLogger(FuncionalidadServiceImpl.class);

    private final FuncionalidadRepository funcionalidadRepository;

    private final FuncionalidadMapper funcionalidadMapper;

    public FuncionalidadServiceImpl(FuncionalidadRepository funcionalidadRepository, FuncionalidadMapper funcionalidadMapper) {
        this.funcionalidadRepository = funcionalidadRepository;
        this.funcionalidadMapper = funcionalidadMapper;
    }

    @Override
    public FuncionalidadDTO save(FuncionalidadDTO funcionalidadDTO) {
        log.debug("Request to save Funcionalidad : {}", funcionalidadDTO);
        Funcionalidad funcionalidad = funcionalidadMapper.toEntity(funcionalidadDTO);
        funcionalidad = funcionalidadRepository.save(funcionalidad);
        return funcionalidadMapper.toDto(funcionalidad);
    }

    @Override
    public Optional<FuncionalidadDTO> partialUpdate(FuncionalidadDTO funcionalidadDTO) {
        log.debug("Request to partially update Funcionalidad : {}", funcionalidadDTO);

        return funcionalidadRepository
            .findById(funcionalidadDTO.getId())
            .map(existingFuncionalidad -> {
                funcionalidadMapper.partialUpdate(existingFuncionalidad, funcionalidadDTO);

                return existingFuncionalidad;
            })
            .map(funcionalidadRepository::save)
            .map(funcionalidadMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Page<FuncionalidadDTO> findAll(Pageable pageable) {
        log.debug("Request to get all Funcionalidads");
        return funcionalidadRepository.findAll(pageable).map(funcionalidadMapper::toDto);
    }

    @Override
    @Transactional(readOnly = true)
    public Optional<FuncionalidadDTO> findOne(Long id) {
        log.debug("Request to get Funcionalidad : {}", id);
        return funcionalidadRepository.findById(id).map(funcionalidadMapper::toDto);
    }

    @Override
    public void delete(Long id) {
        log.debug("Request to delete Funcionalidad : {}", id);
        funcionalidadRepository.deleteById(id);
    }
}
