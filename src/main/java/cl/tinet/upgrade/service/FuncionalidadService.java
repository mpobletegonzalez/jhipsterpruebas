package cl.tinet.upgrade.service;

import cl.tinet.upgrade.service.dto.FuncionalidadDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link cl.tinet.upgrade.domain.Funcionalidad}.
 */
public interface FuncionalidadService {
    /**
     * Save a funcionalidad.
     *
     * @param funcionalidadDTO the entity to save.
     * @return the persisted entity.
     */
    FuncionalidadDTO save(FuncionalidadDTO funcionalidadDTO);

    /**
     * Partially updates a funcionalidad.
     *
     * @param funcionalidadDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<FuncionalidadDTO> partialUpdate(FuncionalidadDTO funcionalidadDTO);

    /**
     * Get all the funcionalidads.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<FuncionalidadDTO> findAll(Pageable pageable);

    /**
     * Get the "id" funcionalidad.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<FuncionalidadDTO> findOne(Long id);

    /**
     * Delete the "id" funcionalidad.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
