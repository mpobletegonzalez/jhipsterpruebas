package cl.tinet.upgrade.service.mapper;

import cl.tinet.upgrade.domain.Servicio;
import cl.tinet.upgrade.service.dto.ServicioDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Servicio} and its DTO {@link ServicioDTO}.
 */
@Mapper(componentModel = "spring", uses = { ClienteMapper.class })
public interface ServicioMapper extends EntityMapper<ServicioDTO, Servicio> {
    @Mapping(target = "cliente", source = "cliente", qualifiedByName = "nombre")
    ServicioDTO toDto(Servicio s);

    @Named("nombre")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "nombre", source = "nombre")
    ServicioDTO toDtoNombre(Servicio servicio);
}
