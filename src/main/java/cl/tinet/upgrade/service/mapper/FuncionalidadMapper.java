package cl.tinet.upgrade.service.mapper;

import cl.tinet.upgrade.domain.Funcionalidad;
import cl.tinet.upgrade.service.dto.FuncionalidadDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Funcionalidad} and its DTO {@link FuncionalidadDTO}.
 */
@Mapper(componentModel = "spring", uses = {})
public interface FuncionalidadMapper extends EntityMapper<FuncionalidadDTO, Funcionalidad> {
    @Named("nombre")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "nombre", source = "nombre")
    FuncionalidadDTO toDtoNombre(Funcionalidad funcionalidad);
}
