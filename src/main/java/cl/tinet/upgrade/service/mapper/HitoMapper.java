package cl.tinet.upgrade.service.mapper;

import cl.tinet.upgrade.domain.Hito;
import cl.tinet.upgrade.service.dto.HitoDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Hito} and its DTO {@link HitoDTO}.
 */
@Mapper(componentModel = "spring", uses = { IniciativaMapper.class })
public interface HitoMapper extends EntityMapper<HitoDTO, Hito> {
    @Mapping(target = "iniciativa", source = "iniciativa", qualifiedByName = "id")
    HitoDTO toDto(Hito s);
}
