package cl.tinet.upgrade.service.mapper;

import cl.tinet.upgrade.domain.Iniciativa;
import cl.tinet.upgrade.service.dto.IniciativaDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Iniciativa} and its DTO {@link IniciativaDTO}.
 */
@Mapper(componentModel = "spring", uses = { ServicioMapper.class })
public interface IniciativaMapper extends EntityMapper<IniciativaDTO, Iniciativa> {
    @Mapping(target = "servicio", source = "servicio", qualifiedByName = "nombre")
    IniciativaDTO toDto(Iniciativa s);

    @Named("id")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    IniciativaDTO toDtoId(Iniciativa iniciativa);

    @Named("nombre")
    @BeanMapping(ignoreByDefault = true)
    @Mapping(target = "id", source = "id")
    @Mapping(target = "nombre", source = "nombre")
    IniciativaDTO toDtoNombre(Iniciativa iniciativa);
}
