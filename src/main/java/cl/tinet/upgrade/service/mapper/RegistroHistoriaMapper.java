package cl.tinet.upgrade.service.mapper;

import cl.tinet.upgrade.domain.RegistroHistoria;
import cl.tinet.upgrade.service.dto.RegistroHistoriaDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link RegistroHistoria} and its DTO {@link RegistroHistoriaDTO}.
 */
@Mapper(componentModel = "spring", uses = { FuncionalidadMapper.class })
public interface RegistroHistoriaMapper extends EntityMapper<RegistroHistoriaDTO, RegistroHistoria> {
    @Mapping(target = "funcionalidad", source = "funcionalidad", qualifiedByName = "nombre")
    RegistroHistoriaDTO toDto(RegistroHistoria s);
}
