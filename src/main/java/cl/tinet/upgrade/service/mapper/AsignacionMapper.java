package cl.tinet.upgrade.service.mapper;

import cl.tinet.upgrade.domain.Asignacion;
import cl.tinet.upgrade.service.dto.AsignacionDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link Asignacion} and its DTO {@link AsignacionDTO}.
 */
@Mapper(componentModel = "spring", uses = { PersonaMapper.class, IniciativaMapper.class })
public interface AsignacionMapper extends EntityMapper<AsignacionDTO, Asignacion> {
    @Mapping(target = "cliente", source = "cliente", qualifiedByName = "nombre")
    @Mapping(target = "iniciativa", source = "iniciativa", qualifiedByName = "nombre")
    AsignacionDTO toDto(Asignacion s);
}
