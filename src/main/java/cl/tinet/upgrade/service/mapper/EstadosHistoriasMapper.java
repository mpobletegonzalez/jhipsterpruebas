package cl.tinet.upgrade.service.mapper;

import cl.tinet.upgrade.domain.EstadosHistorias;
import cl.tinet.upgrade.service.dto.EstadosHistoriasDTO;
import org.mapstruct.*;

/**
 * Mapper for the entity {@link EstadosHistorias} and its DTO {@link EstadosHistoriasDTO}.
 */
@Mapper(componentModel = "spring", uses = { IniciativaMapper.class })
public interface EstadosHistoriasMapper extends EntityMapper<EstadosHistoriasDTO, EstadosHistorias> {
    @Mapping(target = "iniciativa", source = "iniciativa", qualifiedByName = "nombre")
    EstadosHistoriasDTO toDto(EstadosHistorias s);
}
