package cl.tinet.upgrade.service;

import cl.tinet.upgrade.service.dto.EstadosHistoriasDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link cl.tinet.upgrade.domain.EstadosHistorias}.
 */
public interface EstadosHistoriasService {
    /**
     * Save a estadosHistorias.
     *
     * @param estadosHistoriasDTO the entity to save.
     * @return the persisted entity.
     */
    EstadosHistoriasDTO save(EstadosHistoriasDTO estadosHistoriasDTO);

    /**
     * Partially updates a estadosHistorias.
     *
     * @param estadosHistoriasDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<EstadosHistoriasDTO> partialUpdate(EstadosHistoriasDTO estadosHistoriasDTO);

    /**
     * Get all the estadosHistorias.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<EstadosHistoriasDTO> findAll(Pageable pageable);

    /**
     * Get the "id" estadosHistorias.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<EstadosHistoriasDTO> findOne(Long id);

    /**
     * Delete the "id" estadosHistorias.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
