package cl.tinet.upgrade.service;

import cl.tinet.upgrade.service.dto.HitoDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link cl.tinet.upgrade.domain.Hito}.
 */
public interface HitoService {
    /**
     * Save a hito.
     *
     * @param hitoDTO the entity to save.
     * @return the persisted entity.
     */
    HitoDTO save(HitoDTO hitoDTO);

    /**
     * Partially updates a hito.
     *
     * @param hitoDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<HitoDTO> partialUpdate(HitoDTO hitoDTO);

    /**
     * Get all the hitos.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<HitoDTO> findAll(Pageable pageable);

    /**
     * Get the "id" hito.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<HitoDTO> findOne(Long id);

    /**
     * Delete the "id" hito.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
