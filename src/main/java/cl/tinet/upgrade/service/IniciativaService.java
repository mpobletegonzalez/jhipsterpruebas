package cl.tinet.upgrade.service;

import cl.tinet.upgrade.service.dto.IniciativaDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link cl.tinet.upgrade.domain.Iniciativa}.
 */
public interface IniciativaService {
    /**
     * Save a iniciativa.
     *
     * @param iniciativaDTO the entity to save.
     * @return the persisted entity.
     */
    IniciativaDTO save(IniciativaDTO iniciativaDTO);

    /**
     * Partially updates a iniciativa.
     *
     * @param iniciativaDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<IniciativaDTO> partialUpdate(IniciativaDTO iniciativaDTO);

    /**
     * Get all the iniciativas.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<IniciativaDTO> findAll(Pageable pageable);

    /**
     * Get the "id" iniciativa.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<IniciativaDTO> findOne(Long id);

    /**
     * Delete the "id" iniciativa.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
