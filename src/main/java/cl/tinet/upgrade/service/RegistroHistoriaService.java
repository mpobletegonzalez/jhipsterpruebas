package cl.tinet.upgrade.service;

import cl.tinet.upgrade.service.dto.RegistroHistoriaDTO;
import java.util.Optional;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Service Interface for managing {@link cl.tinet.upgrade.domain.RegistroHistoria}.
 */
public interface RegistroHistoriaService {
    /**
     * Save a registroHistoria.
     *
     * @param registroHistoriaDTO the entity to save.
     * @return the persisted entity.
     */
    RegistroHistoriaDTO save(RegistroHistoriaDTO registroHistoriaDTO);

    /**
     * Partially updates a registroHistoria.
     *
     * @param registroHistoriaDTO the entity to update partially.
     * @return the persisted entity.
     */
    Optional<RegistroHistoriaDTO> partialUpdate(RegistroHistoriaDTO registroHistoriaDTO);

    /**
     * Get all the registroHistorias.
     *
     * @param pageable the pagination information.
     * @return the list of entities.
     */
    Page<RegistroHistoriaDTO> findAll(Pageable pageable);

    /**
     * Get the "id" registroHistoria.
     *
     * @param id the id of the entity.
     * @return the entity.
     */
    Optional<RegistroHistoriaDTO> findOne(Long id);

    /**
     * Delete the "id" registroHistoria.
     *
     * @param id the id of the entity.
     */
    void delete(Long id);
}
