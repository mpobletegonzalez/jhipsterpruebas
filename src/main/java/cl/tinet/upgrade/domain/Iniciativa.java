package cl.tinet.upgrade.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Iniciativa.
 */
@Entity
@Table(name = "iniciativa")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Iniciativa implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "nombre", nullable = false)
    private String nombre;

    @NotNull
    @Column(name = "fecha_inicio", nullable = false)
    private ZonedDateTime fechaInicio;

    @NotNull
    @Column(name = "fecha_fin", nullable = false)
    private ZonedDateTime fechaFin;

    @NotNull
    @Column(name = "fecha_creacion", nullable = false)
    private ZonedDateTime fechaCreacion;

    @NotNull
    @Column(name = "is_activo", nullable = false)
    private Boolean isActivo;

    @NotNull
    @Column(name = "promesa", nullable = false)
    private String promesa;

    @NotNull
    @Column(name = "titulo_producto_digital", nullable = false)
    private String tituloProductoDigital;

    @NotNull
    @Column(name = "descripcion_producto_digital", nullable = false)
    private String descripcionProductoDigital;

    @OneToMany(mappedBy = "iniciativa")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "iniciativa" }, allowSetters = true)
    private Set<Hito> hitos = new HashSet<>();

    @OneToMany(mappedBy = "iniciativa")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "iniciativa" }, allowSetters = true)
    private Set<EstadosHistorias> estadosHistorias = new HashSet<>();

    @OneToMany(mappedBy = "iniciativa")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "cliente", "iniciativa" }, allowSetters = true)
    private Set<Asignacion> asignacions = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "iniciativas", "cliente" }, allowSetters = true)
    private Servicio servicio;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Iniciativa id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public Iniciativa nombre(String nombre) {
        this.setNombre(nombre);
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public ZonedDateTime getFechaInicio() {
        return this.fechaInicio;
    }

    public Iniciativa fechaInicio(ZonedDateTime fechaInicio) {
        this.setFechaInicio(fechaInicio);
        return this;
    }

    public void setFechaInicio(ZonedDateTime fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public ZonedDateTime getFechaFin() {
        return this.fechaFin;
    }

    public Iniciativa fechaFin(ZonedDateTime fechaFin) {
        this.setFechaFin(fechaFin);
        return this;
    }

    public void setFechaFin(ZonedDateTime fechaFin) {
        this.fechaFin = fechaFin;
    }

    public ZonedDateTime getFechaCreacion() {
        return this.fechaCreacion;
    }

    public Iniciativa fechaCreacion(ZonedDateTime fechaCreacion) {
        this.setFechaCreacion(fechaCreacion);
        return this;
    }

    public void setFechaCreacion(ZonedDateTime fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Boolean getIsActivo() {
        return this.isActivo;
    }

    public Iniciativa isActivo(Boolean isActivo) {
        this.setIsActivo(isActivo);
        return this;
    }

    public void setIsActivo(Boolean isActivo) {
        this.isActivo = isActivo;
    }

    public String getPromesa() {
        return this.promesa;
    }

    public Iniciativa promesa(String promesa) {
        this.setPromesa(promesa);
        return this;
    }

    public void setPromesa(String promesa) {
        this.promesa = promesa;
    }

    public String getTituloProductoDigital() {
        return this.tituloProductoDigital;
    }

    public Iniciativa tituloProductoDigital(String tituloProductoDigital) {
        this.setTituloProductoDigital(tituloProductoDigital);
        return this;
    }

    public void setTituloProductoDigital(String tituloProductoDigital) {
        this.tituloProductoDigital = tituloProductoDigital;
    }

    public String getDescripcionProductoDigital() {
        return this.descripcionProductoDigital;
    }

    public Iniciativa descripcionProductoDigital(String descripcionProductoDigital) {
        this.setDescripcionProductoDigital(descripcionProductoDigital);
        return this;
    }

    public void setDescripcionProductoDigital(String descripcionProductoDigital) {
        this.descripcionProductoDigital = descripcionProductoDigital;
    }

    public Set<Hito> getHitos() {
        return this.hitos;
    }

    public void setHitos(Set<Hito> hitos) {
        if (this.hitos != null) {
            this.hitos.forEach(i -> i.setIniciativa(null));
        }
        if (hitos != null) {
            hitos.forEach(i -> i.setIniciativa(this));
        }
        this.hitos = hitos;
    }

    public Iniciativa hitos(Set<Hito> hitos) {
        this.setHitos(hitos);
        return this;
    }

    public Iniciativa addHito(Hito hito) {
        this.hitos.add(hito);
        hito.setIniciativa(this);
        return this;
    }

    public Iniciativa removeHito(Hito hito) {
        this.hitos.remove(hito);
        hito.setIniciativa(null);
        return this;
    }

    public Set<EstadosHistorias> getEstadosHistorias() {
        return this.estadosHistorias;
    }

    public void setEstadosHistorias(Set<EstadosHistorias> estadosHistorias) {
        if (this.estadosHistorias != null) {
            this.estadosHistorias.forEach(i -> i.setIniciativa(null));
        }
        if (estadosHistorias != null) {
            estadosHistorias.forEach(i -> i.setIniciativa(this));
        }
        this.estadosHistorias = estadosHistorias;
    }

    public Iniciativa estadosHistorias(Set<EstadosHistorias> estadosHistorias) {
        this.setEstadosHistorias(estadosHistorias);
        return this;
    }

    public Iniciativa addEstadosHistorias(EstadosHistorias estadosHistorias) {
        this.estadosHistorias.add(estadosHistorias);
        estadosHistorias.setIniciativa(this);
        return this;
    }

    public Iniciativa removeEstadosHistorias(EstadosHistorias estadosHistorias) {
        this.estadosHistorias.remove(estadosHistorias);
        estadosHistorias.setIniciativa(null);
        return this;
    }

    public Set<Asignacion> getAsignacions() {
        return this.asignacions;
    }

    public void setAsignacions(Set<Asignacion> asignacions) {
        if (this.asignacions != null) {
            this.asignacions.forEach(i -> i.setIniciativa(null));
        }
        if (asignacions != null) {
            asignacions.forEach(i -> i.setIniciativa(this));
        }
        this.asignacions = asignacions;
    }

    public Iniciativa asignacions(Set<Asignacion> asignacions) {
        this.setAsignacions(asignacions);
        return this;
    }

    public Iniciativa addAsignacion(Asignacion asignacion) {
        this.asignacions.add(asignacion);
        asignacion.setIniciativa(this);
        return this;
    }

    public Iniciativa removeAsignacion(Asignacion asignacion) {
        this.asignacions.remove(asignacion);
        asignacion.setIniciativa(null);
        return this;
    }

    public Servicio getServicio() {
        return this.servicio;
    }

    public void setServicio(Servicio servicio) {
        this.servicio = servicio;
    }

    public Iniciativa servicio(Servicio servicio) {
        this.setServicio(servicio);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Iniciativa)) {
            return false;
        }
        return id != null && id.equals(((Iniciativa) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Iniciativa{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", fechaInicio='" + getFechaInicio() + "'" +
            ", fechaFin='" + getFechaFin() + "'" +
            ", fechaCreacion='" + getFechaCreacion() + "'" +
            ", isActivo='" + getIsActivo() + "'" +
            ", promesa='" + getPromesa() + "'" +
            ", tituloProductoDigital='" + getTituloProductoDigital() + "'" +
            ", descripcionProductoDigital='" + getDescripcionProductoDigital() + "'" +
            "}";
    }
}
