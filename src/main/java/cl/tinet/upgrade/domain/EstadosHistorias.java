package cl.tinet.upgrade.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A EstadosHistorias.
 */
@Entity
@Table(name = "estados_historias")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class EstadosHistorias implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "total_estimacion")
    private Integer totalEstimacion;

    @Column(name = "total_backlog")
    private Integer totalBacklog;

    @Column(name = "total_en_refinamiento")
    private Integer totalEnRefinamiento;

    @Column(name = "total_listo_para_desarrollo")
    private Integer totalListoParaDesarrollo;

    @Column(name = "total_en_desarrollo")
    private Integer totalEnDesarrollo;

    @Column(name = "total_listo_certificacion")
    private Integer totalListoCertificacion;

    @Column(name = "total_en_certificacion")
    private Integer totalEnCertificacion;

    @Column(name = "total_pruebas_uat")
    private Integer totalPruebasUAT;

    @Column(name = "total_listo_produccion")
    private Integer totalListoProduccion;

    @Column(name = "total_en_produccion")
    private Integer totalEnProduccion;

    @Column(name = "total_aprobados_po")
    private Integer totalAprobadosPO;

    @ManyToOne
    @JsonIgnoreProperties(value = { "hitos", "estadosHistorias", "asignacions", "servicio" }, allowSetters = true)
    private Iniciativa iniciativa;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public EstadosHistorias id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Integer getTotalEstimacion() {
        return this.totalEstimacion;
    }

    public EstadosHistorias totalEstimacion(Integer totalEstimacion) {
        this.setTotalEstimacion(totalEstimacion);
        return this;
    }

    public void setTotalEstimacion(Integer totalEstimacion) {
        this.totalEstimacion = totalEstimacion;
    }

    public Integer getTotalBacklog() {
        return this.totalBacklog;
    }

    public EstadosHistorias totalBacklog(Integer totalBacklog) {
        this.setTotalBacklog(totalBacklog);
        return this;
    }

    public void setTotalBacklog(Integer totalBacklog) {
        this.totalBacklog = totalBacklog;
    }

    public Integer getTotalEnRefinamiento() {
        return this.totalEnRefinamiento;
    }

    public EstadosHistorias totalEnRefinamiento(Integer totalEnRefinamiento) {
        this.setTotalEnRefinamiento(totalEnRefinamiento);
        return this;
    }

    public void setTotalEnRefinamiento(Integer totalEnRefinamiento) {
        this.totalEnRefinamiento = totalEnRefinamiento;
    }

    public Integer getTotalListoParaDesarrollo() {
        return this.totalListoParaDesarrollo;
    }

    public EstadosHistorias totalListoParaDesarrollo(Integer totalListoParaDesarrollo) {
        this.setTotalListoParaDesarrollo(totalListoParaDesarrollo);
        return this;
    }

    public void setTotalListoParaDesarrollo(Integer totalListoParaDesarrollo) {
        this.totalListoParaDesarrollo = totalListoParaDesarrollo;
    }

    public Integer getTotalEnDesarrollo() {
        return this.totalEnDesarrollo;
    }

    public EstadosHistorias totalEnDesarrollo(Integer totalEnDesarrollo) {
        this.setTotalEnDesarrollo(totalEnDesarrollo);
        return this;
    }

    public void setTotalEnDesarrollo(Integer totalEnDesarrollo) {
        this.totalEnDesarrollo = totalEnDesarrollo;
    }

    public Integer getTotalListoCertificacion() {
        return this.totalListoCertificacion;
    }

    public EstadosHistorias totalListoCertificacion(Integer totalListoCertificacion) {
        this.setTotalListoCertificacion(totalListoCertificacion);
        return this;
    }

    public void setTotalListoCertificacion(Integer totalListoCertificacion) {
        this.totalListoCertificacion = totalListoCertificacion;
    }

    public Integer getTotalEnCertificacion() {
        return this.totalEnCertificacion;
    }

    public EstadosHistorias totalEnCertificacion(Integer totalEnCertificacion) {
        this.setTotalEnCertificacion(totalEnCertificacion);
        return this;
    }

    public void setTotalEnCertificacion(Integer totalEnCertificacion) {
        this.totalEnCertificacion = totalEnCertificacion;
    }

    public Integer getTotalPruebasUAT() {
        return this.totalPruebasUAT;
    }

    public EstadosHistorias totalPruebasUAT(Integer totalPruebasUAT) {
        this.setTotalPruebasUAT(totalPruebasUAT);
        return this;
    }

    public void setTotalPruebasUAT(Integer totalPruebasUAT) {
        this.totalPruebasUAT = totalPruebasUAT;
    }

    public Integer getTotalListoProduccion() {
        return this.totalListoProduccion;
    }

    public EstadosHistorias totalListoProduccion(Integer totalListoProduccion) {
        this.setTotalListoProduccion(totalListoProduccion);
        return this;
    }

    public void setTotalListoProduccion(Integer totalListoProduccion) {
        this.totalListoProduccion = totalListoProduccion;
    }

    public Integer getTotalEnProduccion() {
        return this.totalEnProduccion;
    }

    public EstadosHistorias totalEnProduccion(Integer totalEnProduccion) {
        this.setTotalEnProduccion(totalEnProduccion);
        return this;
    }

    public void setTotalEnProduccion(Integer totalEnProduccion) {
        this.totalEnProduccion = totalEnProduccion;
    }

    public Integer getTotalAprobadosPO() {
        return this.totalAprobadosPO;
    }

    public EstadosHistorias totalAprobadosPO(Integer totalAprobadosPO) {
        this.setTotalAprobadosPO(totalAprobadosPO);
        return this;
    }

    public void setTotalAprobadosPO(Integer totalAprobadosPO) {
        this.totalAprobadosPO = totalAprobadosPO;
    }

    public Iniciativa getIniciativa() {
        return this.iniciativa;
    }

    public void setIniciativa(Iniciativa iniciativa) {
        this.iniciativa = iniciativa;
    }

    public EstadosHistorias iniciativa(Iniciativa iniciativa) {
        this.setIniciativa(iniciativa);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof EstadosHistorias)) {
            return false;
        }
        return id != null && id.equals(((EstadosHistorias) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "EstadosHistorias{" +
            "id=" + getId() +
            ", totalEstimacion=" + getTotalEstimacion() +
            ", totalBacklog=" + getTotalBacklog() +
            ", totalEnRefinamiento=" + getTotalEnRefinamiento() +
            ", totalListoParaDesarrollo=" + getTotalListoParaDesarrollo() +
            ", totalEnDesarrollo=" + getTotalEnDesarrollo() +
            ", totalListoCertificacion=" + getTotalListoCertificacion() +
            ", totalEnCertificacion=" + getTotalEnCertificacion() +
            ", totalPruebasUAT=" + getTotalPruebasUAT() +
            ", totalListoProduccion=" + getTotalListoProduccion() +
            ", totalEnProduccion=" + getTotalEnProduccion() +
            ", totalAprobadosPO=" + getTotalAprobadosPO() +
            "}";
    }
}
