package cl.tinet.upgrade.domain;

import cl.tinet.upgrade.domain.enumeration.TipoEstadoFuncionalidad;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Funcionalidad.
 */
@Entity
@Table(name = "funcionalidad")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Funcionalidad implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @Enumerated(EnumType.STRING)
    @Column(name = "estado")
    private TipoEstadoFuncionalidad estado;

    @Column(name = "prioridad")
    private Integer prioridad;

    @OneToMany(mappedBy = "funcionalidad")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "funcionalidad" }, allowSetters = true)
    private Set<RegistroHistoria> registroHistorias = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Funcionalidad id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public Funcionalidad nombre(String nombre) {
        this.setNombre(nombre);
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public TipoEstadoFuncionalidad getEstado() {
        return this.estado;
    }

    public Funcionalidad estado(TipoEstadoFuncionalidad estado) {
        this.setEstado(estado);
        return this;
    }

    public void setEstado(TipoEstadoFuncionalidad estado) {
        this.estado = estado;
    }

    public Integer getPrioridad() {
        return this.prioridad;
    }

    public Funcionalidad prioridad(Integer prioridad) {
        this.setPrioridad(prioridad);
        return this;
    }

    public void setPrioridad(Integer prioridad) {
        this.prioridad = prioridad;
    }

    public Set<RegistroHistoria> getRegistroHistorias() {
        return this.registroHistorias;
    }

    public void setRegistroHistorias(Set<RegistroHistoria> registroHistorias) {
        if (this.registroHistorias != null) {
            this.registroHistorias.forEach(i -> i.setFuncionalidad(null));
        }
        if (registroHistorias != null) {
            registroHistorias.forEach(i -> i.setFuncionalidad(this));
        }
        this.registroHistorias = registroHistorias;
    }

    public Funcionalidad registroHistorias(Set<RegistroHistoria> registroHistorias) {
        this.setRegistroHistorias(registroHistorias);
        return this;
    }

    public Funcionalidad addRegistroHistoria(RegistroHistoria registroHistoria) {
        this.registroHistorias.add(registroHistoria);
        registroHistoria.setFuncionalidad(this);
        return this;
    }

    public Funcionalidad removeRegistroHistoria(RegistroHistoria registroHistoria) {
        this.registroHistorias.remove(registroHistoria);
        registroHistoria.setFuncionalidad(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Funcionalidad)) {
            return false;
        }
        return id != null && id.equals(((Funcionalidad) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Funcionalidad{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", estado='" + getEstado() + "'" +
            ", prioridad=" + getPrioridad() +
            "}";
    }
}
