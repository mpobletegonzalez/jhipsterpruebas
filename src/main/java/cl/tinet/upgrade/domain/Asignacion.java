package cl.tinet.upgrade.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Asignacion.
 */
@Entity
@Table(name = "asignacion")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Asignacion implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "porcentaje")
    private Double porcentaje;

    @ManyToOne
    @JsonIgnoreProperties(value = { "asignacions" }, allowSetters = true)
    private Persona cliente;

    @ManyToOne
    @JsonIgnoreProperties(value = { "hitos", "estadosHistorias", "asignacions", "servicio" }, allowSetters = true)
    private Iniciativa iniciativa;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Asignacion id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getPorcentaje() {
        return this.porcentaje;
    }

    public Asignacion porcentaje(Double porcentaje) {
        this.setPorcentaje(porcentaje);
        return this;
    }

    public void setPorcentaje(Double porcentaje) {
        this.porcentaje = porcentaje;
    }

    public Persona getCliente() {
        return this.cliente;
    }

    public void setCliente(Persona persona) {
        this.cliente = persona;
    }

    public Asignacion cliente(Persona persona) {
        this.setCliente(persona);
        return this;
    }

    public Iniciativa getIniciativa() {
        return this.iniciativa;
    }

    public void setIniciativa(Iniciativa iniciativa) {
        this.iniciativa = iniciativa;
    }

    public Asignacion iniciativa(Iniciativa iniciativa) {
        this.setIniciativa(iniciativa);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Asignacion)) {
            return false;
        }
        return id != null && id.equals(((Asignacion) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Asignacion{" +
            "id=" + getId() +
            ", porcentaje=" + getPorcentaje() +
            "}";
    }
}
