package cl.tinet.upgrade.domain;

import cl.tinet.upgrade.domain.enumeration.TipoHito;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.ZonedDateTime;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Hito.
 */
@Entity
@Table(name = "hito")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Hito implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "nombre")
    private String nombre;

    @NotNull
    @Column(name = "descripcion", nullable = false)
    private String descripcion;

    @Column(name = "fecha_inicio")
    private ZonedDateTime fechaInicio;

    @Column(name = "fecha_cumplimiento")
    private ZonedDateTime fechaCumplimiento;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "tipo_hito", nullable = false)
    private TipoHito tipoHito;

    @NotNull
    @Column(name = "fecha_creacion", nullable = false)
    private ZonedDateTime fechaCreacion;

    @NotNull
    @Column(name = "is_activo", nullable = false)
    private Boolean isActivo;

    @ManyToOne
    @JsonIgnoreProperties(value = { "hitos", "estadosHistorias", "asignacions", "servicio" }, allowSetters = true)
    private Iniciativa iniciativa;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Hito id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public Hito nombre(String nombre) {
        this.setNombre(nombre);
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return this.descripcion;
    }

    public Hito descripcion(String descripcion) {
        this.setDescripcion(descripcion);
        return this;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public ZonedDateTime getFechaInicio() {
        return this.fechaInicio;
    }

    public Hito fechaInicio(ZonedDateTime fechaInicio) {
        this.setFechaInicio(fechaInicio);
        return this;
    }

    public void setFechaInicio(ZonedDateTime fechaInicio) {
        this.fechaInicio = fechaInicio;
    }

    public ZonedDateTime getFechaCumplimiento() {
        return this.fechaCumplimiento;
    }

    public Hito fechaCumplimiento(ZonedDateTime fechaCumplimiento) {
        this.setFechaCumplimiento(fechaCumplimiento);
        return this;
    }

    public void setFechaCumplimiento(ZonedDateTime fechaCumplimiento) {
        this.fechaCumplimiento = fechaCumplimiento;
    }

    public TipoHito getTipoHito() {
        return this.tipoHito;
    }

    public Hito tipoHito(TipoHito tipoHito) {
        this.setTipoHito(tipoHito);
        return this;
    }

    public void setTipoHito(TipoHito tipoHito) {
        this.tipoHito = tipoHito;
    }

    public ZonedDateTime getFechaCreacion() {
        return this.fechaCreacion;
    }

    public Hito fechaCreacion(ZonedDateTime fechaCreacion) {
        this.setFechaCreacion(fechaCreacion);
        return this;
    }

    public void setFechaCreacion(ZonedDateTime fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Boolean getIsActivo() {
        return this.isActivo;
    }

    public Hito isActivo(Boolean isActivo) {
        this.setIsActivo(isActivo);
        return this;
    }

    public void setIsActivo(Boolean isActivo) {
        this.isActivo = isActivo;
    }

    public Iniciativa getIniciativa() {
        return this.iniciativa;
    }

    public void setIniciativa(Iniciativa iniciativa) {
        this.iniciativa = iniciativa;
    }

    public Hito iniciativa(Iniciativa iniciativa) {
        this.setIniciativa(iniciativa);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Hito)) {
            return false;
        }
        return id != null && id.equals(((Hito) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Hito{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", descripcion='" + getDescripcion() + "'" +
            ", fechaInicio='" + getFechaInicio() + "'" +
            ", fechaCumplimiento='" + getFechaCumplimiento() + "'" +
            ", tipoHito='" + getTipoHito() + "'" +
            ", fechaCreacion='" + getFechaCreacion() + "'" +
            ", isActivo='" + getIsActivo() + "'" +
            "}";
    }
}
