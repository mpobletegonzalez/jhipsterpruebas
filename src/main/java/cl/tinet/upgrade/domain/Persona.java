package cl.tinet.upgrade.domain;

import cl.tinet.upgrade.domain.enumeration.TipoCargo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Persona.
 */
@Entity
@Table(name = "persona")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Persona implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "nombre", nullable = false)
    private String nombre;

    @NotNull
    @Column(name = "apellido", nullable = false)
    private String apellido;

    @Column(name = "img")
    private String img;

    @NotNull
    @Column(name = "email", nullable = false, unique = true)
    private String email;

    @Column(name = "login")
    private String login;

    @Column(name = "user_id", unique = true)
    private Integer userId;

    @NotNull
    @Column(name = "fecha_creacion", nullable = false)
    private ZonedDateTime fechaCreacion;

    @NotNull
    @Column(name = "is_activo", nullable = false)
    private Boolean isActivo;

    @Enumerated(EnumType.STRING)
    @Column(name = "cargo")
    private TipoCargo cargo;

    @OneToMany(mappedBy = "cliente")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "cliente", "iniciativa" }, allowSetters = true)
    private Set<Asignacion> asignacions = new HashSet<>();

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Persona id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public Persona nombre(String nombre) {
        this.setNombre(nombre);
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return this.apellido;
    }

    public Persona apellido(String apellido) {
        this.setApellido(apellido);
        return this;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public String getImg() {
        return this.img;
    }

    public Persona img(String img) {
        this.setImg(img);
        return this;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getEmail() {
        return this.email;
    }

    public Persona email(String email) {
        this.setEmail(email);
        return this;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return this.login;
    }

    public Persona login(String login) {
        this.setLogin(login);
        return this;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Integer getUserId() {
        return this.userId;
    }

    public Persona userId(Integer userId) {
        this.setUserId(userId);
        return this;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public ZonedDateTime getFechaCreacion() {
        return this.fechaCreacion;
    }

    public Persona fechaCreacion(ZonedDateTime fechaCreacion) {
        this.setFechaCreacion(fechaCreacion);
        return this;
    }

    public void setFechaCreacion(ZonedDateTime fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Boolean getIsActivo() {
        return this.isActivo;
    }

    public Persona isActivo(Boolean isActivo) {
        this.setIsActivo(isActivo);
        return this;
    }

    public void setIsActivo(Boolean isActivo) {
        this.isActivo = isActivo;
    }

    public TipoCargo getCargo() {
        return this.cargo;
    }

    public Persona cargo(TipoCargo cargo) {
        this.setCargo(cargo);
        return this;
    }

    public void setCargo(TipoCargo cargo) {
        this.cargo = cargo;
    }

    public Set<Asignacion> getAsignacions() {
        return this.asignacions;
    }

    public void setAsignacions(Set<Asignacion> asignacions) {
        if (this.asignacions != null) {
            this.asignacions.forEach(i -> i.setCliente(null));
        }
        if (asignacions != null) {
            asignacions.forEach(i -> i.setCliente(this));
        }
        this.asignacions = asignacions;
    }

    public Persona asignacions(Set<Asignacion> asignacions) {
        this.setAsignacions(asignacions);
        return this;
    }

    public Persona addAsignacion(Asignacion asignacion) {
        this.asignacions.add(asignacion);
        asignacion.setCliente(this);
        return this;
    }

    public Persona removeAsignacion(Asignacion asignacion) {
        this.asignacions.remove(asignacion);
        asignacion.setCliente(null);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Persona)) {
            return false;
        }
        return id != null && id.equals(((Persona) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Persona{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", apellido='" + getApellido() + "'" +
            ", img='" + getImg() + "'" +
            ", email='" + getEmail() + "'" +
            ", login='" + getLogin() + "'" +
            ", userId=" + getUserId() +
            ", fechaCreacion='" + getFechaCreacion() + "'" +
            ", isActivo='" + getIsActivo() + "'" +
            ", cargo='" + getCargo() + "'" +
            "}";
    }
}
