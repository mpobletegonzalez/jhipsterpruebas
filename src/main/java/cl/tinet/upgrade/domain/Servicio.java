package cl.tinet.upgrade.domain;

import cl.tinet.upgrade.domain.enumeration.EstadoServicio;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Servicio.
 */
@Entity
@Table(name = "servicio")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Servicio implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "nombre", nullable = false, unique = true)
    private String nombre;

    @NotNull
    @Column(name = "url_propuesta", nullable = false)
    private String urlPropuesta;

    @NotNull
    @Column(name = "is_activo", nullable = false)
    private Boolean isActivo;

    @NotNull
    @Enumerated(EnumType.STRING)
    @Column(name = "estado", nullable = false)
    private EstadoServicio estado;

    @NotNull
    @Column(name = "fecha_creacion", nullable = false)
    private ZonedDateTime fechaCreacion;

    @OneToMany(mappedBy = "servicio")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "hitos", "estadosHistorias", "asignacions", "servicio" }, allowSetters = true)
    private Set<Iniciativa> iniciativas = new HashSet<>();

    @ManyToOne
    @JsonIgnoreProperties(value = { "servicios" }, allowSetters = true)
    private Cliente cliente;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Servicio id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNombre() {
        return this.nombre;
    }

    public Servicio nombre(String nombre) {
        this.setNombre(nombre);
        return this;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getUrlPropuesta() {
        return this.urlPropuesta;
    }

    public Servicio urlPropuesta(String urlPropuesta) {
        this.setUrlPropuesta(urlPropuesta);
        return this;
    }

    public void setUrlPropuesta(String urlPropuesta) {
        this.urlPropuesta = urlPropuesta;
    }

    public Boolean getIsActivo() {
        return this.isActivo;
    }

    public Servicio isActivo(Boolean isActivo) {
        this.setIsActivo(isActivo);
        return this;
    }

    public void setIsActivo(Boolean isActivo) {
        this.isActivo = isActivo;
    }

    public EstadoServicio getEstado() {
        return this.estado;
    }

    public Servicio estado(EstadoServicio estado) {
        this.setEstado(estado);
        return this;
    }

    public void setEstado(EstadoServicio estado) {
        this.estado = estado;
    }

    public ZonedDateTime getFechaCreacion() {
        return this.fechaCreacion;
    }

    public Servicio fechaCreacion(ZonedDateTime fechaCreacion) {
        this.setFechaCreacion(fechaCreacion);
        return this;
    }

    public void setFechaCreacion(ZonedDateTime fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public Set<Iniciativa> getIniciativas() {
        return this.iniciativas;
    }

    public void setIniciativas(Set<Iniciativa> iniciativas) {
        if (this.iniciativas != null) {
            this.iniciativas.forEach(i -> i.setServicio(null));
        }
        if (iniciativas != null) {
            iniciativas.forEach(i -> i.setServicio(this));
        }
        this.iniciativas = iniciativas;
    }

    public Servicio iniciativas(Set<Iniciativa> iniciativas) {
        this.setIniciativas(iniciativas);
        return this;
    }

    public Servicio addIniciativa(Iniciativa iniciativa) {
        this.iniciativas.add(iniciativa);
        iniciativa.setServicio(this);
        return this;
    }

    public Servicio removeIniciativa(Iniciativa iniciativa) {
        this.iniciativas.remove(iniciativa);
        iniciativa.setServicio(null);
        return this;
    }

    public Cliente getCliente() {
        return this.cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public Servicio cliente(Cliente cliente) {
        this.setCliente(cliente);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Servicio)) {
            return false;
        }
        return id != null && id.equals(((Servicio) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Servicio{" +
            "id=" + getId() +
            ", nombre='" + getNombre() + "'" +
            ", urlPropuesta='" + getUrlPropuesta() + "'" +
            ", isActivo='" + getIsActivo() + "'" +
            ", estado='" + getEstado() + "'" +
            ", fechaCreacion='" + getFechaCreacion() + "'" +
            "}";
    }
}
