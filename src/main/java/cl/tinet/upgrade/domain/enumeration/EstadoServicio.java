package cl.tinet.upgrade.domain.enumeration;

/**
 * The EstadoServicio enumeration.
 */
public enum EstadoServicio {
    ACTIVO,
    INACTIVO,
    CONGELADO,
    TERMINADO,
}
