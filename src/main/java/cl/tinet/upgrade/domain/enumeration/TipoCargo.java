package cl.tinet.upgrade.domain.enumeration;

/**
 * The TipoCargo enumeration.
 */
public enum TipoCargo {
    ISW,
    SM,
    ARQ,
    UIUX,
}
