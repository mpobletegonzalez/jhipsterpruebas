package cl.tinet.upgrade.domain.enumeration;

/**
 * The TipoHito enumeration.
 */
public enum TipoHito {
    RLS,
    PRD,
    QA,
    DEMO,
}
