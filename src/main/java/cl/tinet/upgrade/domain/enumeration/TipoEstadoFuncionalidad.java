package cl.tinet.upgrade.domain.enumeration;

/**
 * The TipoEstadoFuncionalidad enumeration.
 */
public enum TipoEstadoFuncionalidad {
    ORIGINAL,
    VIGENTE,
    CANCELADA,
    CONGELADA,
}
