package cl.tinet.upgrade.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import cl.tinet.upgrade.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class EstadosHistoriasDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(EstadosHistoriasDTO.class);
        EstadosHistoriasDTO estadosHistoriasDTO1 = new EstadosHistoriasDTO();
        estadosHistoriasDTO1.setId(1L);
        EstadosHistoriasDTO estadosHistoriasDTO2 = new EstadosHistoriasDTO();
        assertThat(estadosHistoriasDTO1).isNotEqualTo(estadosHistoriasDTO2);
        estadosHistoriasDTO2.setId(estadosHistoriasDTO1.getId());
        assertThat(estadosHistoriasDTO1).isEqualTo(estadosHistoriasDTO2);
        estadosHistoriasDTO2.setId(2L);
        assertThat(estadosHistoriasDTO1).isNotEqualTo(estadosHistoriasDTO2);
        estadosHistoriasDTO1.setId(null);
        assertThat(estadosHistoriasDTO1).isNotEqualTo(estadosHistoriasDTO2);
    }
}
