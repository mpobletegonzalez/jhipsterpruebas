package cl.tinet.upgrade.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import cl.tinet.upgrade.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class AsignacionDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(AsignacionDTO.class);
        AsignacionDTO asignacionDTO1 = new AsignacionDTO();
        asignacionDTO1.setId(1L);
        AsignacionDTO asignacionDTO2 = new AsignacionDTO();
        assertThat(asignacionDTO1).isNotEqualTo(asignacionDTO2);
        asignacionDTO2.setId(asignacionDTO1.getId());
        assertThat(asignacionDTO1).isEqualTo(asignacionDTO2);
        asignacionDTO2.setId(2L);
        assertThat(asignacionDTO1).isNotEqualTo(asignacionDTO2);
        asignacionDTO1.setId(null);
        assertThat(asignacionDTO1).isNotEqualTo(asignacionDTO2);
    }
}
