package cl.tinet.upgrade.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import cl.tinet.upgrade.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class HitoDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(HitoDTO.class);
        HitoDTO hitoDTO1 = new HitoDTO();
        hitoDTO1.setId(1L);
        HitoDTO hitoDTO2 = new HitoDTO();
        assertThat(hitoDTO1).isNotEqualTo(hitoDTO2);
        hitoDTO2.setId(hitoDTO1.getId());
        assertThat(hitoDTO1).isEqualTo(hitoDTO2);
        hitoDTO2.setId(2L);
        assertThat(hitoDTO1).isNotEqualTo(hitoDTO2);
        hitoDTO1.setId(null);
        assertThat(hitoDTO1).isNotEqualTo(hitoDTO2);
    }
}
