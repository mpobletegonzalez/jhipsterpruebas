package cl.tinet.upgrade.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import cl.tinet.upgrade.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class RegistroHistoriaDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegistroHistoriaDTO.class);
        RegistroHistoriaDTO registroHistoriaDTO1 = new RegistroHistoriaDTO();
        registroHistoriaDTO1.setId(1L);
        RegistroHistoriaDTO registroHistoriaDTO2 = new RegistroHistoriaDTO();
        assertThat(registroHistoriaDTO1).isNotEqualTo(registroHistoriaDTO2);
        registroHistoriaDTO2.setId(registroHistoriaDTO1.getId());
        assertThat(registroHistoriaDTO1).isEqualTo(registroHistoriaDTO2);
        registroHistoriaDTO2.setId(2L);
        assertThat(registroHistoriaDTO1).isNotEqualTo(registroHistoriaDTO2);
        registroHistoriaDTO1.setId(null);
        assertThat(registroHistoriaDTO1).isNotEqualTo(registroHistoriaDTO2);
    }
}
