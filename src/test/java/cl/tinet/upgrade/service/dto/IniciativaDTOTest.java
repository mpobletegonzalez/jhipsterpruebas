package cl.tinet.upgrade.service.dto;

import static org.assertj.core.api.Assertions.assertThat;

import cl.tinet.upgrade.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class IniciativaDTOTest {

    @Test
    void dtoEqualsVerifier() throws Exception {
        TestUtil.equalsVerifier(IniciativaDTO.class);
        IniciativaDTO iniciativaDTO1 = new IniciativaDTO();
        iniciativaDTO1.setId(1L);
        IniciativaDTO iniciativaDTO2 = new IniciativaDTO();
        assertThat(iniciativaDTO1).isNotEqualTo(iniciativaDTO2);
        iniciativaDTO2.setId(iniciativaDTO1.getId());
        assertThat(iniciativaDTO1).isEqualTo(iniciativaDTO2);
        iniciativaDTO2.setId(2L);
        assertThat(iniciativaDTO1).isNotEqualTo(iniciativaDTO2);
        iniciativaDTO1.setId(null);
        assertThat(iniciativaDTO1).isNotEqualTo(iniciativaDTO2);
    }
}
