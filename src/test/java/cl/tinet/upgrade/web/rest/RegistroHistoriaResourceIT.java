package cl.tinet.upgrade.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import cl.tinet.upgrade.IntegrationTest;
import cl.tinet.upgrade.domain.RegistroHistoria;
import cl.tinet.upgrade.repository.RegistroHistoriaRepository;
import cl.tinet.upgrade.service.dto.RegistroHistoriaDTO;
import cl.tinet.upgrade.service.mapper.RegistroHistoriaMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link RegistroHistoriaResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class RegistroHistoriaResourceIT {

    private static final String DEFAULT_NOMBRE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE = "BBBBBBBBBB";

    private static final Integer DEFAULT_CODIGO = 1;
    private static final Integer UPDATED_CODIGO = 2;

    private static final String ENTITY_API_URL = "/api/registro-historias";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private RegistroHistoriaRepository registroHistoriaRepository;

    @Autowired
    private RegistroHistoriaMapper registroHistoriaMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restRegistroHistoriaMockMvc;

    private RegistroHistoria registroHistoria;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegistroHistoria createEntity(EntityManager em) {
        RegistroHistoria registroHistoria = new RegistroHistoria().nombre(DEFAULT_NOMBRE).codigo(DEFAULT_CODIGO);
        return registroHistoria;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static RegistroHistoria createUpdatedEntity(EntityManager em) {
        RegistroHistoria registroHistoria = new RegistroHistoria().nombre(UPDATED_NOMBRE).codigo(UPDATED_CODIGO);
        return registroHistoria;
    }

    @BeforeEach
    public void initTest() {
        registroHistoria = createEntity(em);
    }

    @Test
    @Transactional
    void createRegistroHistoria() throws Exception {
        int databaseSizeBeforeCreate = registroHistoriaRepository.findAll().size();
        // Create the RegistroHistoria
        RegistroHistoriaDTO registroHistoriaDTO = registroHistoriaMapper.toDto(registroHistoria);
        restRegistroHistoriaMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(registroHistoriaDTO))
            )
            .andExpect(status().isCreated());

        // Validate the RegistroHistoria in the database
        List<RegistroHistoria> registroHistoriaList = registroHistoriaRepository.findAll();
        assertThat(registroHistoriaList).hasSize(databaseSizeBeforeCreate + 1);
        RegistroHistoria testRegistroHistoria = registroHistoriaList.get(registroHistoriaList.size() - 1);
        assertThat(testRegistroHistoria.getNombre()).isEqualTo(DEFAULT_NOMBRE);
        assertThat(testRegistroHistoria.getCodigo()).isEqualTo(DEFAULT_CODIGO);
    }

    @Test
    @Transactional
    void createRegistroHistoriaWithExistingId() throws Exception {
        // Create the RegistroHistoria with an existing ID
        registroHistoria.setId(1L);
        RegistroHistoriaDTO registroHistoriaDTO = registroHistoriaMapper.toDto(registroHistoria);

        int databaseSizeBeforeCreate = registroHistoriaRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restRegistroHistoriaMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(registroHistoriaDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RegistroHistoria in the database
        List<RegistroHistoria> registroHistoriaList = registroHistoriaRepository.findAll();
        assertThat(registroHistoriaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllRegistroHistorias() throws Exception {
        // Initialize the database
        registroHistoriaRepository.saveAndFlush(registroHistoria);

        // Get all the registroHistoriaList
        restRegistroHistoriaMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(registroHistoria.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE)))
            .andExpect(jsonPath("$.[*].codigo").value(hasItem(DEFAULT_CODIGO)));
    }

    @Test
    @Transactional
    void getRegistroHistoria() throws Exception {
        // Initialize the database
        registroHistoriaRepository.saveAndFlush(registroHistoria);

        // Get the registroHistoria
        restRegistroHistoriaMockMvc
            .perform(get(ENTITY_API_URL_ID, registroHistoria.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(registroHistoria.getId().intValue()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE))
            .andExpect(jsonPath("$.codigo").value(DEFAULT_CODIGO));
    }

    @Test
    @Transactional
    void getNonExistingRegistroHistoria() throws Exception {
        // Get the registroHistoria
        restRegistroHistoriaMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewRegistroHistoria() throws Exception {
        // Initialize the database
        registroHistoriaRepository.saveAndFlush(registroHistoria);

        int databaseSizeBeforeUpdate = registroHistoriaRepository.findAll().size();

        // Update the registroHistoria
        RegistroHistoria updatedRegistroHistoria = registroHistoriaRepository.findById(registroHistoria.getId()).get();
        // Disconnect from session so that the updates on updatedRegistroHistoria are not directly saved in db
        em.detach(updatedRegistroHistoria);
        updatedRegistroHistoria.nombre(UPDATED_NOMBRE).codigo(UPDATED_CODIGO);
        RegistroHistoriaDTO registroHistoriaDTO = registroHistoriaMapper.toDto(updatedRegistroHistoria);

        restRegistroHistoriaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, registroHistoriaDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(registroHistoriaDTO))
            )
            .andExpect(status().isOk());

        // Validate the RegistroHistoria in the database
        List<RegistroHistoria> registroHistoriaList = registroHistoriaRepository.findAll();
        assertThat(registroHistoriaList).hasSize(databaseSizeBeforeUpdate);
        RegistroHistoria testRegistroHistoria = registroHistoriaList.get(registroHistoriaList.size() - 1);
        assertThat(testRegistroHistoria.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testRegistroHistoria.getCodigo()).isEqualTo(UPDATED_CODIGO);
    }

    @Test
    @Transactional
    void putNonExistingRegistroHistoria() throws Exception {
        int databaseSizeBeforeUpdate = registroHistoriaRepository.findAll().size();
        registroHistoria.setId(count.incrementAndGet());

        // Create the RegistroHistoria
        RegistroHistoriaDTO registroHistoriaDTO = registroHistoriaMapper.toDto(registroHistoria);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegistroHistoriaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, registroHistoriaDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(registroHistoriaDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RegistroHistoria in the database
        List<RegistroHistoria> registroHistoriaList = registroHistoriaRepository.findAll();
        assertThat(registroHistoriaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchRegistroHistoria() throws Exception {
        int databaseSizeBeforeUpdate = registroHistoriaRepository.findAll().size();
        registroHistoria.setId(count.incrementAndGet());

        // Create the RegistroHistoria
        RegistroHistoriaDTO registroHistoriaDTO = registroHistoriaMapper.toDto(registroHistoria);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRegistroHistoriaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(registroHistoriaDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RegistroHistoria in the database
        List<RegistroHistoria> registroHistoriaList = registroHistoriaRepository.findAll();
        assertThat(registroHistoriaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamRegistroHistoria() throws Exception {
        int databaseSizeBeforeUpdate = registroHistoriaRepository.findAll().size();
        registroHistoria.setId(count.incrementAndGet());

        // Create the RegistroHistoria
        RegistroHistoriaDTO registroHistoriaDTO = registroHistoriaMapper.toDto(registroHistoria);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRegistroHistoriaMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(registroHistoriaDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the RegistroHistoria in the database
        List<RegistroHistoria> registroHistoriaList = registroHistoriaRepository.findAll();
        assertThat(registroHistoriaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateRegistroHistoriaWithPatch() throws Exception {
        // Initialize the database
        registroHistoriaRepository.saveAndFlush(registroHistoria);

        int databaseSizeBeforeUpdate = registroHistoriaRepository.findAll().size();

        // Update the registroHistoria using partial update
        RegistroHistoria partialUpdatedRegistroHistoria = new RegistroHistoria();
        partialUpdatedRegistroHistoria.setId(registroHistoria.getId());

        partialUpdatedRegistroHistoria.codigo(UPDATED_CODIGO);

        restRegistroHistoriaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRegistroHistoria.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRegistroHistoria))
            )
            .andExpect(status().isOk());

        // Validate the RegistroHistoria in the database
        List<RegistroHistoria> registroHistoriaList = registroHistoriaRepository.findAll();
        assertThat(registroHistoriaList).hasSize(databaseSizeBeforeUpdate);
        RegistroHistoria testRegistroHistoria = registroHistoriaList.get(registroHistoriaList.size() - 1);
        assertThat(testRegistroHistoria.getNombre()).isEqualTo(DEFAULT_NOMBRE);
        assertThat(testRegistroHistoria.getCodigo()).isEqualTo(UPDATED_CODIGO);
    }

    @Test
    @Transactional
    void fullUpdateRegistroHistoriaWithPatch() throws Exception {
        // Initialize the database
        registroHistoriaRepository.saveAndFlush(registroHistoria);

        int databaseSizeBeforeUpdate = registroHistoriaRepository.findAll().size();

        // Update the registroHistoria using partial update
        RegistroHistoria partialUpdatedRegistroHistoria = new RegistroHistoria();
        partialUpdatedRegistroHistoria.setId(registroHistoria.getId());

        partialUpdatedRegistroHistoria.nombre(UPDATED_NOMBRE).codigo(UPDATED_CODIGO);

        restRegistroHistoriaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRegistroHistoria.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRegistroHistoria))
            )
            .andExpect(status().isOk());

        // Validate the RegistroHistoria in the database
        List<RegistroHistoria> registroHistoriaList = registroHistoriaRepository.findAll();
        assertThat(registroHistoriaList).hasSize(databaseSizeBeforeUpdate);
        RegistroHistoria testRegistroHistoria = registroHistoriaList.get(registroHistoriaList.size() - 1);
        assertThat(testRegistroHistoria.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testRegistroHistoria.getCodigo()).isEqualTo(UPDATED_CODIGO);
    }

    @Test
    @Transactional
    void patchNonExistingRegistroHistoria() throws Exception {
        int databaseSizeBeforeUpdate = registroHistoriaRepository.findAll().size();
        registroHistoria.setId(count.incrementAndGet());

        // Create the RegistroHistoria
        RegistroHistoriaDTO registroHistoriaDTO = registroHistoriaMapper.toDto(registroHistoria);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRegistroHistoriaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, registroHistoriaDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(registroHistoriaDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RegistroHistoria in the database
        List<RegistroHistoria> registroHistoriaList = registroHistoriaRepository.findAll();
        assertThat(registroHistoriaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchRegistroHistoria() throws Exception {
        int databaseSizeBeforeUpdate = registroHistoriaRepository.findAll().size();
        registroHistoria.setId(count.incrementAndGet());

        // Create the RegistroHistoria
        RegistroHistoriaDTO registroHistoriaDTO = registroHistoriaMapper.toDto(registroHistoria);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRegistroHistoriaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(registroHistoriaDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the RegistroHistoria in the database
        List<RegistroHistoria> registroHistoriaList = registroHistoriaRepository.findAll();
        assertThat(registroHistoriaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamRegistroHistoria() throws Exception {
        int databaseSizeBeforeUpdate = registroHistoriaRepository.findAll().size();
        registroHistoria.setId(count.incrementAndGet());

        // Create the RegistroHistoria
        RegistroHistoriaDTO registroHistoriaDTO = registroHistoriaMapper.toDto(registroHistoria);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRegistroHistoriaMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(registroHistoriaDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the RegistroHistoria in the database
        List<RegistroHistoria> registroHistoriaList = registroHistoriaRepository.findAll();
        assertThat(registroHistoriaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteRegistroHistoria() throws Exception {
        // Initialize the database
        registroHistoriaRepository.saveAndFlush(registroHistoria);

        int databaseSizeBeforeDelete = registroHistoriaRepository.findAll().size();

        // Delete the registroHistoria
        restRegistroHistoriaMockMvc
            .perform(delete(ENTITY_API_URL_ID, registroHistoria.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<RegistroHistoria> registroHistoriaList = registroHistoriaRepository.findAll();
        assertThat(registroHistoriaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
