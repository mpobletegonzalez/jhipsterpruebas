package cl.tinet.upgrade.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import cl.tinet.upgrade.IntegrationTest;
import cl.tinet.upgrade.domain.EstadosHistorias;
import cl.tinet.upgrade.repository.EstadosHistoriasRepository;
import cl.tinet.upgrade.service.dto.EstadosHistoriasDTO;
import cl.tinet.upgrade.service.mapper.EstadosHistoriasMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link EstadosHistoriasResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class EstadosHistoriasResourceIT {

    private static final Integer DEFAULT_TOTAL_ESTIMACION = 1;
    private static final Integer UPDATED_TOTAL_ESTIMACION = 2;

    private static final Integer DEFAULT_TOTAL_BACKLOG = 1;
    private static final Integer UPDATED_TOTAL_BACKLOG = 2;

    private static final Integer DEFAULT_TOTAL_EN_REFINAMIENTO = 1;
    private static final Integer UPDATED_TOTAL_EN_REFINAMIENTO = 2;

    private static final Integer DEFAULT_TOTAL_LISTO_PARA_DESARROLLO = 1;
    private static final Integer UPDATED_TOTAL_LISTO_PARA_DESARROLLO = 2;

    private static final Integer DEFAULT_TOTAL_EN_DESARROLLO = 1;
    private static final Integer UPDATED_TOTAL_EN_DESARROLLO = 2;

    private static final Integer DEFAULT_TOTAL_LISTO_CERTIFICACION = 1;
    private static final Integer UPDATED_TOTAL_LISTO_CERTIFICACION = 2;

    private static final Integer DEFAULT_TOTAL_EN_CERTIFICACION = 1;
    private static final Integer UPDATED_TOTAL_EN_CERTIFICACION = 2;

    private static final Integer DEFAULT_TOTAL_PRUEBAS_UAT = 1;
    private static final Integer UPDATED_TOTAL_PRUEBAS_UAT = 2;

    private static final Integer DEFAULT_TOTAL_LISTO_PRODUCCION = 1;
    private static final Integer UPDATED_TOTAL_LISTO_PRODUCCION = 2;

    private static final Integer DEFAULT_TOTAL_EN_PRODUCCION = 1;
    private static final Integer UPDATED_TOTAL_EN_PRODUCCION = 2;

    private static final Integer DEFAULT_TOTAL_APROBADOS_PO = 1;
    private static final Integer UPDATED_TOTAL_APROBADOS_PO = 2;

    private static final String ENTITY_API_URL = "/api/estados-historias";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private EstadosHistoriasRepository estadosHistoriasRepository;

    @Autowired
    private EstadosHistoriasMapper estadosHistoriasMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restEstadosHistoriasMockMvc;

    private EstadosHistorias estadosHistorias;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EstadosHistorias createEntity(EntityManager em) {
        EstadosHistorias estadosHistorias = new EstadosHistorias()
            .totalEstimacion(DEFAULT_TOTAL_ESTIMACION)
            .totalBacklog(DEFAULT_TOTAL_BACKLOG)
            .totalEnRefinamiento(DEFAULT_TOTAL_EN_REFINAMIENTO)
            .totalListoParaDesarrollo(DEFAULT_TOTAL_LISTO_PARA_DESARROLLO)
            .totalEnDesarrollo(DEFAULT_TOTAL_EN_DESARROLLO)
            .totalListoCertificacion(DEFAULT_TOTAL_LISTO_CERTIFICACION)
            .totalEnCertificacion(DEFAULT_TOTAL_EN_CERTIFICACION)
            .totalPruebasUAT(DEFAULT_TOTAL_PRUEBAS_UAT)
            .totalListoProduccion(DEFAULT_TOTAL_LISTO_PRODUCCION)
            .totalEnProduccion(DEFAULT_TOTAL_EN_PRODUCCION)
            .totalAprobadosPO(DEFAULT_TOTAL_APROBADOS_PO);
        return estadosHistorias;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static EstadosHistorias createUpdatedEntity(EntityManager em) {
        EstadosHistorias estadosHistorias = new EstadosHistorias()
            .totalEstimacion(UPDATED_TOTAL_ESTIMACION)
            .totalBacklog(UPDATED_TOTAL_BACKLOG)
            .totalEnRefinamiento(UPDATED_TOTAL_EN_REFINAMIENTO)
            .totalListoParaDesarrollo(UPDATED_TOTAL_LISTO_PARA_DESARROLLO)
            .totalEnDesarrollo(UPDATED_TOTAL_EN_DESARROLLO)
            .totalListoCertificacion(UPDATED_TOTAL_LISTO_CERTIFICACION)
            .totalEnCertificacion(UPDATED_TOTAL_EN_CERTIFICACION)
            .totalPruebasUAT(UPDATED_TOTAL_PRUEBAS_UAT)
            .totalListoProduccion(UPDATED_TOTAL_LISTO_PRODUCCION)
            .totalEnProduccion(UPDATED_TOTAL_EN_PRODUCCION)
            .totalAprobadosPO(UPDATED_TOTAL_APROBADOS_PO);
        return estadosHistorias;
    }

    @BeforeEach
    public void initTest() {
        estadosHistorias = createEntity(em);
    }

    @Test
    @Transactional
    void createEstadosHistorias() throws Exception {
        int databaseSizeBeforeCreate = estadosHistoriasRepository.findAll().size();
        // Create the EstadosHistorias
        EstadosHistoriasDTO estadosHistoriasDTO = estadosHistoriasMapper.toDto(estadosHistorias);
        restEstadosHistoriasMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(estadosHistoriasDTO))
            )
            .andExpect(status().isCreated());

        // Validate the EstadosHistorias in the database
        List<EstadosHistorias> estadosHistoriasList = estadosHistoriasRepository.findAll();
        assertThat(estadosHistoriasList).hasSize(databaseSizeBeforeCreate + 1);
        EstadosHistorias testEstadosHistorias = estadosHistoriasList.get(estadosHistoriasList.size() - 1);
        assertThat(testEstadosHistorias.getTotalEstimacion()).isEqualTo(DEFAULT_TOTAL_ESTIMACION);
        assertThat(testEstadosHistorias.getTotalBacklog()).isEqualTo(DEFAULT_TOTAL_BACKLOG);
        assertThat(testEstadosHistorias.getTotalEnRefinamiento()).isEqualTo(DEFAULT_TOTAL_EN_REFINAMIENTO);
        assertThat(testEstadosHistorias.getTotalListoParaDesarrollo()).isEqualTo(DEFAULT_TOTAL_LISTO_PARA_DESARROLLO);
        assertThat(testEstadosHistorias.getTotalEnDesarrollo()).isEqualTo(DEFAULT_TOTAL_EN_DESARROLLO);
        assertThat(testEstadosHistorias.getTotalListoCertificacion()).isEqualTo(DEFAULT_TOTAL_LISTO_CERTIFICACION);
        assertThat(testEstadosHistorias.getTotalEnCertificacion()).isEqualTo(DEFAULT_TOTAL_EN_CERTIFICACION);
        assertThat(testEstadosHistorias.getTotalPruebasUAT()).isEqualTo(DEFAULT_TOTAL_PRUEBAS_UAT);
        assertThat(testEstadosHistorias.getTotalListoProduccion()).isEqualTo(DEFAULT_TOTAL_LISTO_PRODUCCION);
        assertThat(testEstadosHistorias.getTotalEnProduccion()).isEqualTo(DEFAULT_TOTAL_EN_PRODUCCION);
        assertThat(testEstadosHistorias.getTotalAprobadosPO()).isEqualTo(DEFAULT_TOTAL_APROBADOS_PO);
    }

    @Test
    @Transactional
    void createEstadosHistoriasWithExistingId() throws Exception {
        // Create the EstadosHistorias with an existing ID
        estadosHistorias.setId(1L);
        EstadosHistoriasDTO estadosHistoriasDTO = estadosHistoriasMapper.toDto(estadosHistorias);

        int databaseSizeBeforeCreate = estadosHistoriasRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restEstadosHistoriasMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(estadosHistoriasDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the EstadosHistorias in the database
        List<EstadosHistorias> estadosHistoriasList = estadosHistoriasRepository.findAll();
        assertThat(estadosHistoriasList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllEstadosHistorias() throws Exception {
        // Initialize the database
        estadosHistoriasRepository.saveAndFlush(estadosHistorias);

        // Get all the estadosHistoriasList
        restEstadosHistoriasMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(estadosHistorias.getId().intValue())))
            .andExpect(jsonPath("$.[*].totalEstimacion").value(hasItem(DEFAULT_TOTAL_ESTIMACION)))
            .andExpect(jsonPath("$.[*].totalBacklog").value(hasItem(DEFAULT_TOTAL_BACKLOG)))
            .andExpect(jsonPath("$.[*].totalEnRefinamiento").value(hasItem(DEFAULT_TOTAL_EN_REFINAMIENTO)))
            .andExpect(jsonPath("$.[*].totalListoParaDesarrollo").value(hasItem(DEFAULT_TOTAL_LISTO_PARA_DESARROLLO)))
            .andExpect(jsonPath("$.[*].totalEnDesarrollo").value(hasItem(DEFAULT_TOTAL_EN_DESARROLLO)))
            .andExpect(jsonPath("$.[*].totalListoCertificacion").value(hasItem(DEFAULT_TOTAL_LISTO_CERTIFICACION)))
            .andExpect(jsonPath("$.[*].totalEnCertificacion").value(hasItem(DEFAULT_TOTAL_EN_CERTIFICACION)))
            .andExpect(jsonPath("$.[*].totalPruebasUAT").value(hasItem(DEFAULT_TOTAL_PRUEBAS_UAT)))
            .andExpect(jsonPath("$.[*].totalListoProduccion").value(hasItem(DEFAULT_TOTAL_LISTO_PRODUCCION)))
            .andExpect(jsonPath("$.[*].totalEnProduccion").value(hasItem(DEFAULT_TOTAL_EN_PRODUCCION)))
            .andExpect(jsonPath("$.[*].totalAprobadosPO").value(hasItem(DEFAULT_TOTAL_APROBADOS_PO)));
    }

    @Test
    @Transactional
    void getEstadosHistorias() throws Exception {
        // Initialize the database
        estadosHistoriasRepository.saveAndFlush(estadosHistorias);

        // Get the estadosHistorias
        restEstadosHistoriasMockMvc
            .perform(get(ENTITY_API_URL_ID, estadosHistorias.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(estadosHistorias.getId().intValue()))
            .andExpect(jsonPath("$.totalEstimacion").value(DEFAULT_TOTAL_ESTIMACION))
            .andExpect(jsonPath("$.totalBacklog").value(DEFAULT_TOTAL_BACKLOG))
            .andExpect(jsonPath("$.totalEnRefinamiento").value(DEFAULT_TOTAL_EN_REFINAMIENTO))
            .andExpect(jsonPath("$.totalListoParaDesarrollo").value(DEFAULT_TOTAL_LISTO_PARA_DESARROLLO))
            .andExpect(jsonPath("$.totalEnDesarrollo").value(DEFAULT_TOTAL_EN_DESARROLLO))
            .andExpect(jsonPath("$.totalListoCertificacion").value(DEFAULT_TOTAL_LISTO_CERTIFICACION))
            .andExpect(jsonPath("$.totalEnCertificacion").value(DEFAULT_TOTAL_EN_CERTIFICACION))
            .andExpect(jsonPath("$.totalPruebasUAT").value(DEFAULT_TOTAL_PRUEBAS_UAT))
            .andExpect(jsonPath("$.totalListoProduccion").value(DEFAULT_TOTAL_LISTO_PRODUCCION))
            .andExpect(jsonPath("$.totalEnProduccion").value(DEFAULT_TOTAL_EN_PRODUCCION))
            .andExpect(jsonPath("$.totalAprobadosPO").value(DEFAULT_TOTAL_APROBADOS_PO));
    }

    @Test
    @Transactional
    void getNonExistingEstadosHistorias() throws Exception {
        // Get the estadosHistorias
        restEstadosHistoriasMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewEstadosHistorias() throws Exception {
        // Initialize the database
        estadosHistoriasRepository.saveAndFlush(estadosHistorias);

        int databaseSizeBeforeUpdate = estadosHistoriasRepository.findAll().size();

        // Update the estadosHistorias
        EstadosHistorias updatedEstadosHistorias = estadosHistoriasRepository.findById(estadosHistorias.getId()).get();
        // Disconnect from session so that the updates on updatedEstadosHistorias are not directly saved in db
        em.detach(updatedEstadosHistorias);
        updatedEstadosHistorias
            .totalEstimacion(UPDATED_TOTAL_ESTIMACION)
            .totalBacklog(UPDATED_TOTAL_BACKLOG)
            .totalEnRefinamiento(UPDATED_TOTAL_EN_REFINAMIENTO)
            .totalListoParaDesarrollo(UPDATED_TOTAL_LISTO_PARA_DESARROLLO)
            .totalEnDesarrollo(UPDATED_TOTAL_EN_DESARROLLO)
            .totalListoCertificacion(UPDATED_TOTAL_LISTO_CERTIFICACION)
            .totalEnCertificacion(UPDATED_TOTAL_EN_CERTIFICACION)
            .totalPruebasUAT(UPDATED_TOTAL_PRUEBAS_UAT)
            .totalListoProduccion(UPDATED_TOTAL_LISTO_PRODUCCION)
            .totalEnProduccion(UPDATED_TOTAL_EN_PRODUCCION)
            .totalAprobadosPO(UPDATED_TOTAL_APROBADOS_PO);
        EstadosHistoriasDTO estadosHistoriasDTO = estadosHistoriasMapper.toDto(updatedEstadosHistorias);

        restEstadosHistoriasMockMvc
            .perform(
                put(ENTITY_API_URL_ID, estadosHistoriasDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(estadosHistoriasDTO))
            )
            .andExpect(status().isOk());

        // Validate the EstadosHistorias in the database
        List<EstadosHistorias> estadosHistoriasList = estadosHistoriasRepository.findAll();
        assertThat(estadosHistoriasList).hasSize(databaseSizeBeforeUpdate);
        EstadosHistorias testEstadosHistorias = estadosHistoriasList.get(estadosHistoriasList.size() - 1);
        assertThat(testEstadosHistorias.getTotalEstimacion()).isEqualTo(UPDATED_TOTAL_ESTIMACION);
        assertThat(testEstadosHistorias.getTotalBacklog()).isEqualTo(UPDATED_TOTAL_BACKLOG);
        assertThat(testEstadosHistorias.getTotalEnRefinamiento()).isEqualTo(UPDATED_TOTAL_EN_REFINAMIENTO);
        assertThat(testEstadosHistorias.getTotalListoParaDesarrollo()).isEqualTo(UPDATED_TOTAL_LISTO_PARA_DESARROLLO);
        assertThat(testEstadosHistorias.getTotalEnDesarrollo()).isEqualTo(UPDATED_TOTAL_EN_DESARROLLO);
        assertThat(testEstadosHistorias.getTotalListoCertificacion()).isEqualTo(UPDATED_TOTAL_LISTO_CERTIFICACION);
        assertThat(testEstadosHistorias.getTotalEnCertificacion()).isEqualTo(UPDATED_TOTAL_EN_CERTIFICACION);
        assertThat(testEstadosHistorias.getTotalPruebasUAT()).isEqualTo(UPDATED_TOTAL_PRUEBAS_UAT);
        assertThat(testEstadosHistorias.getTotalListoProduccion()).isEqualTo(UPDATED_TOTAL_LISTO_PRODUCCION);
        assertThat(testEstadosHistorias.getTotalEnProduccion()).isEqualTo(UPDATED_TOTAL_EN_PRODUCCION);
        assertThat(testEstadosHistorias.getTotalAprobadosPO()).isEqualTo(UPDATED_TOTAL_APROBADOS_PO);
    }

    @Test
    @Transactional
    void putNonExistingEstadosHistorias() throws Exception {
        int databaseSizeBeforeUpdate = estadosHistoriasRepository.findAll().size();
        estadosHistorias.setId(count.incrementAndGet());

        // Create the EstadosHistorias
        EstadosHistoriasDTO estadosHistoriasDTO = estadosHistoriasMapper.toDto(estadosHistorias);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEstadosHistoriasMockMvc
            .perform(
                put(ENTITY_API_URL_ID, estadosHistoriasDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(estadosHistoriasDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the EstadosHistorias in the database
        List<EstadosHistorias> estadosHistoriasList = estadosHistoriasRepository.findAll();
        assertThat(estadosHistoriasList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchEstadosHistorias() throws Exception {
        int databaseSizeBeforeUpdate = estadosHistoriasRepository.findAll().size();
        estadosHistorias.setId(count.incrementAndGet());

        // Create the EstadosHistorias
        EstadosHistoriasDTO estadosHistoriasDTO = estadosHistoriasMapper.toDto(estadosHistorias);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEstadosHistoriasMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(estadosHistoriasDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the EstadosHistorias in the database
        List<EstadosHistorias> estadosHistoriasList = estadosHistoriasRepository.findAll();
        assertThat(estadosHistoriasList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamEstadosHistorias() throws Exception {
        int databaseSizeBeforeUpdate = estadosHistoriasRepository.findAll().size();
        estadosHistorias.setId(count.incrementAndGet());

        // Create the EstadosHistorias
        EstadosHistoriasDTO estadosHistoriasDTO = estadosHistoriasMapper.toDto(estadosHistorias);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEstadosHistoriasMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(estadosHistoriasDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the EstadosHistorias in the database
        List<EstadosHistorias> estadosHistoriasList = estadosHistoriasRepository.findAll();
        assertThat(estadosHistoriasList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateEstadosHistoriasWithPatch() throws Exception {
        // Initialize the database
        estadosHistoriasRepository.saveAndFlush(estadosHistorias);

        int databaseSizeBeforeUpdate = estadosHistoriasRepository.findAll().size();

        // Update the estadosHistorias using partial update
        EstadosHistorias partialUpdatedEstadosHistorias = new EstadosHistorias();
        partialUpdatedEstadosHistorias.setId(estadosHistorias.getId());

        partialUpdatedEstadosHistorias
            .totalListoParaDesarrollo(UPDATED_TOTAL_LISTO_PARA_DESARROLLO)
            .totalEnCertificacion(UPDATED_TOTAL_EN_CERTIFICACION)
            .totalPruebasUAT(UPDATED_TOTAL_PRUEBAS_UAT)
            .totalAprobadosPO(UPDATED_TOTAL_APROBADOS_PO);

        restEstadosHistoriasMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEstadosHistorias.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEstadosHistorias))
            )
            .andExpect(status().isOk());

        // Validate the EstadosHistorias in the database
        List<EstadosHistorias> estadosHistoriasList = estadosHistoriasRepository.findAll();
        assertThat(estadosHistoriasList).hasSize(databaseSizeBeforeUpdate);
        EstadosHistorias testEstadosHistorias = estadosHistoriasList.get(estadosHistoriasList.size() - 1);
        assertThat(testEstadosHistorias.getTotalEstimacion()).isEqualTo(DEFAULT_TOTAL_ESTIMACION);
        assertThat(testEstadosHistorias.getTotalBacklog()).isEqualTo(DEFAULT_TOTAL_BACKLOG);
        assertThat(testEstadosHistorias.getTotalEnRefinamiento()).isEqualTo(DEFAULT_TOTAL_EN_REFINAMIENTO);
        assertThat(testEstadosHistorias.getTotalListoParaDesarrollo()).isEqualTo(UPDATED_TOTAL_LISTO_PARA_DESARROLLO);
        assertThat(testEstadosHistorias.getTotalEnDesarrollo()).isEqualTo(DEFAULT_TOTAL_EN_DESARROLLO);
        assertThat(testEstadosHistorias.getTotalListoCertificacion()).isEqualTo(DEFAULT_TOTAL_LISTO_CERTIFICACION);
        assertThat(testEstadosHistorias.getTotalEnCertificacion()).isEqualTo(UPDATED_TOTAL_EN_CERTIFICACION);
        assertThat(testEstadosHistorias.getTotalPruebasUAT()).isEqualTo(UPDATED_TOTAL_PRUEBAS_UAT);
        assertThat(testEstadosHistorias.getTotalListoProduccion()).isEqualTo(DEFAULT_TOTAL_LISTO_PRODUCCION);
        assertThat(testEstadosHistorias.getTotalEnProduccion()).isEqualTo(DEFAULT_TOTAL_EN_PRODUCCION);
        assertThat(testEstadosHistorias.getTotalAprobadosPO()).isEqualTo(UPDATED_TOTAL_APROBADOS_PO);
    }

    @Test
    @Transactional
    void fullUpdateEstadosHistoriasWithPatch() throws Exception {
        // Initialize the database
        estadosHistoriasRepository.saveAndFlush(estadosHistorias);

        int databaseSizeBeforeUpdate = estadosHistoriasRepository.findAll().size();

        // Update the estadosHistorias using partial update
        EstadosHistorias partialUpdatedEstadosHistorias = new EstadosHistorias();
        partialUpdatedEstadosHistorias.setId(estadosHistorias.getId());

        partialUpdatedEstadosHistorias
            .totalEstimacion(UPDATED_TOTAL_ESTIMACION)
            .totalBacklog(UPDATED_TOTAL_BACKLOG)
            .totalEnRefinamiento(UPDATED_TOTAL_EN_REFINAMIENTO)
            .totalListoParaDesarrollo(UPDATED_TOTAL_LISTO_PARA_DESARROLLO)
            .totalEnDesarrollo(UPDATED_TOTAL_EN_DESARROLLO)
            .totalListoCertificacion(UPDATED_TOTAL_LISTO_CERTIFICACION)
            .totalEnCertificacion(UPDATED_TOTAL_EN_CERTIFICACION)
            .totalPruebasUAT(UPDATED_TOTAL_PRUEBAS_UAT)
            .totalListoProduccion(UPDATED_TOTAL_LISTO_PRODUCCION)
            .totalEnProduccion(UPDATED_TOTAL_EN_PRODUCCION)
            .totalAprobadosPO(UPDATED_TOTAL_APROBADOS_PO);

        restEstadosHistoriasMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedEstadosHistorias.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedEstadosHistorias))
            )
            .andExpect(status().isOk());

        // Validate the EstadosHistorias in the database
        List<EstadosHistorias> estadosHistoriasList = estadosHistoriasRepository.findAll();
        assertThat(estadosHistoriasList).hasSize(databaseSizeBeforeUpdate);
        EstadosHistorias testEstadosHistorias = estadosHistoriasList.get(estadosHistoriasList.size() - 1);
        assertThat(testEstadosHistorias.getTotalEstimacion()).isEqualTo(UPDATED_TOTAL_ESTIMACION);
        assertThat(testEstadosHistorias.getTotalBacklog()).isEqualTo(UPDATED_TOTAL_BACKLOG);
        assertThat(testEstadosHistorias.getTotalEnRefinamiento()).isEqualTo(UPDATED_TOTAL_EN_REFINAMIENTO);
        assertThat(testEstadosHistorias.getTotalListoParaDesarrollo()).isEqualTo(UPDATED_TOTAL_LISTO_PARA_DESARROLLO);
        assertThat(testEstadosHistorias.getTotalEnDesarrollo()).isEqualTo(UPDATED_TOTAL_EN_DESARROLLO);
        assertThat(testEstadosHistorias.getTotalListoCertificacion()).isEqualTo(UPDATED_TOTAL_LISTO_CERTIFICACION);
        assertThat(testEstadosHistorias.getTotalEnCertificacion()).isEqualTo(UPDATED_TOTAL_EN_CERTIFICACION);
        assertThat(testEstadosHistorias.getTotalPruebasUAT()).isEqualTo(UPDATED_TOTAL_PRUEBAS_UAT);
        assertThat(testEstadosHistorias.getTotalListoProduccion()).isEqualTo(UPDATED_TOTAL_LISTO_PRODUCCION);
        assertThat(testEstadosHistorias.getTotalEnProduccion()).isEqualTo(UPDATED_TOTAL_EN_PRODUCCION);
        assertThat(testEstadosHistorias.getTotalAprobadosPO()).isEqualTo(UPDATED_TOTAL_APROBADOS_PO);
    }

    @Test
    @Transactional
    void patchNonExistingEstadosHistorias() throws Exception {
        int databaseSizeBeforeUpdate = estadosHistoriasRepository.findAll().size();
        estadosHistorias.setId(count.incrementAndGet());

        // Create the EstadosHistorias
        EstadosHistoriasDTO estadosHistoriasDTO = estadosHistoriasMapper.toDto(estadosHistorias);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restEstadosHistoriasMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, estadosHistoriasDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(estadosHistoriasDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the EstadosHistorias in the database
        List<EstadosHistorias> estadosHistoriasList = estadosHistoriasRepository.findAll();
        assertThat(estadosHistoriasList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchEstadosHistorias() throws Exception {
        int databaseSizeBeforeUpdate = estadosHistoriasRepository.findAll().size();
        estadosHistorias.setId(count.incrementAndGet());

        // Create the EstadosHistorias
        EstadosHistoriasDTO estadosHistoriasDTO = estadosHistoriasMapper.toDto(estadosHistorias);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEstadosHistoriasMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(estadosHistoriasDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the EstadosHistorias in the database
        List<EstadosHistorias> estadosHistoriasList = estadosHistoriasRepository.findAll();
        assertThat(estadosHistoriasList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamEstadosHistorias() throws Exception {
        int databaseSizeBeforeUpdate = estadosHistoriasRepository.findAll().size();
        estadosHistorias.setId(count.incrementAndGet());

        // Create the EstadosHistorias
        EstadosHistoriasDTO estadosHistoriasDTO = estadosHistoriasMapper.toDto(estadosHistorias);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restEstadosHistoriasMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(estadosHistoriasDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the EstadosHistorias in the database
        List<EstadosHistorias> estadosHistoriasList = estadosHistoriasRepository.findAll();
        assertThat(estadosHistoriasList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteEstadosHistorias() throws Exception {
        // Initialize the database
        estadosHistoriasRepository.saveAndFlush(estadosHistorias);

        int databaseSizeBeforeDelete = estadosHistoriasRepository.findAll().size();

        // Delete the estadosHistorias
        restEstadosHistoriasMockMvc
            .perform(delete(ENTITY_API_URL_ID, estadosHistorias.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<EstadosHistorias> estadosHistoriasList = estadosHistoriasRepository.findAll();
        assertThat(estadosHistoriasList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
