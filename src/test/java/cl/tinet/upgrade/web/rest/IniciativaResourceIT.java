package cl.tinet.upgrade.web.rest;

import static cl.tinet.upgrade.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import cl.tinet.upgrade.IntegrationTest;
import cl.tinet.upgrade.domain.Iniciativa;
import cl.tinet.upgrade.repository.IniciativaRepository;
import cl.tinet.upgrade.service.dto.IniciativaDTO;
import cl.tinet.upgrade.service.mapper.IniciativaMapper;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link IniciativaResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class IniciativaResourceIT {

    private static final String DEFAULT_NOMBRE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_FECHA_INICIO = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_FECHA_INICIO = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_FECHA_FIN = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_FECHA_FIN = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_FECHA_CREACION = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_FECHA_CREACION = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_IS_ACTIVO = false;
    private static final Boolean UPDATED_IS_ACTIVO = true;

    private static final String DEFAULT_PROMESA = "AAAAAAAAAA";
    private static final String UPDATED_PROMESA = "BBBBBBBBBB";

    private static final String DEFAULT_TITULO_PRODUCTO_DIGITAL = "AAAAAAAAAA";
    private static final String UPDATED_TITULO_PRODUCTO_DIGITAL = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPCION_PRODUCTO_DIGITAL = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPCION_PRODUCTO_DIGITAL = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/iniciativas";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private IniciativaRepository iniciativaRepository;

    @Autowired
    private IniciativaMapper iniciativaMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restIniciativaMockMvc;

    private Iniciativa iniciativa;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Iniciativa createEntity(EntityManager em) {
        Iniciativa iniciativa = new Iniciativa()
            .nombre(DEFAULT_NOMBRE)
            .fechaInicio(DEFAULT_FECHA_INICIO)
            .fechaFin(DEFAULT_FECHA_FIN)
            .fechaCreacion(DEFAULT_FECHA_CREACION)
            .isActivo(DEFAULT_IS_ACTIVO)
            .promesa(DEFAULT_PROMESA)
            .tituloProductoDigital(DEFAULT_TITULO_PRODUCTO_DIGITAL)
            .descripcionProductoDigital(DEFAULT_DESCRIPCION_PRODUCTO_DIGITAL);
        return iniciativa;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Iniciativa createUpdatedEntity(EntityManager em) {
        Iniciativa iniciativa = new Iniciativa()
            .nombre(UPDATED_NOMBRE)
            .fechaInicio(UPDATED_FECHA_INICIO)
            .fechaFin(UPDATED_FECHA_FIN)
            .fechaCreacion(UPDATED_FECHA_CREACION)
            .isActivo(UPDATED_IS_ACTIVO)
            .promesa(UPDATED_PROMESA)
            .tituloProductoDigital(UPDATED_TITULO_PRODUCTO_DIGITAL)
            .descripcionProductoDigital(UPDATED_DESCRIPCION_PRODUCTO_DIGITAL);
        return iniciativa;
    }

    @BeforeEach
    public void initTest() {
        iniciativa = createEntity(em);
    }

    @Test
    @Transactional
    void createIniciativa() throws Exception {
        int databaseSizeBeforeCreate = iniciativaRepository.findAll().size();
        // Create the Iniciativa
        IniciativaDTO iniciativaDTO = iniciativaMapper.toDto(iniciativa);
        restIniciativaMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(iniciativaDTO)))
            .andExpect(status().isCreated());

        // Validate the Iniciativa in the database
        List<Iniciativa> iniciativaList = iniciativaRepository.findAll();
        assertThat(iniciativaList).hasSize(databaseSizeBeforeCreate + 1);
        Iniciativa testIniciativa = iniciativaList.get(iniciativaList.size() - 1);
        assertThat(testIniciativa.getNombre()).isEqualTo(DEFAULT_NOMBRE);
        assertThat(testIniciativa.getFechaInicio()).isEqualTo(DEFAULT_FECHA_INICIO);
        assertThat(testIniciativa.getFechaFin()).isEqualTo(DEFAULT_FECHA_FIN);
        assertThat(testIniciativa.getFechaCreacion()).isEqualTo(DEFAULT_FECHA_CREACION);
        assertThat(testIniciativa.getIsActivo()).isEqualTo(DEFAULT_IS_ACTIVO);
        assertThat(testIniciativa.getPromesa()).isEqualTo(DEFAULT_PROMESA);
        assertThat(testIniciativa.getTituloProductoDigital()).isEqualTo(DEFAULT_TITULO_PRODUCTO_DIGITAL);
        assertThat(testIniciativa.getDescripcionProductoDigital()).isEqualTo(DEFAULT_DESCRIPCION_PRODUCTO_DIGITAL);
    }

    @Test
    @Transactional
    void createIniciativaWithExistingId() throws Exception {
        // Create the Iniciativa with an existing ID
        iniciativa.setId(1L);
        IniciativaDTO iniciativaDTO = iniciativaMapper.toDto(iniciativa);

        int databaseSizeBeforeCreate = iniciativaRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restIniciativaMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(iniciativaDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Iniciativa in the database
        List<Iniciativa> iniciativaList = iniciativaRepository.findAll();
        assertThat(iniciativaList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkNombreIsRequired() throws Exception {
        int databaseSizeBeforeTest = iniciativaRepository.findAll().size();
        // set the field null
        iniciativa.setNombre(null);

        // Create the Iniciativa, which fails.
        IniciativaDTO iniciativaDTO = iniciativaMapper.toDto(iniciativa);

        restIniciativaMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(iniciativaDTO)))
            .andExpect(status().isBadRequest());

        List<Iniciativa> iniciativaList = iniciativaRepository.findAll();
        assertThat(iniciativaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkFechaInicioIsRequired() throws Exception {
        int databaseSizeBeforeTest = iniciativaRepository.findAll().size();
        // set the field null
        iniciativa.setFechaInicio(null);

        // Create the Iniciativa, which fails.
        IniciativaDTO iniciativaDTO = iniciativaMapper.toDto(iniciativa);

        restIniciativaMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(iniciativaDTO)))
            .andExpect(status().isBadRequest());

        List<Iniciativa> iniciativaList = iniciativaRepository.findAll();
        assertThat(iniciativaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkFechaFinIsRequired() throws Exception {
        int databaseSizeBeforeTest = iniciativaRepository.findAll().size();
        // set the field null
        iniciativa.setFechaFin(null);

        // Create the Iniciativa, which fails.
        IniciativaDTO iniciativaDTO = iniciativaMapper.toDto(iniciativa);

        restIniciativaMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(iniciativaDTO)))
            .andExpect(status().isBadRequest());

        List<Iniciativa> iniciativaList = iniciativaRepository.findAll();
        assertThat(iniciativaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkFechaCreacionIsRequired() throws Exception {
        int databaseSizeBeforeTest = iniciativaRepository.findAll().size();
        // set the field null
        iniciativa.setFechaCreacion(null);

        // Create the Iniciativa, which fails.
        IniciativaDTO iniciativaDTO = iniciativaMapper.toDto(iniciativa);

        restIniciativaMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(iniciativaDTO)))
            .andExpect(status().isBadRequest());

        List<Iniciativa> iniciativaList = iniciativaRepository.findAll();
        assertThat(iniciativaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkIsActivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = iniciativaRepository.findAll().size();
        // set the field null
        iniciativa.setIsActivo(null);

        // Create the Iniciativa, which fails.
        IniciativaDTO iniciativaDTO = iniciativaMapper.toDto(iniciativa);

        restIniciativaMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(iniciativaDTO)))
            .andExpect(status().isBadRequest());

        List<Iniciativa> iniciativaList = iniciativaRepository.findAll();
        assertThat(iniciativaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkPromesaIsRequired() throws Exception {
        int databaseSizeBeforeTest = iniciativaRepository.findAll().size();
        // set the field null
        iniciativa.setPromesa(null);

        // Create the Iniciativa, which fails.
        IniciativaDTO iniciativaDTO = iniciativaMapper.toDto(iniciativa);

        restIniciativaMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(iniciativaDTO)))
            .andExpect(status().isBadRequest());

        List<Iniciativa> iniciativaList = iniciativaRepository.findAll();
        assertThat(iniciativaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTituloProductoDigitalIsRequired() throws Exception {
        int databaseSizeBeforeTest = iniciativaRepository.findAll().size();
        // set the field null
        iniciativa.setTituloProductoDigital(null);

        // Create the Iniciativa, which fails.
        IniciativaDTO iniciativaDTO = iniciativaMapper.toDto(iniciativa);

        restIniciativaMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(iniciativaDTO)))
            .andExpect(status().isBadRequest());

        List<Iniciativa> iniciativaList = iniciativaRepository.findAll();
        assertThat(iniciativaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkDescripcionProductoDigitalIsRequired() throws Exception {
        int databaseSizeBeforeTest = iniciativaRepository.findAll().size();
        // set the field null
        iniciativa.setDescripcionProductoDigital(null);

        // Create the Iniciativa, which fails.
        IniciativaDTO iniciativaDTO = iniciativaMapper.toDto(iniciativa);

        restIniciativaMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(iniciativaDTO)))
            .andExpect(status().isBadRequest());

        List<Iniciativa> iniciativaList = iniciativaRepository.findAll();
        assertThat(iniciativaList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllIniciativas() throws Exception {
        // Initialize the database
        iniciativaRepository.saveAndFlush(iniciativa);

        // Get all the iniciativaList
        restIniciativaMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(iniciativa.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE)))
            .andExpect(jsonPath("$.[*].fechaInicio").value(hasItem(sameInstant(DEFAULT_FECHA_INICIO))))
            .andExpect(jsonPath("$.[*].fechaFin").value(hasItem(sameInstant(DEFAULT_FECHA_FIN))))
            .andExpect(jsonPath("$.[*].fechaCreacion").value(hasItem(sameInstant(DEFAULT_FECHA_CREACION))))
            .andExpect(jsonPath("$.[*].isActivo").value(hasItem(DEFAULT_IS_ACTIVO.booleanValue())))
            .andExpect(jsonPath("$.[*].promesa").value(hasItem(DEFAULT_PROMESA)))
            .andExpect(jsonPath("$.[*].tituloProductoDigital").value(hasItem(DEFAULT_TITULO_PRODUCTO_DIGITAL)))
            .andExpect(jsonPath("$.[*].descripcionProductoDigital").value(hasItem(DEFAULT_DESCRIPCION_PRODUCTO_DIGITAL)));
    }

    @Test
    @Transactional
    void getIniciativa() throws Exception {
        // Initialize the database
        iniciativaRepository.saveAndFlush(iniciativa);

        // Get the iniciativa
        restIniciativaMockMvc
            .perform(get(ENTITY_API_URL_ID, iniciativa.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(iniciativa.getId().intValue()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE))
            .andExpect(jsonPath("$.fechaInicio").value(sameInstant(DEFAULT_FECHA_INICIO)))
            .andExpect(jsonPath("$.fechaFin").value(sameInstant(DEFAULT_FECHA_FIN)))
            .andExpect(jsonPath("$.fechaCreacion").value(sameInstant(DEFAULT_FECHA_CREACION)))
            .andExpect(jsonPath("$.isActivo").value(DEFAULT_IS_ACTIVO.booleanValue()))
            .andExpect(jsonPath("$.promesa").value(DEFAULT_PROMESA))
            .andExpect(jsonPath("$.tituloProductoDigital").value(DEFAULT_TITULO_PRODUCTO_DIGITAL))
            .andExpect(jsonPath("$.descripcionProductoDigital").value(DEFAULT_DESCRIPCION_PRODUCTO_DIGITAL));
    }

    @Test
    @Transactional
    void getNonExistingIniciativa() throws Exception {
        // Get the iniciativa
        restIniciativaMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewIniciativa() throws Exception {
        // Initialize the database
        iniciativaRepository.saveAndFlush(iniciativa);

        int databaseSizeBeforeUpdate = iniciativaRepository.findAll().size();

        // Update the iniciativa
        Iniciativa updatedIniciativa = iniciativaRepository.findById(iniciativa.getId()).get();
        // Disconnect from session so that the updates on updatedIniciativa are not directly saved in db
        em.detach(updatedIniciativa);
        updatedIniciativa
            .nombre(UPDATED_NOMBRE)
            .fechaInicio(UPDATED_FECHA_INICIO)
            .fechaFin(UPDATED_FECHA_FIN)
            .fechaCreacion(UPDATED_FECHA_CREACION)
            .isActivo(UPDATED_IS_ACTIVO)
            .promesa(UPDATED_PROMESA)
            .tituloProductoDigital(UPDATED_TITULO_PRODUCTO_DIGITAL)
            .descripcionProductoDigital(UPDATED_DESCRIPCION_PRODUCTO_DIGITAL);
        IniciativaDTO iniciativaDTO = iniciativaMapper.toDto(updatedIniciativa);

        restIniciativaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, iniciativaDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(iniciativaDTO))
            )
            .andExpect(status().isOk());

        // Validate the Iniciativa in the database
        List<Iniciativa> iniciativaList = iniciativaRepository.findAll();
        assertThat(iniciativaList).hasSize(databaseSizeBeforeUpdate);
        Iniciativa testIniciativa = iniciativaList.get(iniciativaList.size() - 1);
        assertThat(testIniciativa.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testIniciativa.getFechaInicio()).isEqualTo(UPDATED_FECHA_INICIO);
        assertThat(testIniciativa.getFechaFin()).isEqualTo(UPDATED_FECHA_FIN);
        assertThat(testIniciativa.getFechaCreacion()).isEqualTo(UPDATED_FECHA_CREACION);
        assertThat(testIniciativa.getIsActivo()).isEqualTo(UPDATED_IS_ACTIVO);
        assertThat(testIniciativa.getPromesa()).isEqualTo(UPDATED_PROMESA);
        assertThat(testIniciativa.getTituloProductoDigital()).isEqualTo(UPDATED_TITULO_PRODUCTO_DIGITAL);
        assertThat(testIniciativa.getDescripcionProductoDigital()).isEqualTo(UPDATED_DESCRIPCION_PRODUCTO_DIGITAL);
    }

    @Test
    @Transactional
    void putNonExistingIniciativa() throws Exception {
        int databaseSizeBeforeUpdate = iniciativaRepository.findAll().size();
        iniciativa.setId(count.incrementAndGet());

        // Create the Iniciativa
        IniciativaDTO iniciativaDTO = iniciativaMapper.toDto(iniciativa);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIniciativaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, iniciativaDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(iniciativaDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Iniciativa in the database
        List<Iniciativa> iniciativaList = iniciativaRepository.findAll();
        assertThat(iniciativaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchIniciativa() throws Exception {
        int databaseSizeBeforeUpdate = iniciativaRepository.findAll().size();
        iniciativa.setId(count.incrementAndGet());

        // Create the Iniciativa
        IniciativaDTO iniciativaDTO = iniciativaMapper.toDto(iniciativa);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restIniciativaMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(iniciativaDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Iniciativa in the database
        List<Iniciativa> iniciativaList = iniciativaRepository.findAll();
        assertThat(iniciativaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamIniciativa() throws Exception {
        int databaseSizeBeforeUpdate = iniciativaRepository.findAll().size();
        iniciativa.setId(count.incrementAndGet());

        // Create the Iniciativa
        IniciativaDTO iniciativaDTO = iniciativaMapper.toDto(iniciativa);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restIniciativaMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(iniciativaDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Iniciativa in the database
        List<Iniciativa> iniciativaList = iniciativaRepository.findAll();
        assertThat(iniciativaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateIniciativaWithPatch() throws Exception {
        // Initialize the database
        iniciativaRepository.saveAndFlush(iniciativa);

        int databaseSizeBeforeUpdate = iniciativaRepository.findAll().size();

        // Update the iniciativa using partial update
        Iniciativa partialUpdatedIniciativa = new Iniciativa();
        partialUpdatedIniciativa.setId(iniciativa.getId());

        partialUpdatedIniciativa
            .nombre(UPDATED_NOMBRE)
            .fechaInicio(UPDATED_FECHA_INICIO)
            .fechaFin(UPDATED_FECHA_FIN)
            .fechaCreacion(UPDATED_FECHA_CREACION)
            .isActivo(UPDATED_IS_ACTIVO)
            .promesa(UPDATED_PROMESA)
            .tituloProductoDigital(UPDATED_TITULO_PRODUCTO_DIGITAL);

        restIniciativaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedIniciativa.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedIniciativa))
            )
            .andExpect(status().isOk());

        // Validate the Iniciativa in the database
        List<Iniciativa> iniciativaList = iniciativaRepository.findAll();
        assertThat(iniciativaList).hasSize(databaseSizeBeforeUpdate);
        Iniciativa testIniciativa = iniciativaList.get(iniciativaList.size() - 1);
        assertThat(testIniciativa.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testIniciativa.getFechaInicio()).isEqualTo(UPDATED_FECHA_INICIO);
        assertThat(testIniciativa.getFechaFin()).isEqualTo(UPDATED_FECHA_FIN);
        assertThat(testIniciativa.getFechaCreacion()).isEqualTo(UPDATED_FECHA_CREACION);
        assertThat(testIniciativa.getIsActivo()).isEqualTo(UPDATED_IS_ACTIVO);
        assertThat(testIniciativa.getPromesa()).isEqualTo(UPDATED_PROMESA);
        assertThat(testIniciativa.getTituloProductoDigital()).isEqualTo(UPDATED_TITULO_PRODUCTO_DIGITAL);
        assertThat(testIniciativa.getDescripcionProductoDigital()).isEqualTo(DEFAULT_DESCRIPCION_PRODUCTO_DIGITAL);
    }

    @Test
    @Transactional
    void fullUpdateIniciativaWithPatch() throws Exception {
        // Initialize the database
        iniciativaRepository.saveAndFlush(iniciativa);

        int databaseSizeBeforeUpdate = iniciativaRepository.findAll().size();

        // Update the iniciativa using partial update
        Iniciativa partialUpdatedIniciativa = new Iniciativa();
        partialUpdatedIniciativa.setId(iniciativa.getId());

        partialUpdatedIniciativa
            .nombre(UPDATED_NOMBRE)
            .fechaInicio(UPDATED_FECHA_INICIO)
            .fechaFin(UPDATED_FECHA_FIN)
            .fechaCreacion(UPDATED_FECHA_CREACION)
            .isActivo(UPDATED_IS_ACTIVO)
            .promesa(UPDATED_PROMESA)
            .tituloProductoDigital(UPDATED_TITULO_PRODUCTO_DIGITAL)
            .descripcionProductoDigital(UPDATED_DESCRIPCION_PRODUCTO_DIGITAL);

        restIniciativaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedIniciativa.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedIniciativa))
            )
            .andExpect(status().isOk());

        // Validate the Iniciativa in the database
        List<Iniciativa> iniciativaList = iniciativaRepository.findAll();
        assertThat(iniciativaList).hasSize(databaseSizeBeforeUpdate);
        Iniciativa testIniciativa = iniciativaList.get(iniciativaList.size() - 1);
        assertThat(testIniciativa.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testIniciativa.getFechaInicio()).isEqualTo(UPDATED_FECHA_INICIO);
        assertThat(testIniciativa.getFechaFin()).isEqualTo(UPDATED_FECHA_FIN);
        assertThat(testIniciativa.getFechaCreacion()).isEqualTo(UPDATED_FECHA_CREACION);
        assertThat(testIniciativa.getIsActivo()).isEqualTo(UPDATED_IS_ACTIVO);
        assertThat(testIniciativa.getPromesa()).isEqualTo(UPDATED_PROMESA);
        assertThat(testIniciativa.getTituloProductoDigital()).isEqualTo(UPDATED_TITULO_PRODUCTO_DIGITAL);
        assertThat(testIniciativa.getDescripcionProductoDigital()).isEqualTo(UPDATED_DESCRIPCION_PRODUCTO_DIGITAL);
    }

    @Test
    @Transactional
    void patchNonExistingIniciativa() throws Exception {
        int databaseSizeBeforeUpdate = iniciativaRepository.findAll().size();
        iniciativa.setId(count.incrementAndGet());

        // Create the Iniciativa
        IniciativaDTO iniciativaDTO = iniciativaMapper.toDto(iniciativa);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restIniciativaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, iniciativaDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(iniciativaDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Iniciativa in the database
        List<Iniciativa> iniciativaList = iniciativaRepository.findAll();
        assertThat(iniciativaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchIniciativa() throws Exception {
        int databaseSizeBeforeUpdate = iniciativaRepository.findAll().size();
        iniciativa.setId(count.incrementAndGet());

        // Create the Iniciativa
        IniciativaDTO iniciativaDTO = iniciativaMapper.toDto(iniciativa);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restIniciativaMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(iniciativaDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Iniciativa in the database
        List<Iniciativa> iniciativaList = iniciativaRepository.findAll();
        assertThat(iniciativaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamIniciativa() throws Exception {
        int databaseSizeBeforeUpdate = iniciativaRepository.findAll().size();
        iniciativa.setId(count.incrementAndGet());

        // Create the Iniciativa
        IniciativaDTO iniciativaDTO = iniciativaMapper.toDto(iniciativa);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restIniciativaMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(iniciativaDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Iniciativa in the database
        List<Iniciativa> iniciativaList = iniciativaRepository.findAll();
        assertThat(iniciativaList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteIniciativa() throws Exception {
        // Initialize the database
        iniciativaRepository.saveAndFlush(iniciativa);

        int databaseSizeBeforeDelete = iniciativaRepository.findAll().size();

        // Delete the iniciativa
        restIniciativaMockMvc
            .perform(delete(ENTITY_API_URL_ID, iniciativa.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Iniciativa> iniciativaList = iniciativaRepository.findAll();
        assertThat(iniciativaList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
