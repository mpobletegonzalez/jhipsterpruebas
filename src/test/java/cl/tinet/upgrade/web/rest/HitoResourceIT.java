package cl.tinet.upgrade.web.rest;

import static cl.tinet.upgrade.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import cl.tinet.upgrade.IntegrationTest;
import cl.tinet.upgrade.domain.Hito;
import cl.tinet.upgrade.domain.enumeration.TipoHito;
import cl.tinet.upgrade.repository.HitoRepository;
import cl.tinet.upgrade.service.dto.HitoDTO;
import cl.tinet.upgrade.service.mapper.HitoMapper;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link HitoResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class HitoResourceIT {

    private static final String DEFAULT_NOMBRE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPCION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPCION = "BBBBBBBBBB";

    private static final ZonedDateTime DEFAULT_FECHA_INICIO = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_FECHA_INICIO = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_FECHA_CUMPLIMIENTO = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_FECHA_CUMPLIMIENTO = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final TipoHito DEFAULT_TIPO_HITO = TipoHito.RLS;
    private static final TipoHito UPDATED_TIPO_HITO = TipoHito.PRD;

    private static final ZonedDateTime DEFAULT_FECHA_CREACION = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_FECHA_CREACION = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final Boolean DEFAULT_IS_ACTIVO = false;
    private static final Boolean UPDATED_IS_ACTIVO = true;

    private static final String ENTITY_API_URL = "/api/hitos";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private HitoRepository hitoRepository;

    @Autowired
    private HitoMapper hitoMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restHitoMockMvc;

    private Hito hito;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Hito createEntity(EntityManager em) {
        Hito hito = new Hito()
            .nombre(DEFAULT_NOMBRE)
            .descripcion(DEFAULT_DESCRIPCION)
            .fechaInicio(DEFAULT_FECHA_INICIO)
            .fechaCumplimiento(DEFAULT_FECHA_CUMPLIMIENTO)
            .tipoHito(DEFAULT_TIPO_HITO)
            .fechaCreacion(DEFAULT_FECHA_CREACION)
            .isActivo(DEFAULT_IS_ACTIVO);
        return hito;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Hito createUpdatedEntity(EntityManager em) {
        Hito hito = new Hito()
            .nombre(UPDATED_NOMBRE)
            .descripcion(UPDATED_DESCRIPCION)
            .fechaInicio(UPDATED_FECHA_INICIO)
            .fechaCumplimiento(UPDATED_FECHA_CUMPLIMIENTO)
            .tipoHito(UPDATED_TIPO_HITO)
            .fechaCreacion(UPDATED_FECHA_CREACION)
            .isActivo(UPDATED_IS_ACTIVO);
        return hito;
    }

    @BeforeEach
    public void initTest() {
        hito = createEntity(em);
    }

    @Test
    @Transactional
    void createHito() throws Exception {
        int databaseSizeBeforeCreate = hitoRepository.findAll().size();
        // Create the Hito
        HitoDTO hitoDTO = hitoMapper.toDto(hito);
        restHitoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(hitoDTO)))
            .andExpect(status().isCreated());

        // Validate the Hito in the database
        List<Hito> hitoList = hitoRepository.findAll();
        assertThat(hitoList).hasSize(databaseSizeBeforeCreate + 1);
        Hito testHito = hitoList.get(hitoList.size() - 1);
        assertThat(testHito.getNombre()).isEqualTo(DEFAULT_NOMBRE);
        assertThat(testHito.getDescripcion()).isEqualTo(DEFAULT_DESCRIPCION);
        assertThat(testHito.getFechaInicio()).isEqualTo(DEFAULT_FECHA_INICIO);
        assertThat(testHito.getFechaCumplimiento()).isEqualTo(DEFAULT_FECHA_CUMPLIMIENTO);
        assertThat(testHito.getTipoHito()).isEqualTo(DEFAULT_TIPO_HITO);
        assertThat(testHito.getFechaCreacion()).isEqualTo(DEFAULT_FECHA_CREACION);
        assertThat(testHito.getIsActivo()).isEqualTo(DEFAULT_IS_ACTIVO);
    }

    @Test
    @Transactional
    void createHitoWithExistingId() throws Exception {
        // Create the Hito with an existing ID
        hito.setId(1L);
        HitoDTO hitoDTO = hitoMapper.toDto(hito);

        int databaseSizeBeforeCreate = hitoRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restHitoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(hitoDTO)))
            .andExpect(status().isBadRequest());

        // Validate the Hito in the database
        List<Hito> hitoList = hitoRepository.findAll();
        assertThat(hitoList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkDescripcionIsRequired() throws Exception {
        int databaseSizeBeforeTest = hitoRepository.findAll().size();
        // set the field null
        hito.setDescripcion(null);

        // Create the Hito, which fails.
        HitoDTO hitoDTO = hitoMapper.toDto(hito);

        restHitoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(hitoDTO)))
            .andExpect(status().isBadRequest());

        List<Hito> hitoList = hitoRepository.findAll();
        assertThat(hitoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkTipoHitoIsRequired() throws Exception {
        int databaseSizeBeforeTest = hitoRepository.findAll().size();
        // set the field null
        hito.setTipoHito(null);

        // Create the Hito, which fails.
        HitoDTO hitoDTO = hitoMapper.toDto(hito);

        restHitoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(hitoDTO)))
            .andExpect(status().isBadRequest());

        List<Hito> hitoList = hitoRepository.findAll();
        assertThat(hitoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkFechaCreacionIsRequired() throws Exception {
        int databaseSizeBeforeTest = hitoRepository.findAll().size();
        // set the field null
        hito.setFechaCreacion(null);

        // Create the Hito, which fails.
        HitoDTO hitoDTO = hitoMapper.toDto(hito);

        restHitoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(hitoDTO)))
            .andExpect(status().isBadRequest());

        List<Hito> hitoList = hitoRepository.findAll();
        assertThat(hitoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkIsActivoIsRequired() throws Exception {
        int databaseSizeBeforeTest = hitoRepository.findAll().size();
        // set the field null
        hito.setIsActivo(null);

        // Create the Hito, which fails.
        HitoDTO hitoDTO = hitoMapper.toDto(hito);

        restHitoMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(hitoDTO)))
            .andExpect(status().isBadRequest());

        List<Hito> hitoList = hitoRepository.findAll();
        assertThat(hitoList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllHitos() throws Exception {
        // Initialize the database
        hitoRepository.saveAndFlush(hito);

        // Get all the hitoList
        restHitoMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(hito.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE)))
            .andExpect(jsonPath("$.[*].descripcion").value(hasItem(DEFAULT_DESCRIPCION)))
            .andExpect(jsonPath("$.[*].fechaInicio").value(hasItem(sameInstant(DEFAULT_FECHA_INICIO))))
            .andExpect(jsonPath("$.[*].fechaCumplimiento").value(hasItem(sameInstant(DEFAULT_FECHA_CUMPLIMIENTO))))
            .andExpect(jsonPath("$.[*].tipoHito").value(hasItem(DEFAULT_TIPO_HITO.toString())))
            .andExpect(jsonPath("$.[*].fechaCreacion").value(hasItem(sameInstant(DEFAULT_FECHA_CREACION))))
            .andExpect(jsonPath("$.[*].isActivo").value(hasItem(DEFAULT_IS_ACTIVO.booleanValue())));
    }

    @Test
    @Transactional
    void getHito() throws Exception {
        // Initialize the database
        hitoRepository.saveAndFlush(hito);

        // Get the hito
        restHitoMockMvc
            .perform(get(ENTITY_API_URL_ID, hito.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(hito.getId().intValue()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE))
            .andExpect(jsonPath("$.descripcion").value(DEFAULT_DESCRIPCION))
            .andExpect(jsonPath("$.fechaInicio").value(sameInstant(DEFAULT_FECHA_INICIO)))
            .andExpect(jsonPath("$.fechaCumplimiento").value(sameInstant(DEFAULT_FECHA_CUMPLIMIENTO)))
            .andExpect(jsonPath("$.tipoHito").value(DEFAULT_TIPO_HITO.toString()))
            .andExpect(jsonPath("$.fechaCreacion").value(sameInstant(DEFAULT_FECHA_CREACION)))
            .andExpect(jsonPath("$.isActivo").value(DEFAULT_IS_ACTIVO.booleanValue()));
    }

    @Test
    @Transactional
    void getNonExistingHito() throws Exception {
        // Get the hito
        restHitoMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewHito() throws Exception {
        // Initialize the database
        hitoRepository.saveAndFlush(hito);

        int databaseSizeBeforeUpdate = hitoRepository.findAll().size();

        // Update the hito
        Hito updatedHito = hitoRepository.findById(hito.getId()).get();
        // Disconnect from session so that the updates on updatedHito are not directly saved in db
        em.detach(updatedHito);
        updatedHito
            .nombre(UPDATED_NOMBRE)
            .descripcion(UPDATED_DESCRIPCION)
            .fechaInicio(UPDATED_FECHA_INICIO)
            .fechaCumplimiento(UPDATED_FECHA_CUMPLIMIENTO)
            .tipoHito(UPDATED_TIPO_HITO)
            .fechaCreacion(UPDATED_FECHA_CREACION)
            .isActivo(UPDATED_IS_ACTIVO);
        HitoDTO hitoDTO = hitoMapper.toDto(updatedHito);

        restHitoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, hitoDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(hitoDTO))
            )
            .andExpect(status().isOk());

        // Validate the Hito in the database
        List<Hito> hitoList = hitoRepository.findAll();
        assertThat(hitoList).hasSize(databaseSizeBeforeUpdate);
        Hito testHito = hitoList.get(hitoList.size() - 1);
        assertThat(testHito.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testHito.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
        assertThat(testHito.getFechaInicio()).isEqualTo(UPDATED_FECHA_INICIO);
        assertThat(testHito.getFechaCumplimiento()).isEqualTo(UPDATED_FECHA_CUMPLIMIENTO);
        assertThat(testHito.getTipoHito()).isEqualTo(UPDATED_TIPO_HITO);
        assertThat(testHito.getFechaCreacion()).isEqualTo(UPDATED_FECHA_CREACION);
        assertThat(testHito.getIsActivo()).isEqualTo(UPDATED_IS_ACTIVO);
    }

    @Test
    @Transactional
    void putNonExistingHito() throws Exception {
        int databaseSizeBeforeUpdate = hitoRepository.findAll().size();
        hito.setId(count.incrementAndGet());

        // Create the Hito
        HitoDTO hitoDTO = hitoMapper.toDto(hito);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHitoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, hitoDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(hitoDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Hito in the database
        List<Hito> hitoList = hitoRepository.findAll();
        assertThat(hitoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchHito() throws Exception {
        int databaseSizeBeforeUpdate = hitoRepository.findAll().size();
        hito.setId(count.incrementAndGet());

        // Create the Hito
        HitoDTO hitoDTO = hitoMapper.toDto(hito);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHitoMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(hitoDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Hito in the database
        List<Hito> hitoList = hitoRepository.findAll();
        assertThat(hitoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamHito() throws Exception {
        int databaseSizeBeforeUpdate = hitoRepository.findAll().size();
        hito.setId(count.incrementAndGet());

        // Create the Hito
        HitoDTO hitoDTO = hitoMapper.toDto(hito);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHitoMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(hitoDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Hito in the database
        List<Hito> hitoList = hitoRepository.findAll();
        assertThat(hitoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateHitoWithPatch() throws Exception {
        // Initialize the database
        hitoRepository.saveAndFlush(hito);

        int databaseSizeBeforeUpdate = hitoRepository.findAll().size();

        // Update the hito using partial update
        Hito partialUpdatedHito = new Hito();
        partialUpdatedHito.setId(hito.getId());

        partialUpdatedHito.nombre(UPDATED_NOMBRE).fechaCreacion(UPDATED_FECHA_CREACION);

        restHitoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedHito.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedHito))
            )
            .andExpect(status().isOk());

        // Validate the Hito in the database
        List<Hito> hitoList = hitoRepository.findAll();
        assertThat(hitoList).hasSize(databaseSizeBeforeUpdate);
        Hito testHito = hitoList.get(hitoList.size() - 1);
        assertThat(testHito.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testHito.getDescripcion()).isEqualTo(DEFAULT_DESCRIPCION);
        assertThat(testHito.getFechaInicio()).isEqualTo(DEFAULT_FECHA_INICIO);
        assertThat(testHito.getFechaCumplimiento()).isEqualTo(DEFAULT_FECHA_CUMPLIMIENTO);
        assertThat(testHito.getTipoHito()).isEqualTo(DEFAULT_TIPO_HITO);
        assertThat(testHito.getFechaCreacion()).isEqualTo(UPDATED_FECHA_CREACION);
        assertThat(testHito.getIsActivo()).isEqualTo(DEFAULT_IS_ACTIVO);
    }

    @Test
    @Transactional
    void fullUpdateHitoWithPatch() throws Exception {
        // Initialize the database
        hitoRepository.saveAndFlush(hito);

        int databaseSizeBeforeUpdate = hitoRepository.findAll().size();

        // Update the hito using partial update
        Hito partialUpdatedHito = new Hito();
        partialUpdatedHito.setId(hito.getId());

        partialUpdatedHito
            .nombre(UPDATED_NOMBRE)
            .descripcion(UPDATED_DESCRIPCION)
            .fechaInicio(UPDATED_FECHA_INICIO)
            .fechaCumplimiento(UPDATED_FECHA_CUMPLIMIENTO)
            .tipoHito(UPDATED_TIPO_HITO)
            .fechaCreacion(UPDATED_FECHA_CREACION)
            .isActivo(UPDATED_IS_ACTIVO);

        restHitoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedHito.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedHito))
            )
            .andExpect(status().isOk());

        // Validate the Hito in the database
        List<Hito> hitoList = hitoRepository.findAll();
        assertThat(hitoList).hasSize(databaseSizeBeforeUpdate);
        Hito testHito = hitoList.get(hitoList.size() - 1);
        assertThat(testHito.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testHito.getDescripcion()).isEqualTo(UPDATED_DESCRIPCION);
        assertThat(testHito.getFechaInicio()).isEqualTo(UPDATED_FECHA_INICIO);
        assertThat(testHito.getFechaCumplimiento()).isEqualTo(UPDATED_FECHA_CUMPLIMIENTO);
        assertThat(testHito.getTipoHito()).isEqualTo(UPDATED_TIPO_HITO);
        assertThat(testHito.getFechaCreacion()).isEqualTo(UPDATED_FECHA_CREACION);
        assertThat(testHito.getIsActivo()).isEqualTo(UPDATED_IS_ACTIVO);
    }

    @Test
    @Transactional
    void patchNonExistingHito() throws Exception {
        int databaseSizeBeforeUpdate = hitoRepository.findAll().size();
        hito.setId(count.incrementAndGet());

        // Create the Hito
        HitoDTO hitoDTO = hitoMapper.toDto(hito);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restHitoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, hitoDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(hitoDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Hito in the database
        List<Hito> hitoList = hitoRepository.findAll();
        assertThat(hitoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchHito() throws Exception {
        int databaseSizeBeforeUpdate = hitoRepository.findAll().size();
        hito.setId(count.incrementAndGet());

        // Create the Hito
        HitoDTO hitoDTO = hitoMapper.toDto(hito);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHitoMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(hitoDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Hito in the database
        List<Hito> hitoList = hitoRepository.findAll();
        assertThat(hitoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamHito() throws Exception {
        int databaseSizeBeforeUpdate = hitoRepository.findAll().size();
        hito.setId(count.incrementAndGet());

        // Create the Hito
        HitoDTO hitoDTO = hitoMapper.toDto(hito);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restHitoMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(hitoDTO)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Hito in the database
        List<Hito> hitoList = hitoRepository.findAll();
        assertThat(hitoList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteHito() throws Exception {
        // Initialize the database
        hitoRepository.saveAndFlush(hito);

        int databaseSizeBeforeDelete = hitoRepository.findAll().size();

        // Delete the hito
        restHitoMockMvc
            .perform(delete(ENTITY_API_URL_ID, hito.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Hito> hitoList = hitoRepository.findAll();
        assertThat(hitoList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
