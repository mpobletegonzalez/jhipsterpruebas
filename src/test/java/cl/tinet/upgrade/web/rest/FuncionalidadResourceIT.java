package cl.tinet.upgrade.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import cl.tinet.upgrade.IntegrationTest;
import cl.tinet.upgrade.domain.Funcionalidad;
import cl.tinet.upgrade.domain.enumeration.TipoEstadoFuncionalidad;
import cl.tinet.upgrade.repository.FuncionalidadRepository;
import cl.tinet.upgrade.service.dto.FuncionalidadDTO;
import cl.tinet.upgrade.service.mapper.FuncionalidadMapper;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link FuncionalidadResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class FuncionalidadResourceIT {

    private static final String DEFAULT_NOMBRE = "AAAAAAAAAA";
    private static final String UPDATED_NOMBRE = "BBBBBBBBBB";

    private static final TipoEstadoFuncionalidad DEFAULT_ESTADO = TipoEstadoFuncionalidad.ORIGINAL;
    private static final TipoEstadoFuncionalidad UPDATED_ESTADO = TipoEstadoFuncionalidad.VIGENTE;

    private static final Integer DEFAULT_PRIORIDAD = 1;
    private static final Integer UPDATED_PRIORIDAD = 2;

    private static final String ENTITY_API_URL = "/api/funcionalidads";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private FuncionalidadRepository funcionalidadRepository;

    @Autowired
    private FuncionalidadMapper funcionalidadMapper;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restFuncionalidadMockMvc;

    private Funcionalidad funcionalidad;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Funcionalidad createEntity(EntityManager em) {
        Funcionalidad funcionalidad = new Funcionalidad().nombre(DEFAULT_NOMBRE).estado(DEFAULT_ESTADO).prioridad(DEFAULT_PRIORIDAD);
        return funcionalidad;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Funcionalidad createUpdatedEntity(EntityManager em) {
        Funcionalidad funcionalidad = new Funcionalidad().nombre(UPDATED_NOMBRE).estado(UPDATED_ESTADO).prioridad(UPDATED_PRIORIDAD);
        return funcionalidad;
    }

    @BeforeEach
    public void initTest() {
        funcionalidad = createEntity(em);
    }

    @Test
    @Transactional
    void createFuncionalidad() throws Exception {
        int databaseSizeBeforeCreate = funcionalidadRepository.findAll().size();
        // Create the Funcionalidad
        FuncionalidadDTO funcionalidadDTO = funcionalidadMapper.toDto(funcionalidad);
        restFuncionalidadMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(funcionalidadDTO))
            )
            .andExpect(status().isCreated());

        // Validate the Funcionalidad in the database
        List<Funcionalidad> funcionalidadList = funcionalidadRepository.findAll();
        assertThat(funcionalidadList).hasSize(databaseSizeBeforeCreate + 1);
        Funcionalidad testFuncionalidad = funcionalidadList.get(funcionalidadList.size() - 1);
        assertThat(testFuncionalidad.getNombre()).isEqualTo(DEFAULT_NOMBRE);
        assertThat(testFuncionalidad.getEstado()).isEqualTo(DEFAULT_ESTADO);
        assertThat(testFuncionalidad.getPrioridad()).isEqualTo(DEFAULT_PRIORIDAD);
    }

    @Test
    @Transactional
    void createFuncionalidadWithExistingId() throws Exception {
        // Create the Funcionalidad with an existing ID
        funcionalidad.setId(1L);
        FuncionalidadDTO funcionalidadDTO = funcionalidadMapper.toDto(funcionalidad);

        int databaseSizeBeforeCreate = funcionalidadRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restFuncionalidadMockMvc
            .perform(
                post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(funcionalidadDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Funcionalidad in the database
        List<Funcionalidad> funcionalidadList = funcionalidadRepository.findAll();
        assertThat(funcionalidadList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void getAllFuncionalidads() throws Exception {
        // Initialize the database
        funcionalidadRepository.saveAndFlush(funcionalidad);

        // Get all the funcionalidadList
        restFuncionalidadMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(funcionalidad.getId().intValue())))
            .andExpect(jsonPath("$.[*].nombre").value(hasItem(DEFAULT_NOMBRE)))
            .andExpect(jsonPath("$.[*].estado").value(hasItem(DEFAULT_ESTADO.toString())))
            .andExpect(jsonPath("$.[*].prioridad").value(hasItem(DEFAULT_PRIORIDAD)));
    }

    @Test
    @Transactional
    void getFuncionalidad() throws Exception {
        // Initialize the database
        funcionalidadRepository.saveAndFlush(funcionalidad);

        // Get the funcionalidad
        restFuncionalidadMockMvc
            .perform(get(ENTITY_API_URL_ID, funcionalidad.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(funcionalidad.getId().intValue()))
            .andExpect(jsonPath("$.nombre").value(DEFAULT_NOMBRE))
            .andExpect(jsonPath("$.estado").value(DEFAULT_ESTADO.toString()))
            .andExpect(jsonPath("$.prioridad").value(DEFAULT_PRIORIDAD));
    }

    @Test
    @Transactional
    void getNonExistingFuncionalidad() throws Exception {
        // Get the funcionalidad
        restFuncionalidadMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewFuncionalidad() throws Exception {
        // Initialize the database
        funcionalidadRepository.saveAndFlush(funcionalidad);

        int databaseSizeBeforeUpdate = funcionalidadRepository.findAll().size();

        // Update the funcionalidad
        Funcionalidad updatedFuncionalidad = funcionalidadRepository.findById(funcionalidad.getId()).get();
        // Disconnect from session so that the updates on updatedFuncionalidad are not directly saved in db
        em.detach(updatedFuncionalidad);
        updatedFuncionalidad.nombre(UPDATED_NOMBRE).estado(UPDATED_ESTADO).prioridad(UPDATED_PRIORIDAD);
        FuncionalidadDTO funcionalidadDTO = funcionalidadMapper.toDto(updatedFuncionalidad);

        restFuncionalidadMockMvc
            .perform(
                put(ENTITY_API_URL_ID, funcionalidadDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(funcionalidadDTO))
            )
            .andExpect(status().isOk());

        // Validate the Funcionalidad in the database
        List<Funcionalidad> funcionalidadList = funcionalidadRepository.findAll();
        assertThat(funcionalidadList).hasSize(databaseSizeBeforeUpdate);
        Funcionalidad testFuncionalidad = funcionalidadList.get(funcionalidadList.size() - 1);
        assertThat(testFuncionalidad.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testFuncionalidad.getEstado()).isEqualTo(UPDATED_ESTADO);
        assertThat(testFuncionalidad.getPrioridad()).isEqualTo(UPDATED_PRIORIDAD);
    }

    @Test
    @Transactional
    void putNonExistingFuncionalidad() throws Exception {
        int databaseSizeBeforeUpdate = funcionalidadRepository.findAll().size();
        funcionalidad.setId(count.incrementAndGet());

        // Create the Funcionalidad
        FuncionalidadDTO funcionalidadDTO = funcionalidadMapper.toDto(funcionalidad);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFuncionalidadMockMvc
            .perform(
                put(ENTITY_API_URL_ID, funcionalidadDTO.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(funcionalidadDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Funcionalidad in the database
        List<Funcionalidad> funcionalidadList = funcionalidadRepository.findAll();
        assertThat(funcionalidadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchFuncionalidad() throws Exception {
        int databaseSizeBeforeUpdate = funcionalidadRepository.findAll().size();
        funcionalidad.setId(count.incrementAndGet());

        // Create the Funcionalidad
        FuncionalidadDTO funcionalidadDTO = funcionalidadMapper.toDto(funcionalidad);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFuncionalidadMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(funcionalidadDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Funcionalidad in the database
        List<Funcionalidad> funcionalidadList = funcionalidadRepository.findAll();
        assertThat(funcionalidadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamFuncionalidad() throws Exception {
        int databaseSizeBeforeUpdate = funcionalidadRepository.findAll().size();
        funcionalidad.setId(count.incrementAndGet());

        // Create the Funcionalidad
        FuncionalidadDTO funcionalidadDTO = funcionalidadMapper.toDto(funcionalidad);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFuncionalidadMockMvc
            .perform(
                put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(funcionalidadDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Funcionalidad in the database
        List<Funcionalidad> funcionalidadList = funcionalidadRepository.findAll();
        assertThat(funcionalidadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateFuncionalidadWithPatch() throws Exception {
        // Initialize the database
        funcionalidadRepository.saveAndFlush(funcionalidad);

        int databaseSizeBeforeUpdate = funcionalidadRepository.findAll().size();

        // Update the funcionalidad using partial update
        Funcionalidad partialUpdatedFuncionalidad = new Funcionalidad();
        partialUpdatedFuncionalidad.setId(funcionalidad.getId());

        partialUpdatedFuncionalidad.nombre(UPDATED_NOMBRE).prioridad(UPDATED_PRIORIDAD);

        restFuncionalidadMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedFuncionalidad.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedFuncionalidad))
            )
            .andExpect(status().isOk());

        // Validate the Funcionalidad in the database
        List<Funcionalidad> funcionalidadList = funcionalidadRepository.findAll();
        assertThat(funcionalidadList).hasSize(databaseSizeBeforeUpdate);
        Funcionalidad testFuncionalidad = funcionalidadList.get(funcionalidadList.size() - 1);
        assertThat(testFuncionalidad.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testFuncionalidad.getEstado()).isEqualTo(DEFAULT_ESTADO);
        assertThat(testFuncionalidad.getPrioridad()).isEqualTo(UPDATED_PRIORIDAD);
    }

    @Test
    @Transactional
    void fullUpdateFuncionalidadWithPatch() throws Exception {
        // Initialize the database
        funcionalidadRepository.saveAndFlush(funcionalidad);

        int databaseSizeBeforeUpdate = funcionalidadRepository.findAll().size();

        // Update the funcionalidad using partial update
        Funcionalidad partialUpdatedFuncionalidad = new Funcionalidad();
        partialUpdatedFuncionalidad.setId(funcionalidad.getId());

        partialUpdatedFuncionalidad.nombre(UPDATED_NOMBRE).estado(UPDATED_ESTADO).prioridad(UPDATED_PRIORIDAD);

        restFuncionalidadMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedFuncionalidad.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedFuncionalidad))
            )
            .andExpect(status().isOk());

        // Validate the Funcionalidad in the database
        List<Funcionalidad> funcionalidadList = funcionalidadRepository.findAll();
        assertThat(funcionalidadList).hasSize(databaseSizeBeforeUpdate);
        Funcionalidad testFuncionalidad = funcionalidadList.get(funcionalidadList.size() - 1);
        assertThat(testFuncionalidad.getNombre()).isEqualTo(UPDATED_NOMBRE);
        assertThat(testFuncionalidad.getEstado()).isEqualTo(UPDATED_ESTADO);
        assertThat(testFuncionalidad.getPrioridad()).isEqualTo(UPDATED_PRIORIDAD);
    }

    @Test
    @Transactional
    void patchNonExistingFuncionalidad() throws Exception {
        int databaseSizeBeforeUpdate = funcionalidadRepository.findAll().size();
        funcionalidad.setId(count.incrementAndGet());

        // Create the Funcionalidad
        FuncionalidadDTO funcionalidadDTO = funcionalidadMapper.toDto(funcionalidad);

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restFuncionalidadMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, funcionalidadDTO.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(funcionalidadDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Funcionalidad in the database
        List<Funcionalidad> funcionalidadList = funcionalidadRepository.findAll();
        assertThat(funcionalidadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchFuncionalidad() throws Exception {
        int databaseSizeBeforeUpdate = funcionalidadRepository.findAll().size();
        funcionalidad.setId(count.incrementAndGet());

        // Create the Funcionalidad
        FuncionalidadDTO funcionalidadDTO = funcionalidadMapper.toDto(funcionalidad);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFuncionalidadMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(funcionalidadDTO))
            )
            .andExpect(status().isBadRequest());

        // Validate the Funcionalidad in the database
        List<Funcionalidad> funcionalidadList = funcionalidadRepository.findAll();
        assertThat(funcionalidadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamFuncionalidad() throws Exception {
        int databaseSizeBeforeUpdate = funcionalidadRepository.findAll().size();
        funcionalidad.setId(count.incrementAndGet());

        // Create the Funcionalidad
        FuncionalidadDTO funcionalidadDTO = funcionalidadMapper.toDto(funcionalidad);

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restFuncionalidadMockMvc
            .perform(
                patch(ENTITY_API_URL)
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(funcionalidadDTO))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the Funcionalidad in the database
        List<Funcionalidad> funcionalidadList = funcionalidadRepository.findAll();
        assertThat(funcionalidadList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteFuncionalidad() throws Exception {
        // Initialize the database
        funcionalidadRepository.saveAndFlush(funcionalidad);

        int databaseSizeBeforeDelete = funcionalidadRepository.findAll().size();

        // Delete the funcionalidad
        restFuncionalidadMockMvc
            .perform(delete(ENTITY_API_URL_ID, funcionalidad.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Funcionalidad> funcionalidadList = funcionalidadRepository.findAll();
        assertThat(funcionalidadList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
