package cl.tinet.upgrade.domain;

import static org.assertj.core.api.Assertions.assertThat;

import cl.tinet.upgrade.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class HitoTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Hito.class);
        Hito hito1 = new Hito();
        hito1.setId(1L);
        Hito hito2 = new Hito();
        hito2.setId(hito1.getId());
        assertThat(hito1).isEqualTo(hito2);
        hito2.setId(2L);
        assertThat(hito1).isNotEqualTo(hito2);
        hito1.setId(null);
        assertThat(hito1).isNotEqualTo(hito2);
    }
}
