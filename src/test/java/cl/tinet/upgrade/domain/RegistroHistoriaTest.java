package cl.tinet.upgrade.domain;

import static org.assertj.core.api.Assertions.assertThat;

import cl.tinet.upgrade.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class RegistroHistoriaTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(RegistroHistoria.class);
        RegistroHistoria registroHistoria1 = new RegistroHistoria();
        registroHistoria1.setId(1L);
        RegistroHistoria registroHistoria2 = new RegistroHistoria();
        registroHistoria2.setId(registroHistoria1.getId());
        assertThat(registroHistoria1).isEqualTo(registroHistoria2);
        registroHistoria2.setId(2L);
        assertThat(registroHistoria1).isNotEqualTo(registroHistoria2);
        registroHistoria1.setId(null);
        assertThat(registroHistoria1).isNotEqualTo(registroHistoria2);
    }
}
