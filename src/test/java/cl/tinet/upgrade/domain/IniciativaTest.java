package cl.tinet.upgrade.domain;

import static org.assertj.core.api.Assertions.assertThat;

import cl.tinet.upgrade.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class IniciativaTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Iniciativa.class);
        Iniciativa iniciativa1 = new Iniciativa();
        iniciativa1.setId(1L);
        Iniciativa iniciativa2 = new Iniciativa();
        iniciativa2.setId(iniciativa1.getId());
        assertThat(iniciativa1).isEqualTo(iniciativa2);
        iniciativa2.setId(2L);
        assertThat(iniciativa1).isNotEqualTo(iniciativa2);
        iniciativa1.setId(null);
        assertThat(iniciativa1).isNotEqualTo(iniciativa2);
    }
}
