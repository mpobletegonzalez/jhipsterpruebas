package cl.tinet.upgrade.domain;

import static org.assertj.core.api.Assertions.assertThat;

import cl.tinet.upgrade.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class EstadosHistoriasTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(EstadosHistorias.class);
        EstadosHistorias estadosHistorias1 = new EstadosHistorias();
        estadosHistorias1.setId(1L);
        EstadosHistorias estadosHistorias2 = new EstadosHistorias();
        estadosHistorias2.setId(estadosHistorias1.getId());
        assertThat(estadosHistorias1).isEqualTo(estadosHistorias2);
        estadosHistorias2.setId(2L);
        assertThat(estadosHistorias1).isNotEqualTo(estadosHistorias2);
        estadosHistorias1.setId(null);
        assertThat(estadosHistorias1).isNotEqualTo(estadosHistorias2);
    }
}
